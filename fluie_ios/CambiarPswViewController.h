//
//  CambiarPswViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/19/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CambiarPswViewController : UIViewController

@property (weak, nonatomic) NSDictionary *usuario;

@end
