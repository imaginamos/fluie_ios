//
//  CambiarPswViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/19/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "CambiarPswViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"

@interface CambiarPswViewController (){
    
    UITextField *mytextField;
    UITapGestureRecognizer *tap;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UITextField *antigua;
@property (weak, nonatomic) IBOutlet UITextField *nueva;
@property (weak, nonatomic) IBOutlet UITextField *nueva2;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UIButton *lbl1;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (strong, nonatomic) MBProgressHUD *progressHUD;


@end



@implementation CambiarPswViewController

-(void)viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _nueva.placeholder = @"New password";
        _nueva2.placeholder = @"Confirm New password";
        _antigua.placeholder = @"Old password";
        [_lbl1 setTitle:@"Change password" forState:UIControlStateNormal];
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [_saveBtn setTitle:@"SAVE" forState:UIControlStateNormal];
    }
}

-(void)dismissKeyboard {
    [mytextField resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    //_miimageView.layer.cornerRadius = 100;
    self.imgPhoto.layer.masksToBounds = YES;
    // border radius
    [self.imgPhoto.layer setCornerRadius:45.0f];
    // border
    [self.imgPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.imgPhoto.layer setBorderWidth:2.5f];
    // drop shadow
    [self.imgPhoto.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.imgPhoto.layer setShadowOpacity:0.8];
    [self.imgPhoto.layer setShadowRadius:3.0];
    [self.imgPhoto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.myscroll setScrollEnabled:YES];
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 363)];
    
    [self getFoto];
}

-(void)getFoto {
    /*
     * Traigo la foto del usuario
     */
    
    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            //NSString *foto = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:kCFStringEncodingUTF8];
            NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            if([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender_id"] isEqualToString:@"1"])
                [_imgPhoto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", foto[@"image"]]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            else
                [_imgPhoto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", foto[@"image"]]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
        }
    }];
    [dataTask resume];
    
    
    
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 363+153)];

    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 263)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 263)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [textField resignFirstResponder];
    // }
    return YES;
}


- (IBAction)onGuardar:(id)sender {

    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.dimBackground = YES;
    }
    
    NSLog(@"%@", _usuario);
    
    NSString *language1;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        language1=@"2";
    }
    else
    {
        language1=@"1";
    }
    NSString *urlString = @"http://fluie.com/fluie/api/user/changePassword";
    NSString *myRequestString = [NSString stringWithFormat:@"email=%@&password=%@&language=%@",_usuario[@"email"], _nueva.text,language1];
    
    NSLog(@"%@",myRequestString);
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if([arrayresponse[@"status"] isEqualToString:@"ok"]){
        
        [self hideProgressHUD];
        
        AlmosaferAlertView *alert;
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Entry"
                     contentText:[NSString stringWithFormat:@"%@", arrayresponse[@"message"]]
                     leftButtonTitle:@"Accept"
                     rightButtonTitle:nil];
        }
        else
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Ingreso"
                     contentText:[NSString stringWithFormat:@"%@", arrayresponse[@"message"]]
                     leftButtonTitle:@"Aceptar"
                     rightButtonTitle:nil];
        }
        [alert showInView:self.view];

        alert.leftBlock = ^() {
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        
    
    }else{
        [self hideProgressHUD];
        id val = nil;
        
        AlmosaferAlertView *alert;
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Entry"
                     contentText:[NSString stringWithFormat:@"%@", arrayresponse[@"message"]]
                     leftButtonTitle:@"Accept"
                     rightButtonTitle:nil];
        }
        else
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Ingreso"
                     contentText:[NSString stringWithFormat:@"%@", arrayresponse[@"message"]]
                     leftButtonTitle:@"Aceptar"
                     rightButtonTitle:nil];
        }
        [alert showInView:self.view];

        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
        };
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
