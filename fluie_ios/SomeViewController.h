//
//  SomeViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 1/23/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PayPalMobile.h"

@interface SomeViewController : UIViewController<PayPalPaymentDelegate>

@end
