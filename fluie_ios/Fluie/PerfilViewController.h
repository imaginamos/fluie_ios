//
//  PerfilViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 11/27/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface PerfilViewController : UIViewController<NIDropDownDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (strong, nonatomic) NSURL *fotoUrl;

-(void)rel;

@end
