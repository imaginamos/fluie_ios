//
//  BienvenidoViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/20/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//
/*
                         [_imageYTView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/maxresdefault.jpg", substringForFirstMatch]] placeholderImage:[UIImage imageNamed:@"avatar"]];
 */
#import "BienvenidoViewController.h"
#import "UIImage+Resize.h"


@interface BienvenidoViewController ()
{
    NSDictionary *homedic;
    
}
@property(nonatomic, strong) IBOutlet YTPlayerView *playerView;
@property(nonatomic, strong) IBOutlet UIImageView *imageYTView;
@property(nonatomic, strong) IBOutlet UIButton *youtubeBut;
@property (weak, nonatomic) IBOutlet UILabel *textoBienvenida;
@property (weak, nonatomic) IBOutlet UILabel *titulo;
- (IBAction)playVideo:(id)sender;
- (IBAction)stopVideo:(id)sender;
- (void)playerViewDidBecomeReady:(YTPlayerView *)playerView;
- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state;
- (void)playerView:(YTPlayerView *)playerView didChangeToQuality:(YTPlaybackQuality)quality;
- (void)playerView:(YTPlayerView *)playerView receivedError:(YTPlayerError)error;
@end

@implementation BienvenidoViewController


- (IBAction)playVideo:(id)sender {
    [self.playerView playVideo];

}

- (IBAction)stopVideo:(id)sender {
    [self.playerView stopVideo];
}

- (IBAction)continuar:(id)sender {
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        [self performSegueWithIdentifier:@"logued" sender:nil];
    }else {
        [self performSegueWithIdentifier:@"noLogued" sender:nil];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.imageYTView.hidden = NO;

    self.playerView.delegate = self;
    _textoBienvenida.text = @"";
    _titulo.text = @"";
   
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *urlString = @"http://fluie.com/fluie/api/home";
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    //NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    if(returnData){
    
        NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        NSLog(@"%@", arrayresponse);

        for(id home in arrayresponse) {
            NSLog(@"%@", home);
            if ([language rangeOfString:@"es"].location == NSNotFound && [home[@"language_id"] isEqualToString:@"2"]) {
                
                NSString *youtubeURL = home[@"video"];
                
                homedic = home;
                if (home[@"description"] != (id)[NSNull null])
                    _textoBienvenida.text = home[@"description"];
                if (home[@"description"] != (id)[NSNull null])
                    _titulo.text = home[@"title"];
                NSError *error = NULL;
                NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
                NSRegularExpression *regex =
                [NSRegularExpression regularExpressionWithPattern:regexString
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
                NSTextCheckingResult *match = [regex firstMatchInString:youtubeURL
                                                                options:0
                                                                  range:NSMakeRange(0, [youtubeURL length])];
                if (match) {
                    NSRange videoIDRange = [match rangeAtIndex:0];
                    NSString *substringForFirstMatch = [youtubeURL substringWithRange:videoIDRange];
                    NSLog(@"video id: %@", substringForFirstMatch);
                    UIImage *thumbNailImage = nil;
                    
                    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/maxresdefault.jpg", substringForFirstMatch]]];
                    thumbNailImage = [UIImage imageWithData: imageData];
                    UIImage *scaledImage = [thumbNailImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(320, 256) interpolationQuality:kCGInterpolationHigh];
                    
                    UIImage *scaledImage2 = [scaledImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeTile];
                    
                    [self.imageYTView setImage:scaledImage2];
                    
                    [self.playerView loadWithVideoId:substringForFirstMatch];
                    
                    
                }
            }
            
            if ([language rangeOfString:@"es"].location != NSNotFound  && [home[@"language_id"] isEqualToString:@"1"]) {
                NSString *youtubeURL = home[@"video"];
                homedic = home;
                if (home[@"description"] != (id)[NSNull null])
                    _textoBienvenida.text = home[@"description"];
                if (home[@"description"] != (id)[NSNull null])
                    _titulo.text = home[@"title"];
                
                NSError *error = NULL;
                NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
                NSRegularExpression *regex =
                [NSRegularExpression regularExpressionWithPattern:regexString
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
                NSTextCheckingResult *match = [regex firstMatchInString:youtubeURL
                                                                options:0
                                                                  range:NSMakeRange(0, [youtubeURL length])];
                if (match) {
                    NSRange videoIDRange = [match rangeAtIndex:0];
                    NSString *substringForFirstMatch = [youtubeURL substringWithRange:videoIDRange];
                    NSLog(@"video id: %@", substringForFirstMatch);
                    UIImage *thumbNailImage = nil;
                    
                    NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: [NSString stringWithFormat:@"http://img.youtube.com/vi/%@/maxresdefault.jpg", substringForFirstMatch]]];
                    thumbNailImage = [UIImage imageWithData: imageData];
                    UIImage *scaledImage = [thumbNailImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(320, 256) interpolationQuality:kCGInterpolationHigh];
                    
                    UIImage *scaledImage2 = [scaledImage resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0) resizingMode:UIImageResizingModeTile];
                    
                    [self.imageYTView setImage:scaledImage2];
                    
                    [self.playerView loadWithVideoId:substringForFirstMatch];
                    
                    
                }
            }
            
        }
    
    }
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)playerView:(YTPlayerView *)playerView didChangeToState:(YTPlayerState)state {
    switch (state) {
        case kYTPlayerStatePlaying:
            NSLog(@"Started playback");
            break;
        case kYTPlayerStatePaused:
            NSLog(@"Paused playback");
            break;
        case kYTPlayerStateBuffering:
            self.imageYTView.hidden = YES;
            self.youtubeBut.hidden = YES;
            NSLog(@"Video buffering");
            break;
        default:
            break;
    }
}


@end
