//
//  MsnVideoViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/11/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface MsnVideoViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@end
