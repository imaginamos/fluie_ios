//
//  PushNoAnimationSegue.h
//  Fluie
//
//  Created by Carlos Robinson on 2/23/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 Move to the next screen without an animation.
 */
@interface PushNoAnimationSegue : UIStoryboardSegue

@end
