//
//  SplashViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 3/7/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import "SplashViewController.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"

@interface SplashViewController (){
    NSString *language_id;
    NSTimer *timerupdt;
    int lastPage;
}

@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (strong, nonatomic) MBProgressHUD *progressHUD;

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    for (UIView *aView in [_myscroll subviews])
    {
        [aView removeFromSuperview];
    }
    language_id = @"1";
    _myscroll.delegate = self;
    _myscroll.pagingEnabled = YES;
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        language_id = @"2";
    }
    
    NSString* boxQuery = [NSString stringWithFormat:@"{\"language_id\":\"%@\"}",language_id];
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?filter=%@", escBox]; //query para pasarle a la url
    //NSString *urlString = @"http://fluie.com/fluie/api/home";
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"banner"] stringByAppendingString:query];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil)
        {
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSLog(@"%@", arrayresponse);
            self.myscroll.contentSize = CGSizeMake(self.myscroll.frame.size.width * 4, self.myscroll.frame.size.height);
            self.myscroll.delegate = self;
            lastPage = (int)[arrayresponse count];
            for(int i = 0; i<[arrayresponse count]; i++)
            {
                NSDictionary *receptDictionary = [arrayresponse objectAtIndex:i];
                UIImageView *_miimageView = [[UIImageView alloc] init];
                _miimageView.frame = CGRectMake(i*(_myscroll.frame.size.width), 0, _myscroll.frame.size.width, _myscroll.frame.size.height);//width and height
                
                NSData * imageData = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: receptDictionary[@"image"]]];
                UIImage *thumbNailImage = [UIImage imageWithData: imageData];
                UIImage *scaledImage = [thumbNailImage resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(_miimageView.frame.size.width, _miimageView.frame.size.height) interpolationQuality:kCGInterpolationHigh];
                
                [_miimageView setImage:scaledImage];
                
                [_myscroll addSubview:_miimageView];
                _miimageView.contentMode = UIViewContentModeScaleAspectFit;
            }
            timerupdt = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
        }
    }];
    [dataTask resume];
    

    // Do any additional setup after loading the view.
}
- (void)updateTime:(NSTimer *)timer
{
    CGFloat contentOffset = _myscroll.contentOffset.x;
    // calculate next page to display
    int nextPage = (int)(contentOffset/_myscroll.frame.size.width) + 1 ;
    
    if(nextPage == lastPage)
    {
        if(timerupdt)
        {
            [timerupdt invalidate];
            timerupdt = nil;
        }
        NSTimer *timerfinish = [NSTimer scheduledTimerWithTimeInterval:3.0 target:self selector:@selector(gotonextview:) userInfo:nil repeats:NO];
    }
    else
    {
        //self.myscroll.scrollRectToVisible(CGRectMake(contentOffset + _myscroll.frame.size.width, 0, _myscroll.frame.size.width, _myscroll.frame.size.height, animated: true);
        
        CGRect scrollTarget = CGRectMake(nextPage * _myscroll.frame.size.width, 0, _myscroll.frame.size.width, _myscroll.frame.size.height);
        [_myscroll scrollRectToVisible:scrollTarget animated:YES];
        
    }
}

- (void)gotonextview:(NSTimer *)timer
{
    if(timer)
    {
        [timer invalidate];
        timer = nil;
    }
    
    [self performSegueWithIdentifier:@"goBienvenido" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
