//
//  TerminosViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/26/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "TerminosViewController.h"

@interface TerminosViewController ()


@property (weak, nonatomic) IBOutlet UIWebView *mywebview;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;

@end

@implementation TerminosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self cargaTerminos];
    // Do any additional setup after loading the view.
}

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        [_cancel setTitle:@"Cancel" forState: UIControlStateNormal];
        _lbl1.text = @"Terms and Conditions";
    }
}

-(void) cargaTerminos {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/home/";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    UIFont *font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    for(id home in arrayresponse) {
        if ([language rangeOfString:@"es"].location == NSNotFound && [home[@"language_id"] isEqualToString:@"2"]) {
            NSString *htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: 12\">%@</span>", font.fontName, home[@"terms_conditions"]];
            [_mywebview setBackgroundColor:[UIColor whiteColor]];
            [_mywebview setOpaque:NO];
            [_mywebview loadHTMLString:htmlString baseURL:nil];
            
            break;
        }
        
        if ([language rangeOfString:@"es"].location != NSNotFound  && [home[@"language_id"] isEqualToString:@"1"]) {
            NSString *htmlString = [NSString stringWithFormat:@"<span style=\"font-family: %@; font-size: 12\">%@</span>", font.fontName, home[@"terms_conditions"]];
            [_mywebview setBackgroundColor:[UIColor whiteColor]];
            [_mywebview setOpaque:NO];
            [_mywebview loadHTMLString:htmlString baseURL:nil];
            
            break;
        }
        
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
