//
//  MsnVozViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/12/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "BeneficiariosListViewController.h"
#import "NIDropDown.h"

@interface MsnVozViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate, UITextFieldDelegate, BeneficiariosListViewControllerDelegate, NIDropDownDelegate>{
    NIDropDown *dropDownPregrado;
}
@property (weak, nonatomic) IBOutlet UIButton *recordPauseButton;
@property (weak, nonatomic) IBOutlet UIButton *stopButton;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIProgressView *progressview;
@property (strong, nonatomic) NSDictionary *borradorDiccionario;
@property (assign, nonatomic) BOOL *esBorrador;

- (IBAction)recordPauseTapped:(id)sender;
- (IBAction)stopTapped:(id)sender;
- (IBAction)playTapped:(id)sender;
- (IBAction)borrar:(id)sender;
@end
