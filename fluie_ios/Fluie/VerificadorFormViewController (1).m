//
//  VerificadorFormViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "VerificadorFormViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"

@interface VerificadorFormViewController (){
    bool masculino;
    UITextField *mytextField;
    NSArray *contentsParentezcoArray;
    NSArray *usuariosArray;
    NSDictionary *recienCreado;
    NSString *parentezcoSelected;
    bool match;
    int beneficiarioId;
    
}

@property (weak, nonatomic) IBOutlet UITextField *nombresTxt;
@property (weak, nonatomic) IBOutlet UITextField *apellidosTxt;
@property (weak, nonatomic) IBOutlet UITextField *correoTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnMasculino;
@property (weak, nonatomic) IBOutlet UIButton *btnFemenino;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;

@property (weak, nonatomic) IBOutlet UILabel *lbl3;
@property (weak, nonatomic) IBOutlet UILabel *lbl4;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;

@end

@implementation VerificadorFormViewController

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        _lbl1.text = @"Fill the checker information";
        _nombresTxt.placeholder = @"Name";
        _apellidosTxt.placeholder = @"Last Name";
        _correoTxt.placeholder = @"Email";
       
        _lbl2.text=@"Gender";
        _lbl3.text=@"Male";
        _lbl4.text=@"Female";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        [_btn2 setTitle:@"DELETE" forState:UIControlStateNormal];
        
    }
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    masculino = TRUE;
    match = FALSE;
    // Do any additional setup after loading the view.
    [self cargarUsuarios];
    if(_verificador!=nil){
        NSLog(@"%@", _verificador);
        beneficiarioId = [_verificador[@"user_id"] intValue];
        _nombresTxt.text = _verificador[@"user"][@"name"];
        _apellidosTxt.text = _verificador[@"user"][@"name_last"];
        _correoTxt.text = _verificador[@"user"][@"email"];
        
    }
        // Do any additional setup after loading the view.
}

-(void)cargarUsuarios {
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user/";
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    if(returnData){
        NSArray *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        if(response.count){
            usuariosArray=response;
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)guardar:(id)sender{
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Guardando información";
        
    }
    
    if(_verificador!=nil){
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Verificador"
                                     contentText:@"El verificador se ha actualizado con éxito"
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        
    }else
        for(id user in usuariosArray) {
            if ([_correoTxt.text isEqualToString:user[@"email"]]) {
                match = TRUE;
                
                //Creo el beneficiario y lo asigno a mi usuario
               
                NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/checkerUser";
                NSString *myRequestString = [NSString stringWithFormat:@"checker_id=%@&user_id=%@&code=asdfg",user[@"iduser"], [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
                
                NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                [request setHTTPMethod: @"POST"];
                [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                [request setHTTPBody: myRequestData];
                NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                if(returnData){
                    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
                    NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    
                    if(!responseDic[@"error"]){
                        
                        _nombresTxt.text = @"";
                        _correoTxt.text = @"";
                        _apellidosTxt.text = @"";
                        [self hideProgressHUD];
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Verificador"
                                                     contentText:@"El Verificador se ha guardado con éxito"
                                                     leftButtonTitle:@"Aceptar"
                                                     rightButtonTitle:nil];
                        [alert showInView:self.view];
                        
                        alert.leftBlock = ^() {
                            NSLog(@"left button clicked");
                            [self dismissViewControllerAnimated:YES completion:nil];
                        };
                        
                    }else if(responseDic[@"error"]){
                        id val = nil;
                        NSArray *values = [responseDic[@"error"] allValues];
                        if ([values count] != 0)
                            val = [values objectAtIndex:0];
                        
                        NSLog(@"%@", val);
                        NSString *respuesta=@"";
                        if([val isKindOfClass:[NSArray class]]){
                            respuesta = [val objectAtIndex:0];
                        }else if([val isKindOfClass:[NSString class]]){
                            respuesta = val;
                        }else{
                            respuesta = val;
                        }
                        NSLog(@"%@", respuesta);
                        [self hideProgressHUD];
                        
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Verificador"
                                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                                     leftButtonTitle:@"Aceptar"
                                                     rightButtonTitle:nil];
                        [alert showInView:self.view];
                        
                        alert.leftBlock = ^() {
                            NSLog(@"left button clicked");
                            
                        };
                        
                    }
                }
                break;
                
            }
            
            
        }
    
    if (!match) {
        //Si llego aquí es porque debo crear al usuario
        NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user";
        NSString *myRequestString = [NSString stringWithFormat:@"space=0&identification=checker&pass=checker&confirm_password=checker&terms=1&email=%@&name=%@&name_last=%@&phone=11111&address=checker",_correoTxt.text, _nombresTxt.text, _apellidosTxt.text];
        NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"POST"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody: myRequestData];
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        if(returnData){
            [self cargarUsuarios];
            
            for(id usuario in usuariosArray) {
                if ([usuario[@"email"] isEqualToString:_correoTxt.text]) {
                    recienCreado = usuario;
                }
            }
            NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            if(!arrayresponse[@"error"]){
                
                NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/checkerUser";
                NSString *myRequestString = [NSString stringWithFormat:@"checker_id=%@&user_id=%@&code=checkerapp",recienCreado[@"iduser"], [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
                
                NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                [request setHTTPMethod: @"POST"];
                [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                [request setHTTPBody: myRequestData];
                NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                if(returnData){
                    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
                    NSLog(@"response %@",responsestring);
                    NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    
                    if(!responseDic[@"error"]){
                        
                        _nombresTxt.text = @"";
                        _correoTxt.text = @"";
                        _apellidosTxt.text = @"";
                        
                        [self hideProgressHUD];
                        
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Verificador"
                                                     contentText:@"El verificador se ha guardado con éxito"
                                                     leftButtonTitle:@"Aceptar"
                                                     rightButtonTitle:nil];
                        [alert showInView:self.view];
                        
                        alert.leftBlock = ^() {
                            NSLog(@"left button clicked");
                            [self dismissViewControllerAnimated:YES completion:nil];
                        };
                        
                    }else if(responseDic[@"error"]){
                        id val = nil;
                        NSArray *values = [responseDic[@"error"] allValues];
                        if ([values count] != 0)
                            val = [values objectAtIndex:0];
                        
                        NSString *respuesta=@"";
                        if([val isKindOfClass:[NSArray class]]){
                            respuesta = [val objectAtIndex:0];
                        }else if([val isKindOfClass:[NSString class]]){
                            respuesta = val;
                        }else{
                            respuesta = val;
                        }
                        NSLog(@"%@", respuesta);
                        [self hideProgressHUD];
                        
                        
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Verificador"
                                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                                     leftButtonTitle:@"Aceptar"
                                                     rightButtonTitle:nil];
                        [alert showInView:self.view];
                        
                        alert.leftBlock = ^() {
                            NSLog(@"left button clicked");
                            
                        };
                        
                        
                    }
                }
                //[self performSegueWithIdentifier:@"logged" sender:nil];
            }else if(arrayresponse[@"error"]){
                id val = nil;
                NSArray *values = [arrayresponse[@"error"] allValues];
                if ([values count] != 0)
                    val = [values objectAtIndex:0];
                
                NSLog(@"%@", val);
                NSString *respuesta=@"";
                if([val isKindOfClass:[NSArray class]]){
                    respuesta = [val objectAtIndex:0];
                }else if([val isKindOfClass:[NSString class]]){
                    respuesta = val;
                }else{
                    respuesta = val;
                }
                NSLog(@"%@", respuesta);
                [self hideProgressHUD];
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Verificador"
                                             contentText:[NSString stringWithFormat:@"%@", respuesta]
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
                
                alert.leftBlock = ^() {
                    NSLog(@"left button clicked");
                    
                };
            }
        }
    }
}

-(IBAction)borrar:(id)sender{
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Borrando verificador";
        
    }
    
    if(beneficiarioId){
        
        NSString *urlString = [NSString stringWithFormat:@"http://mentesweb.com/fluie/programacion/api/checkerUser/%i", beneficiarioId];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"DELETE"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        if(returnData){
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
            
            
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Beneficiario"
                                         contentText:@"El beneficiario se ha borrado"
                                         leftButtonTitle:@"Aceptar"
                                         rightButtonTitle:nil];
            [alert showInView:self.view];
            
            alert.leftBlock = ^() {
                NSLog(@"left button clicked");
                [self dismissViewControllerAnimated:YES completion:nil];
            };
        }
        
    }else{
        _nombresTxt.text = @"";
        _apellidosTxt.text = @"";
        _correoTxt.text = @"";
        
    }
}

-(IBAction)onMasculino:(id)sender{
    NSLog(@"masculino");
    
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnMasculino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnFemenino setImage:nil forState:UIControlStateNormal];
    
}

-(IBAction)onFemenino:(id)sender{
    NSLog(@"femenino");
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnFemenino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnMasculino setImage:nil forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [textField resignFirstResponder];
    // }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509+253)];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
