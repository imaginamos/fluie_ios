//
//  PerfilViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/27/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "PerfilViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"

@interface PerfilViewController ()
{
    NSDictionary *usuario;
    NSArray *contentsPaisArray;
    NSString *paisSelected;
    UITextField *mytextField;
    BOOL isPhotoSelected;
    NSData *image;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UITextField *nombre;
@property (weak, nonatomic) IBOutlet UITextField *direccion;
@property (weak, nonatomic) IBOutlet UITextField *mail;
@property (weak, nonatomic) IBOutlet UITextField *movil;
@property (weak, nonatomic) IBOutlet UITextField *apellido;
@property (weak, nonatomic) IBOutlet UITextField *tel;
@property (weak, nonatomic) IBOutlet UITextField *pais;
@property (weak, nonatomic) IBOutlet UITextField *mail2;
@property (weak, nonatomic) IBOutlet UIButton *pais_button;
@property (strong, nonatomic) MBProgressHUD *progressHUD;


@end

@implementation PerfilViewController

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Cargando información";
        
    }
    
    self.imgPhoto.layer.masksToBounds = YES;
    // border radius
    [self.imgPhoto.layer setCornerRadius:45.0f];
    // border
    [self.imgPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.imgPhoto.layer setBorderWidth:2.5f];
    // drop shadow
    [self.imgPhoto.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.imgPhoto.layer setShadowOpacity:0.8];
    [self.imgPhoto.layer setShadowRadius:3.0];
    [self.imgPhoto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    /**/
    
    dropDownPregrado.delegate = self;
    
    [self.myscroll setScrollEnabled:YES];
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609)];
    
//    [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"];
    
    NSString *urlString = [NSString stringWithFormat:@"http://mentesweb.com/fluie/programacion/api/user/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    if(returnData){
        NSDictionary *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        
        usuario = response;
        NSLog(@"%@", usuario);
        _nombre.text = usuario[@"name"];
        _direccion.text = usuario[@"address"];
        _mail.text = usuario[@"email"];
        _mail2.text = usuario[@"email_optional"];
        _movil.text = usuario[@"phone"];
        _apellido.text = usuario[@"name_last"];
        _tel.text = usuario[@"phone"];
        [self hideProgressHUD];
        NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/country/";
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"GET"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
        NSLog(@"response %@",responsestring);
        
        NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        if(returnData){
             for(id pais in arrayresponse) {
                 if([pais[@"idcountry"] isEqualToString:usuario[@"country_id"]])
                 _pais_button.titleLabel.text = pais[@"country_id"];
             }
        }
        
        [self cargaPaises];
        
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void) cargaPaises {
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/country/";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        contentsPaisArray=arrayresponse;
        
        for (int i=0; i<contentsPaisArray.count; i++) {
            NSDictionary *diccionario = [contentsPaisArray objectAtIndex:i];
            NSLog(@"diccionario[idcountry]: %@", diccionario[@"idcountry"]);
            NSLog(@"usuario[country_id]: %@", usuario[@"country_id"]);
            if([diccionario[@"idcountry"] isEqualToString: usuario[@"country_id"]]){
                [_pais_button setTitle:diccionario[@"country"] forState:UIControlStateNormal];
            }
        
        }
    }
}

-(IBAction)onregister:(id)sender
{
    
    
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Cargando Beneficiarios";
        
    }
    
    
    if(![_pais_button.titleLabel.text isEqualToString:@""]){
        for (int i=0; i<contentsPaisArray.count; i++) {
            NSDictionary *diccionario = [contentsPaisArray objectAtIndex:i];
            NSLog(@"%@", diccionario);
            NSLog(@"title label %@", paisSelected);
            
            if([diccionario[@"country"] isEqualToString:[paisSelected stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                paisSelected = diccionario[@"idcountry"];
        }
        
    }
    
    NSLog(@"%@", paisSelected);
    NSString *urlString = [NSString stringWithFormat:@"http://mentesweb.com/fluie/programacion/api/user/%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:@"0" forKey:@"space"];
    [_params setObject:[NSString stringWithFormat:@"%i", [paisSelected intValue]] forKey:@"country_id"];
    [_params setObject:[NSString stringWithFormat:@"%@", _tel.text] forKey:@"phone"];
    [_params setObject:[NSString stringWithFormat:@"123123"] forKey:@"identification"];
    [_params setObject:[NSString stringWithFormat:@"1"] forKey:@"terms"];
    [_params setObject:[NSString stringWithFormat:@"%@", _mail.text] forKey:@"email"];
    [_params setObject:[NSString stringWithFormat:@"%@", _mail2.text] forKey:@"email_opcional"];
    [_params setObject:[NSString stringWithFormat:@"%@", _nombre.text] forKey:@"nombre"];
    [_params setObject:[NSString stringWithFormat:@"%@", _nombre.text] forKey:@"name"];
    [_params setObject:[NSString stringWithFormat:@"%@", _apellido.text] forKey:@"name_last"];
    [_params setObject:[NSString stringWithFormat:@"%@", _direccion.text] forKey:@"address"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"image";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    //[request setTimeoutInterval:30];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"PUT"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    NSData *imageData = UIImageJPEGRepresentation(self.imgPhoto.image, 1.0);
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",returnString);
    NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(!arrayresponse[@"error"]){
        [self hideProgressHUD];
        
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Registro"
                                     contentText:@"Ud se ha registrado con éxito"
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
            [self dismissViewControllerAnimated:YES completion:nil];
            
        };
        
        //[self performSegueWithIdentifier:@"logged" sender:nil];
    }else if(arrayresponse[@"error"]){
        id val = nil;
        NSArray *values = [arrayresponse[@"error"] allValues];
        if ([values count] != 0)
            val = [values objectAtIndex:0];
        
        NSLog(@"%@", val);
        NSString *respuesta=@"";
        if([val isKindOfClass:[NSArray class]]){
            respuesta = [val objectAtIndex:0];
        }else if([val isKindOfClass:[NSString class]]){
            respuesta = val;
        }else{
            respuesta = val;
        }
        NSLog(@"%@", respuesta);
        [self hideProgressHUD];
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Registro"
                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
            
        };
    }
    
    
    
}

- (IBAction)onPais:(id)sender
{
    [mytextField resignFirstResponder];
    
    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsPaisArray valueForKey:@"country"];
    if(dropDownPregrado == nil) {
        CGFloat f = 80;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}

#pragma mark Photo

- (IBAction)onBtnTakePhoto:(id)sender {
    UIActionSheet *actionSheetView = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Tomar foto" otherButtonTitles:@"Tomar foto existente", nil];
    
    [actionSheetView showInView:[self view]];
}

-(void)selectPhoto {
   
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    [pickerController setAllowsEditing:NO];
    [pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [self presentViewController:pickerController animated:YES completion:^{}];
    
}

-(void)takePhoto {
   
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    [pickerController setAllowsEditing:NO];
    [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    
    [self presentViewController:pickerController animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    isPhotoSelected = YES;
    
    UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
    // Crop the image to a square (yikes, fancy!)
    UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
    
    [self.imgPhoto setImage:croppedImage];
    
    NSData* imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    NSString* imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/saved_example_image.png"];
    [imageData writeToFile:imagePath atomically:YES];
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
   
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self takePhoto];
            break;
        }
        case 1: {
            [self selectPhoto];
            break;
        }
        default:
            break;
    }
}

#pragma mark NIDropDown

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    paisSelected = titulo;
    if(sender == _pais_button)
        [[NSUserDefaults standardUserDefaults] setObject:titulo forKey:@"pais_Button"];
    
    //[self rel];
}

-(void)rel{
    dropDownPregrado = nil;
}


-(void)dismissKeyboard {
    [mytextField resignFirstResponder];
}

#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609+253)];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [textField resignFirstResponder];
    // }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
}


@end
