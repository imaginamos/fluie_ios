//
//  PushNoAnimationSegue.m
//  Fluie
//
//  Created by Carlos Robinson on 2/23/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import "PushNoAnimationSegue.h"

@implementation PushNoAnimationSegue

- (void)perform {
    
    [self.sourceViewController.navigationController pushViewController:self.destinationViewController animated:NO];
}

@end