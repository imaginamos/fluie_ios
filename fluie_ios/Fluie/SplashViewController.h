//
//  SplashViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 3/7/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController<UIScrollViewDelegate>

@end
