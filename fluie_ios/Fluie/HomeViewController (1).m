//
//  HomeViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/8/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "HomeViewController.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"



@interface HomeViewController ()
{
    __weak IBOutlet UIImageView *progreso;
    __weak IBOutlet UIView *menu;
    NSDictionary *usuario;
    __weak IBOutlet UIView *menuppal;
    NSString * language;
}
@property (weak, nonatomic) IBOutlet UIButton *btncomprar;
@property (weak, nonatomic) IBOutlet UIImageView *foto;
@property (weak, nonatomic) IBOutlet UILabel *nombremenu;
@property (weak, nonatomic) IBOutlet UIView *vistaopaca;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *progressbar;
@property (weak, nonatomic) IBOutlet UILabel *pais;
@property (weak, nonatomic) IBOutlet UILabel *mail;
@property (weak, nonatomic) IBOutlet UILabel *progresoText;

@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIProgressView *progressview;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UILabel *LBL1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UILabel *lbl3;
@property (weak, nonatomic) IBOutlet UILabel *lbl4;
@property (weak, nonatomic) IBOutlet UILabel *lbl5;
@property (weak, nonatomic) IBOutlet UILabel *lbl6;
@property (weak, nonatomic) IBOutlet UILabel *lbl7;
@property (weak, nonatomic) IBOutlet UILabel *lbl8;
@property (weak, nonatomic) IBOutlet UILabel *lbl9;
@property (weak, nonatomic) IBOutlet UILabel *lbl10;
@property (weak, nonatomic) IBOutlet UILabel *lbl11;


@end

@implementation HomeViewController

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void)viewWillAppear:(BOOL)animated {
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    [_progressview setProgress:1 animated:NO];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _LBL1.text = @"Buy space";
        _progresoText.text=@"You have 40Mb available to create new messages";
        [_btn1 setTitle:@"VIEW HISTORY" forState:UIControlStateNormal];
        [_btn2 setTitle:@"NEW MESSAGE" forState:UIControlStateNormal];
        _lbl2.text =@"Beneficiary";
        _lbl3.text =@"Checker";
        _lbl4.text =@"Profile";
        _lbl5.text =@"Messages";
        _lbl6.text =@"New video message";
        _lbl7.text =@"New voice message";
        _lbl8.text =@"New text message";
        _lbl9.text =@"New picture message";
        _lbl10.text =@"Storega";
        _lbl11.text =@"Sign out";
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        menu.frame = CGRectMake(0, -153, 306, 98);
        
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            _progressHUD.labelText = @"Loading...";
            _progressHUD.detailsLabelText = @"Cargando Beneficiarios";
            
        }
        
        
        //    [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"];
        
        NSString *urlString = [NSString stringWithFormat:@"http://mentesweb.com/fluie/programacion/api/user/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"GET"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        if(returnData){
            NSDictionary *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            
            usuario = response;
            NSLog(@"%@", usuario);
            
            if(usuario[@"image"]  != (id)[NSNull null]){
                NSURL *imageURL = [NSURL URLWithString:usuario[@"image"]];
                NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                _foto.image = [UIImage imageWithData:imageData];
            }else{
                _foto.image = [UIImage imageNamed:@"avatar"];
            }
            
            _foto.layer.masksToBounds = YES;
            // border radius
            [_foto.layer setCornerRadius:50.0f];
            // border
            [_foto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
            [_foto.layer setBorderWidth:2.5f];
            // drop shadow
            [_foto.layer setShadowColor:[UIColor whiteColor].CGColor];
            [_foto.layer setShadowOpacity:0.8];
            [_foto.layer setShadowRadius:3.0];
            [_foto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
            
            _name.text = [NSString stringWithFormat:@"%@ %@", usuario[@"name"], usuario[@"name_last"]];
            _nombremenu.text = [NSString stringWithFormat:@"%@ %@", usuario[@"name"], usuario[@"name_last"]];
            _mail.text = [NSString stringWithFormat:@"%@", usuario[@"email"]];
            [self hideProgressHUD];
            
            
        }

    }
}

-(void)viewDidAppear:(BOOL)animated {
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        [self performSegueWithIdentifier:@"goLogin" sender:nil];
    }else{
        NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://mentesweb.com/fluie/programacion/api/user/freeSpace/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
        NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
        [requestFS setHTTPMethod: @"GET"];
        [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
        if(dataFS){
            NSArray *freeSpace = [[NSJSONSerialization JSONObjectWithData:dataFS options:0 error: NULL] mutableCopy];
            for(id space in freeSpace) {
                _progresoText.text = [NSString stringWithFormat:@"Actualmente tienes %@Mb disponibles para crear mensajes.", space[@"free"]];
                
                if([language rangeOfString:@"es"].location == NSNotFound)
                _progresoText.text = [NSString stringWithFormat:@"You have %@Mb available to create new messages", space[@"free"]];
                
                int progressVal = 0;
                
                if([space[@"space"] intValue]!=0){
                    int progressVal = ([space[@"space"] intValue] - [space[@"free"] intValue])/[space[@"space"] intValue];
                }
                    
                
                [_progressview setProgress:progressVal animated:YES];
                
            }
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNeedsStatusBarAppearanceUpdate];
    // Do any additional setup after loading the view.
    
}

-(IBAction)logout:(id)sender{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"idUser"];
    [self performSegueWithIdentifier:@"goLogin" sender:nil];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)viewWillDisappear:(BOOL)animated{
    menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    menu.frame = CGRectMake(6, -153, 306, 98);
    _vistaopaca.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}
- (IBAction)onBtnMenu:(id)sender {
    static Menu menu = MenuLeft;
    
    [[SlideNavigationController sharedInstance] bounceMenu:menu withCompletion:nil];
}

-(IBAction)showmenu:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.50];
    [UIView setAnimationDelegate:self];
   
    if(menu.frame.origin.y != _vistaopaca.frame.origin.y){
        menu.frame = CGRectMake(6, _vistaopaca.frame.origin.y, menu.frame.size.width,  menu.frame.size.height);
    }else{
        menu.frame = CGRectMake(6, -153, menu.frame.size.width,  menu.frame.size.height);
    }
    [UIView commitAnimations];
    
    _vistaopaca.hidden=!_vistaopaca.hidden;
}

-(IBAction)showmenuppal:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelegate:self];
    
    if(menuppal.frame.origin.x != 0)
        menuppal.frame = CGRectMake(0, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    else
        menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    
    [UIView commitAnimations];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
