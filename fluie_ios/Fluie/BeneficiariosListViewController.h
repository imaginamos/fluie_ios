//
//  BeneficiariosListViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BeneficiariosListViewController;

@protocol BeneficiariosListViewControllerDelegate <NSObject>
- (void)PlayerAdded: (BeneficiariosListViewController *)controller
          andPlayer:(NSDictionary *) beneficiario;
@end

@interface BeneficiariosListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate, UISearchDisplayDelegate,UISearchControllerDelegate>

@property (nonatomic, weak) id <BeneficiariosListViewControllerDelegate> delegado;

@end
