//
//  VerificadorListViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/11/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "VerificadorListViewController.h"
#import "VerificadorFormViewController.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "AlmosaferAlertView.h"

@interface VerificadorListViewController ()
{
    NSArray *dataSource;
    NSArray *parentezcoArray;
    NSArray *userArray;
    NSMutableDictionary *beneficiario;
    IBOutlet UISearchBar *candySearchBar;
    NSMutableArray *filteredCandyArray;
    NSString *language_id;
}
@property (weak, nonatomic) IBOutlet UITableView *mytableview;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIButton *btn1;

@end

@implementation VerificadorListViewController

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        [_btn1 setTitle:@"ADD CHECKER" forState:UIControlStateNormal];
        candySearchBar.placeholder = @"Search";
    }
    [self getdataSource];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    _mytableview.backgroundColor = [UIColor clearColor];
    _mytableview.separatorColor = [UIColor clearColor];
    _mytableview.delegate = self;
    _mytableview.dataSource = self;
    [_mytableview setSeparatorColor:[UIColor grayColor]];
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];        
    }
    
    // Do any additional setup after loading the view.
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}


-(void)getdataSource {
    
    NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?with=checker&filter=%@", escBox]; //query para pasarle a la url
    
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"checkerUser"] stringByAppendingString:query];

    //NSString *urlString = @"http://fluie.com/fluie/api/checkerUser";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    NSMutableArray *datasourcetemporal = [NSMutableArray arrayWithObjects:nil];
    if(arrayresponse.count)
    {
        for(NSDictionary* dato in arrayresponse) {
            NSMutableDictionary *datoTemp = [dato mutableCopy];
            NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", datoTemp[@"checker"][@"iduser"]];
            NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
            
            NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
            [request setHTTPMethod: @"POST"];
            [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
            [request setHTTPBody: myRequestData];
            
            NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
            NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            datoTemp[@"checker"] = [dato[@"checker"] mutableCopy];
            NSLog(@"%@", datoTemp[@"checker"]);
            [datoTemp[@"checker"] setObject:foto[@"image"] forKey:@"image"];
            
            [datasourcetemporal addObject:datoTemp]; //Almaceno el checker
        }
        dataSource=datasourcetemporal;
        [_mytableview reloadData];
        
    }else
    {
        dataSource =nil;
        [_mytableview reloadData];
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Notification"
                                         contentText:@"No assigned verifiers"
                                         leftButtonTitle:@"Accept"
                                         rightButtonTitle:nil];
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
        }
        else
        {
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Notificacion"
                                         contentText:@"No tiene verificadores asignados"
                                         leftButtonTitle:@"Aceptar"
                                         rightButtonTitle:nil];
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
        }
    }
    [self hideProgressHUD];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 62;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return filteredCandyArray.count;
    } else {
        return dataSource.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [_mytableview dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        cell = [_mytableview dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    NSMutableDictionary *diccionario = [dataSource[indexPath.row] mutableCopy];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        diccionario = [filteredCandyArray[indexPath.row] mutableCopy];
        //cell.textLabel.text = filteredCandyArray[indexPath.row][@"beneficiary"][@"name"];
    }
    
    
    UILabel *nombre = (UILabel *)[cell viewWithTag:101];
    nombre.text = [NSString stringWithFormat:@"%@ %@", diccionario[@"checker"][@"name"], diccionario[@"checker"][@"name_last"]];
    
    UILabel *editar = (UILabel *)[cell viewWithTag:1010];
    
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        editar.text = @"Edit";
        UILabel *relacion = (UILabel *)[cell viewWithTag:102];
        if([diccionario[@"checker"][@"active"] isEqualToString:@"1"])
            relacion.text = @"Active";
        else
            relacion.text = @"Inactive";
    }
    else
    {
        UILabel *relacion = (UILabel *)[cell viewWithTag:102];
        if([diccionario[@"checker"][@"active"] isEqualToString:@"1"])
            relacion.text = @"Activo";
        else
            relacion.text = @"Inactivo";
    }
    
    UIImageView *_miimageView = (UIImageView *)[cell viewWithTag:103];
    _miimageView.layer.masksToBounds = YES;
    // border radius
    [_miimageView.layer setCornerRadius:23.0f];
    // border
    [_miimageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_miimageView.layer setBorderWidth:0.5f];
    // drop shadow
    [_miimageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_miimageView.layer setShadowOpacity:0.8];
    [_miimageView.layer setShadowRadius:3.0];
    [_miimageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    if([diccionario[@"checker"][@"gender_id"] isEqualToString:@"1"])
    [_miimageView sd_setImageWithURL:[NSURL URLWithString:diccionario[@"checker"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
    else
        [_miimageView sd_setImageWithURL:[NSURL URLWithString:diccionario[@"checker"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    beneficiario = [[dataSource objectAtIndex:indexPath.row] mutableCopy];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        beneficiario = [filteredCandyArray[indexPath.row] mutableCopy];
    }
    [self performSegueWithIdentifier:@"editarverificador" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(IBAction)editar:(id)sender{
    UIButton *tempotal = (UIButton *) sender;
    NSLog(@"%ld", (long)tempotal.tag);
    beneficiario = [[dataSource objectAtIndex:tempotal.tag] mutableCopy];
    [self performSegueWithIdentifier:@"editarverificador" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"editarverificador"]) {
        VerificadorFormViewController *VerificadorDetailViewController = segue.destinationViewController;
        VerificadorDetailViewController.verificador = beneficiario;
    }
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    //searchResults = [recipes filteredArrayUsingPredicate:resultPredicate];
    
    [filteredCandyArray removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"checker.name CONTAINS[c] %@",searchText];
    NSArray *myarray = dataSource;
    
    filteredCandyArray = [NSMutableArray arrayWithArray:[dataSource filteredArrayUsingPredicate:predicate]];
    NSLog(@"filteredCandyArray %@", filteredCandyArray);
    //dataSource = [NSMutableArray arrayWithArray:[myarray filteredArrayUsingPredicate:predicate]];
    [_mytableview reloadData];
}

@end
