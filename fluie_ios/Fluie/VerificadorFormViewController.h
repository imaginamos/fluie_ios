//
//  VerificadorFormViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerificadorFormViewController : UIViewController<UITextInputDelegate>


@property (strong, nonatomic) NSDictionary *verificador;


@end
