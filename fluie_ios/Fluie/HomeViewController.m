//
//  HomeViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/8/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "HomeViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"
#import "UIImageView+WebCache.h"
#import "PerfilViewController.h"



@interface HomeViewController ()
{
    __weak IBOutlet UIImageView *progreso;
    __weak IBOutlet UIView *menu;
    __weak IBOutlet UIView *notificaciones;
    NSDictionary *usuario;
    __weak IBOutlet UIView *menuppal;
    NSString * language;
    NSArray *contentsPaisArray;
    NSString *language_id;
    NSArray *dataSource;
    NSArray *beneficiarios;
    NSArray *parentezcoArray;
}
@property (weak, nonatomic) IBOutlet UIImageView *icon1;
@property (weak, nonatomic) IBOutlet UIImageView *splash;
@property (weak, nonatomic) IBOutlet UIImageView *icon2;
@property (weak, nonatomic) IBOutlet UIButton *btncomprar;
@property (weak, nonatomic) IBOutlet UIImageView *foto;
@property (weak, nonatomic) IBOutlet UIImageView *fotopeq;
@property (weak, nonatomic) IBOutlet UILabel *nombremenu;
@property (weak, nonatomic) IBOutlet UIView *vistaopaca;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIImageView *progressbar;
@property (weak, nonatomic) IBOutlet UILabel *pais;
@property (weak, nonatomic) IBOutlet UILabel *mail;
@property (weak, nonatomic) IBOutlet UILabel *progresoText;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIProgressView *progressview;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UILabel *LBL1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UILabel *lbl3;
@property (weak, nonatomic) IBOutlet UILabel *lbl4;
@property (weak, nonatomic) IBOutlet UILabel *lbl5;
@property (weak, nonatomic) IBOutlet UILabel *lbl6;
@property (weak, nonatomic) IBOutlet UILabel *lbl7;
@property (weak, nonatomic) IBOutlet UILabel *lbl8;
@property (weak, nonatomic) IBOutlet UILabel *lbl9;
@property (weak, nonatomic) IBOutlet UILabel *lbl10;
@property (weak, nonatomic) IBOutlet UILabel *lbl11;
@property (weak, nonatomic) IBOutlet UITableView *mytableview;


@end

@implementation HomeViewController

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void)viewWillAppear:(BOOL)animated {

    /*
     Verifico el idioma del teléfono
     */
    
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    [_progressview setProgress:1 animated:NO];
    language_id = @"1";
    if([language rangeOfString:@"es"].location == NSNotFound) {
        language_id = @"2";
        _LBL1.text = @"Buy space";
        [_btncomprar setTitle:@"Buy space" forState:UIControlStateNormal];
        _progresoText.text=@"You have 40Mb available to create new messages";
        [_btn1 setTitle:@"VIEW HISTORY" forState:UIControlStateNormal];
        [_btn2 setTitle:@"NEW MESSAGE" forState:UIControlStateNormal];
        _lbl2.text =@"Beneficiary";
        _lbl3.text =@"Checker";
        _lbl4.text =@"Profile";
        _lbl5.text =@"Messages";
        _lbl6.text =@"New video message";
        _lbl7.text =@"New voice message";
        _lbl8.text =@"New text message";
        _lbl9.text =@"New picture message";
        _lbl10.text =@"History";
        _lbl11.text =@"Sign out";
    }
    
    /*
     Verifico la sesión del usuario
     */
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        _splash.hidden = YES;
        menu.frame = CGRectMake(0, -153, 306, 98); // Ajusto los tamaños del menú
        
        //Muestro la vista de cargando
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
        
        
    }
}

-(void) cargaPaises {
    
    NSString *urlString = @"http://fluie.com/fluie/api/country/";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        contentsPaisArray=arrayresponse;
        for (int i=0; i<contentsPaisArray.count; i++) {
            NSDictionary *diccionario = [contentsPaisArray objectAtIndex:i];
            NSLog(@"diccionario[idcountry]: %@", diccionario[@"idcountry"]);
            NSLog(@"usuario: %@", usuario);
            if([diccionario[@"idcountry"] isEqualToString: usuario[@"country_id"]]){
                _pais.text=diccionario[@"country"];
            }
            
        }
    }
    
    [self hideProgressHUD];
    
    
}

-(void)loadFreeSpace {
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/freeSpace/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"GET"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            if(returnData){
                NSArray *freeSpace = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                for(id space in freeSpace) {
                    _progresoText.text = [NSString stringWithFormat:@"Actualmente tienes %@Mb disponibles para crear mensajes.", space[@"free"]];
                    
                    if([language rangeOfString:@"es"].location == NSNotFound)
                        _progresoText.text = [NSString stringWithFormat:@"You have %@Mb available to create new messages", space[@"free"]];
                    
                    float progressVal = 0;
                    
                    NSLog(@"%@",space);
                    
                    if([space[@"space"] intValue]!=0){
                        progressVal = ([space[@"space"] floatValue] - [space[@"free"] floatValue])/[space[@"space"] floatValue];
                    }
                    
                    [_progressview setProgress:progressVal animated:YES];
                    
                    NSLog(@"%f",progressVal);
                    [_progressview setProgress:progressVal animated:YES];
                    
                    [self loadUserData]; //Cargo los datos del usuario
                    
                }
            }
        }else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
}

-(void)loadUserData {
    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            if(returnData){
                if(returnData){
                    NSDictionary *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    
                    usuario = response;
                    [[NSUserDefaults standardUserDefaults] setObject:usuario[@"gender_id"] forKey:@"gender_id"];
                    //_pais.text = usuario[@"pais"];
                    //NSLog(@"%@", usuario);
                    _name.text = [NSString stringWithFormat:@"%@ %@", usuario[@"name"], usuario[@"name_last"]];
                    _nombremenu.text = [NSString stringWithFormat:@"%@ %@", usuario[@"name"], usuario[@"name_last"]];
                    _mail.text = [NSString stringWithFormat:@"%@", usuario[@"email"]];
                    //[self getDataSource];
                    [self getFoto];
                }
                
            }
        }else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
        
        
    }];
    [dataTask resume];
}

-(void)viewDidAppear:(BOOL)animated
{
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"])
    {
        [self performSegueWithIdentifier:@"goLogin" sender:nil];
    }
    else
    {
        [self loadFreeSpace];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    
    NSString *filePath = [documentsPath stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
    NSError *error;
    /*BOOL success = [fileManager removeItemAtPath:filePath error:&error];
    if (success) {
        UIAlertView *removeSuccessFulAlert=[[UIAlertView alloc]initWithTitle:@"Congratulation:" message:@"Successfully removed" delegate:self cancelButtonTitle:@"Close" otherButtonTitles:nil];
        [removeSuccessFulAlert show];
    }
    else
    {
        NSLog(@"Could not delete file -:%@ ",[error localizedDescription]);
    }*/
    //[self setNeedsStatusBarAppearanceUpdate];
    
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closemenus)];
    
    UISwipeGestureRecognizer *leftSwipe = [[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(closemenus)];
    leftSwipe.direction = UISwipeGestureRecognizerDirectionLeft;
    
    [self.vistaopaca addGestureRecognizer:tap];
    [self.vistaopaca addGestureRecognizer:leftSwipe];
    [menuppal addGestureRecognizer:leftSwipe];
    
    _foto.layer.masksToBounds = YES;
    // border radius
    [_foto.layer setCornerRadius:50.0f];
    // border
    [_foto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_foto.layer setBorderWidth:2.5f];
    // drop shadow
    [_foto.layer setShadowColor:[UIColor whiteColor].CGColor];
    [_foto.layer setShadowOpacity:0.8];
    [_foto.layer setShadowRadius:3.0];
    [_foto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    _fotopeq.layer.masksToBounds = YES;
    // border radius
    [_fotopeq.layer setCornerRadius:26.0f];
    // border
    [_fotopeq.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [_fotopeq.layer setBorderWidth:2.5f];
    // drop shadow
    [_fotopeq.layer setShadowColor:[UIColor whiteColor].CGColor];
    [_fotopeq.layer setShadowOpacity:0.8];
    [_fotopeq.layer setShadowRadius:3.0];
    [_fotopeq.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
}

-(IBAction)logout:(id)sender{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"idUser"];
    [self performSegueWithIdentifier:@"goLogin" sender:nil];
}

- (UIStatusBarStyle) preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

-(void)viewWillDisappear:(BOOL)animated{
    menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    menu.frame = CGRectMake(6, -153, 306, 98);
    _vistaopaca.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return YES;
}
- (IBAction)onBtnMenu:(id)sender {
    //static Menu menu = MenuLeft;
    
    //[[SlideNavigationController sharedInstance] bounceMenu:menu withCompletion:nil];
}

-(void)closemenus {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.50];
    [UIView setAnimationDelegate:self];
    _vistaopaca.hidden=YES;
    menu.frame = CGRectMake(6, -153, self.view.frame.size.width-12,  menu.frame.size.height);
    menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    notificaciones.frame = CGRectMake(0, -500, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
    [UIView commitAnimations];
}

-(IBAction)showmenu:(id)sender
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.50];
    [UIView setAnimationDelegate:self];
    
    menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    notificaciones.frame = CGRectMake(0, -500, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
    if(menu.frame.origin.y != _vistaopaca.frame.origin.y){
        _vistaopaca.hidden=YES;
        menu.frame = CGRectMake(6, _vistaopaca.frame.origin.y, self.view.frame.size.width-12,  menu.frame.size.height);
    }else{
        _vistaopaca.hidden=NO;
        menu.frame = CGRectMake(6, -153, self.view.frame.size.width-12,  menu.frame.size.height);
    }
    [UIView commitAnimations];
    
    _vistaopaca.hidden=!_vistaopaca.hidden;
}

-(IBAction)showmenuppal:(id)sender
{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelegate:self];
    
    menu.frame = CGRectMake(6, -153, self.view.frame.size.width-12,  menu.frame.size.height);
    notificaciones.frame = CGRectMake(0, -500, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
    if(menuppal.frame.origin.x != 0){
        _vistaopaca.hidden=NO;
        menuppal.frame = CGRectMake(0, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    }else{
        menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
        _vistaopaca.hidden=YES;
    }
    [UIView commitAnimations];
    
}

-(IBAction)shownotificaciones:(id)sender
{
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    [UIView setAnimationDelegate:self];
    
    menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
    menu.frame = CGRectMake(6, -153, self.view.frame.size.width-12,  menu.frame.size.height);
    
    if(notificaciones.frame.origin.y != _vistaopaca.frame.origin.y){
        _vistaopaca.hidden=NO;
        notificaciones.frame = CGRectMake(0, _vistaopaca.frame.origin.y, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
    }else{
        _vistaopaca.hidden=YES;
        notificaciones.frame = CGRectMake(0, -500, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
    }
    [UIView commitAnimations];

    
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    if ([segue.identifier isEqualToString:@"gotoperfil"]) {
        PerfilViewController *cntrlr = segue.destinationViewController;
        NSLog(@"%@", [_foto sd_imageURL]);
        cntrlr.fotoUrl = [_foto sd_imageURL];
    }
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

#pragma mark - TableView

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 63;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [_mytableview dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSMutableDictionary *diccionario = [dataSource[indexPath.row] mutableCopy];
    NSLog(@"%@", diccionario);
    UILabel *nombre = (UILabel *)[cell viewWithTag:101];
    nombre.text = diccionario[@"userFrom"][@"name"];
    
    UILabel *notificacion = (UILabel *)[cell viewWithTag:102];
    notificacion.text = diccionario[@"notification"];
    
    UIButton *botonReportar = (UIButton *)[cell viewWithTag:100];
    [botonReportar addTarget:self
                    action:@selector(reportar:)
          forControlEvents:UIControlEventTouchUpInside];
    
    botonReportar.tag = indexPath.row;
    
    UIButton *botonBorrar = (UIButton *)[cell viewWithTag:110];
    [botonBorrar addTarget:self
                      action:@selector(borrarNotificacion:)
            forControlEvents:UIControlEventTouchUpInside];
    
    UILabel *falso = (UILabel *)[cell viewWithTag:2];
    
    if(![diccionario[@"type_notification_id"] isEqualToString:@"6"]) {
        [botonReportar setHidden:YES];
        [falso setHidden:YES];
    }
    
    botonBorrar.tag = indexPath.row;
    
    UIImageView *_miimageView = (UIImageView *)[cell viewWithTag:103];
    _miimageView.layer.masksToBounds = YES;
    [_miimageView.layer setCornerRadius:23.0f];
    [_miimageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_miimageView.layer setBorderWidth:0.5f];
    [_miimageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_miimageView.layer setShadowOpacity:0.8];
    [_miimageView.layer setShadowRadius:3.0];
    [_miimageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    NSLog(@"%@", diccionario);
    [_miimageView sd_setImageWithURL:[NSURL URLWithString:diccionario[@"userFrom"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
    
    return cell;
}

-(IBAction)reportar:(id)sender {
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    UIButton *tempotal = (UIButton *) sender;
    NSMutableDictionary *diccionario = [dataSource[tempotal.tag] mutableCopy];
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/user/reportPrincipal"];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    NSString *myRequestString = [NSString stringWithFormat:@"idcheckeruser=%@", diccionario[@"other_id"]];
    NSLog(@"requeststring: %@", myRequestString);
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    [requestFS setHTTPMethod: @"POST"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requestFS setHTTPBody: myRequestData];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Notification"
                         contentText:arrayresponse[@"msg"]
                         leftButtonTitle:@"Accept"
                         rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                 alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:arrayresponse[@"msg"]
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            alert.leftBlock = ^() {
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.25];
                [UIView setAnimationDelegate:self];
                
                menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
                menu.frame = CGRectMake(6, -153, self.view.frame.size.width-12,  menu.frame.size.height);
                
                if(notificaciones.frame.origin.y != _vistaopaca.frame.origin.y){
                    _vistaopaca.hidden=NO;
                    notificaciones.frame = CGRectMake(0, _vistaopaca.frame.origin.y, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
                }else{
                    _vistaopaca.hidden=YES;
                    notificaciones.frame = CGRectMake(0, -500, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
                }
                [UIView commitAnimations];
                [self getDataSource];
            };
        }
        else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
        

    }];
    [dataTask resume];
}

-(IBAction)borrarNotificacion:(id)sender {
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];        
    }
    UIButton *tempotal = (UIButton *) sender;
    NSMutableDictionary *diccionario = [dataSource[tempotal.tag] mutableCopy];
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/user/stateNotification"];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    NSString *myRequestString = [NSString stringWithFormat:@"idnotification=%@", diccionario[@"idnotification"]];
    NSLog(@"requeststring: %@", myRequestString);
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    [requestFS setHTTPMethod: @"POST"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requestFS setHTTPBody: myRequestData];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
            NSLog(@"%@", responsestring);
            NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                 alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:arrayresponse[@"msg"]
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                 alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:arrayresponse[@"msg"]
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            
            alert.leftBlock = ^() {
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.25];
                [UIView setAnimationDelegate:self];
                
                menuppal.frame = CGRectMake(0 - menuppal.frame.size.width, _vistaopaca.frame.origin.y, menuppal.frame.size.width, menuppal.frame.size.height);
                menu.frame = CGRectMake(6, -153, self.view.frame.size.width-12,  menu.frame.size.height);
                
                if(notificaciones.frame.origin.y != _vistaopaca.frame.origin.y){
                    _vistaopaca.hidden=NO;
                    notificaciones.frame = CGRectMake(0, _vistaopaca.frame.origin.y, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
                }else{
                    _vistaopaca.hidden=YES;
                    notificaciones.frame = CGRectMake(0, -500, notificaciones.frame.size.width-12,  notificaciones.frame.size.height);
                }
                [UIView commitAnimations];
                [self getDataSource];
            };
        }else{
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
        
        
    }];
    [dataTask resume];
}

-(void)getFoto {
    /*
     * Traigo la foto del usuario
     */
    
    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            //NSString *foto = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:kCFStringEncodingUTF8];
            NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSLog(@"response %@",foto);
            
            if([usuario[@"gender_id"] isEqualToString:@"1"]){
                [_foto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", foto[@"image"]]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
                [_fotopeq sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", foto[@"image"]]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_foto sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", foto[@"image"]]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
                [_fotopeq sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", foto[@"image"]]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
        }
        [self getDataSource];
    }];
    [dataTask resume];
    
    
    
}
-(void)getDataSource {
    /*
     * Hago el request de las notificaciones
     */
    
    NSString *myRequestString = [NSString stringWithFormat:@"user_id=%@&language_id=%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], language_id];
    NSLog(@"requeststring: %@", myRequestString);
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/notification/getNotifications/"];
    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"POST"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requestFS setHTTPBody: myRequestData];
    
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSLog(@"%@", arrayresponse);
            dataSource = arrayresponse;
            [_mytableview reloadData];
        }
        else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
        [self cargaPaises];

    }];
    [dataTask resume];
}
@end
