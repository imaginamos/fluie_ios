//
//  RecuperarViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/30/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "RecuperarViewController.h"
#import "AlmosaferAlertView.h"

@interface RecuperarViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usrTextField;

@end

@implementation RecuperarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)recuperar:(id)sender{
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user/changePassword";
    NSString *myRequestString = [NSString stringWithFormat:@"email=%@",_usrTextField.text];
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if([arrayresponse[@"status"] isEqualToString:@"ok"]){
        
        
        NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user/";
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"GET"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        if(returnData){
            NSDictionary *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSLog(@"%@", response);
            
        }
        
        [self performSegueWithIdentifier:@"logged" sender:nil];
    }else if([arrayresponse[@"status"] isEqualToString:@"err"]){
        NSLog(@"%@", arrayresponse);
        NSString *respuesta=arrayresponse[@"message"];
        /*id val = nil;
        NSArray *values = [arrayresponse[@"message"] allValues];
        if ([values count] != 0)
            val = [values objectAtIndex:0];
        
        NSString *respuesta=@"";
        if([val isKindOfClass:[NSArray class]]){
            respuesta = [val objectAtIndex:0];
        }else if([val isKindOfClass:[NSString class]]){
            respuesta = val;
        }else{
            respuesta = val;
        }*/
        
        
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Recordar Contraseña"
                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
        };
        
    }
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
