//
//  LoginViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/20/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "LoginViewController.h"
#import "BienvenidoViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"

@interface LoginViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UILabel *lbl3;
@property (weak, nonatomic) IBOutlet UIButton *btncrearcuenta;
@property (weak, nonatomic) IBOutlet UIButton *btniniciarsesion;

@property (weak, nonatomic) IBOutlet UITextField *usrTextField;
@property (weak, nonatomic) IBOutlet UITextField *pswrdTextField;
@property (strong, nonatomic) MBProgressHUD *progressHUD;


@end

@implementation LoginViewController

-(void)viewDidAppear:(BOOL)animated{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        [self dismissViewControllerAnimated:YES completion:nil];
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    [self setNeedsStatusBarAppearanceUpdate];
    
    UIColor *color = [UIColor whiteColor];
    _usrTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    
    _pswrdTextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Contraseña" attributes:@{NSForegroundColorAttributeName: color}];

    
    // Do any additional setup after loading the view.
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _lbl1.text = @"Forgot something?";
        _lbl2.text = @"Forgot something? Remember password";
        _lbl3.text = @"© 2015 Flüie - All rights reserved";
        [_btniniciarsesion setTitle:@"Login" forState:UIControlStateNormal];
        [_btncrearcuenta setTitle:@"Sign in" forState:UIControlStateNormal];
    }
}


-(void)dismissKeyboard {
    [_usrTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}



-(IBAction)login:(id)sender {
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Iniciando Sesión";
        _progressHUD.dimBackground = YES;
    }

    
        NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user/login";
        NSString *myRequestString = [NSString stringWithFormat:@"email=%@&password=%@",_usrTextField.text, _pswrdTextField.text];
        NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
         NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"POST"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody: myRequestData];
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
        NSLog(@"response %@",responsestring);
        
        NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        
        if([arrayresponse[@"status"] isEqualToString:@"err"]){
            [self hideProgressHUD];
            id val = nil;
            NSArray *values = [arrayresponse[@"message"] allValues];
            if ([values count] != 0)
                val = [values objectAtIndex:0];
            
            NSString *respuesta=@"";
            if([val isKindOfClass:[NSArray class]]){
                respuesta = [val objectAtIndex:0];
            }else if([val isKindOfClass:[NSString class]]){
                respuesta = val;
            }else{
                respuesta = val;
            }
            
            
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Ingreso"
                                         contentText:[NSString stringWithFormat:@"%@", respuesta]
                                         leftButtonTitle:@"Aceptar"
                                         rightButtonTitle:nil];
            [alert showInView:self.view];
            
            alert.leftBlock = ^() {
                NSLog(@"left button clicked");
            };
           
        }else{
            
           [[NSUserDefaults standardUserDefaults] setObject:arrayresponse[@"iduser"] forKey:@"idUser"];
            
            [self hideProgressHUD];
            
            [self dismissViewControllerAnimated:YES completion:nil];
            //[self performSegueWithIdentifier:@"logged" sender:nil];
        }
    
}

#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
}

@end
