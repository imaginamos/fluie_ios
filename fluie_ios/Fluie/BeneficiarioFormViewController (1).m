//
//  BeneficiarioFormViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "BeneficiarioFormViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"


@interface BeneficiarioFormViewController () {
    bool masculino;
    UITextField *mytextField;
    NSArray *contentsParentezcoArray;
    NSArray *usuariosArray;
    NSDictionary *recienCreado;
    NSString *parentezcoSelected;
    bool match;
    int beneficiarioId;

}

@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UIButton *parentezco_Button;
@property (weak, nonatomic) IBOutlet UITextField *nombresTxt;
@property (weak, nonatomic) IBOutlet UITextField *apellidosTxt;
@property (weak, nonatomic) IBOutlet UITextField *correoTxt;
@property (weak, nonatomic) IBOutlet UITextField *parentescoTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnMasculino;
@property (weak, nonatomic) IBOutlet UIButton *btnFemenino;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *reloj;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UITextField *code;
@property (weak, nonatomic) IBOutlet UITextField *telefonoTxt;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIButton *btn1;

@property (weak, nonatomic) IBOutlet UIButton *btn2;

@property (weak, nonatomic) IBOutlet UILabel *lbl3;
@property (weak, nonatomic) IBOutlet UILabel *lbl4;

@end

@implementation BeneficiarioFormViewController


-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        _lbl1.text = @"Fill the beneficiary information";
        _nombresTxt.placeholder = @"Name";
        _apellidosTxt.placeholder = @"Last Name";
        _correoTxt.placeholder = @"Email";
        _parentescoTxt.placeholder = @"Relation";
        _telefonoTxt.placeholder =@"Phone";
        _lbl2.text=@"Gender";
        _lbl3.text=@"Male";
        _lbl4.text=@"Female";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        [_btn2 setTitle:@"DELETE" forState:UIControlStateNormal];
        
    }
}
- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    [_reloj setHidden:YES];
    [_reloj stopAnimating];
    masculino = TRUE;
    match = FALSE;
    // Do any additional setup after loading the view.
    [self cargaParentezco];
    if(_beneficiario!=nil){
        NSLog(@"%@", _beneficiario);
        beneficiarioId = [_beneficiario[@"user_id"] intValue];
        _nombresTxt.text = _beneficiario[@"user"][@"name"];
        _apellidosTxt.text = _beneficiario[@"user"][@"name_last"];
        _correoTxt.text = _beneficiario[@"user"][@"email"];
        _telefonoTxt.text = _beneficiario[@"user"][@"phone"];
        _code.text = _beneficiario[@"code"];
        _parentezco_Button.titleLabel.text = _beneficiario[@"relationship"];
        [_parentezco_Button setTitle:_beneficiario[@"relationship"] forState:UIControlStateNormal];
        
    }
    
    dropDownPregrado.delegate = self;
}

-(void) cargaParentezco {
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/relationship";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        contentsParentezcoArray=arrayresponse;
    }
    
    [self cargarUsuarios];
}

-(void)cargarUsuarios {
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user/";
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    if(returnData){
        NSArray *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        if(response.count){
            usuariosArray=response;
        }
    }
}

- (IBAction)onPais:(id)sender
{
    [mytextField resignFirstResponder];
    
    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsParentezcoArray valueForKey:@"relationship"];
    if(dropDownPregrado == nil) {
        CGFloat f = 80;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}


-(void)dismissKeyboard {
    [_correoTxt resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)borrar:(id)sender{
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Borrando beneficiario";
        
    }
    
    if(beneficiarioId){
        
        NSString *urlString = [NSString stringWithFormat:@"http://mentesweb.com/fluie/programacion/api/beneficiaryUser/%i", beneficiarioId];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"DELETE"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        if(returnData){
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
            
            
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Beneficiario"
                                         contentText:@"El beneficiario se ha borrado"
                                         leftButtonTitle:@"Aceptar"
                                         rightButtonTitle:nil];
            [alert showInView:self.view];
            
            alert.leftBlock = ^() {
                NSLog(@"left button clicked");
                [self dismissViewControllerAnimated:YES completion:nil];
            };
        }
        
    }else{
        _nombresTxt.text = @"";
        _correoTxt.text = @"";
        _apellidosTxt.text = @"";
        _parentezco_Button.titleLabel.text = @"";
        _parentescoTxt.placeholder = @"Parentezco";

    }
    
    
}

-(IBAction)guardar:(id)sender{
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Guardando información";
        
    }
    
    if(_beneficiario!=nil){
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Beneficiario"
                                     contentText:@"El beneficiario se ha actualizado con éxito"
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
            //[self dismissViewControllerAnimated:YES completion:nil];
        };

    }else
        for(id user in usuariosArray) {
            NSLog(@"user: %@", user);
            NSLog(@"user email: %@", user[@"email"]);
            NSLog(@"texto email: %@", _correoTxt.text);
            if ([_correoTxt.text isEqualToString:user[@"email"]]) {
                match = TRUE;
                NSLog(@"user email: %@", user[@"email"]);
                
                    
                    //Creo el beneficiario y lo asigno a mi usuario
                NSLog(@"%@", _parentezco_Button.titleLabel.text);
                    if(![_parentezco_Button.titleLabel.text isEqualToString:@""]){
                        for (int i=0; i<contentsParentezcoArray.count; i++) {
                            NSDictionary *diccionario = [contentsParentezcoArray objectAtIndex:i];
                            
                            if([diccionario[@"relationship"] isEqualToString:[parentezcoSelected stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                                parentezcoSelected = diccionario[@"idrelationship"];
                        }
                        
                    }
                    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/beneficiaryUser";
                    NSString *myRequestString = [NSString stringWithFormat:@"beneficiary_id=%@&user_id=%@&code=asdfg&relationship_id=%@",user[@"iduser"], [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], parentezcoSelected];
                    
                    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                    [request setHTTPMethod: @"POST"];
                    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                    [request setHTTPBody: myRequestData];
                    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                    if(returnData){
                        NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
                        NSLog(@"response %@",responsestring);
                        NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];

                        if(!responseDic[@"error"]){
                            
                            _nombresTxt.text = @"";
                            _correoTxt.text = @"";
                            _apellidosTxt.text = @"";
                            _code.text = @"";
                            _parentezco_Button.titleLabel.text = @"";
                            _parentescoTxt.placeholder = @"Parentezco";
                            
                            [_reloj setHidden:YES];
                            [_reloj stopAnimating];
                            [self hideProgressHUD];
                            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                         initWithTitle:@"Beneficiario"
                                                         contentText:@"El beneficiario se ha guardado con éxito"
                                                         leftButtonTitle:@"Aceptar"
                                                         rightButtonTitle:nil];
                            [alert showInView:self.view];
                            
                            alert.leftBlock = ^() {
                                NSLog(@"left button clicked");
                                //[self dismissViewControllerAnimated:YES completion:nil];
                            };
                            
                            
                            
                        }else if(responseDic[@"error"]){
                            id val = nil;
                            NSArray *values = [responseDic[@"error"] allValues];
                            if ([values count] != 0)
                                val = [values objectAtIndex:0];
                            
                            NSLog(@"%@", val);
                            NSString *respuesta=@"";
                            if([val isKindOfClass:[NSArray class]]){
                                respuesta = [val objectAtIndex:0];
                            }else if([val isKindOfClass:[NSString class]]){
                                respuesta = val;
                            }else{
                                respuesta = val;
                            }
                            NSLog(@"%@", respuesta);
                            [self hideProgressHUD];
                            
                            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                         initWithTitle:@"Beneficiario"
                                                         contentText:[NSString stringWithFormat:@"%@", respuesta]
                                                         leftButtonTitle:@"Aceptar"
                                                         rightButtonTitle:nil];
                            [alert showInView:self.view];
                            
                            alert.leftBlock = ^() {
                                NSLog(@"left button clicked");
                                
                            };
                        
                        }
                    }
                break;
                
            }
            
        }
    
    if(!match){
        //Si llego aquí es porque debo crear al usuario
        if(![_parentezco_Button.titleLabel.text isEqualToString:@""]){
            for (int i=0; i<contentsParentezcoArray.count; i++) {
                NSDictionary *diccionario = [contentsParentezcoArray objectAtIndex:i];
                
                if([diccionario[@"relationship"] isEqualToString:[parentezcoSelected stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                    parentezcoSelected = diccionario[@"idrelationship"];
            }
            
        }
        NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user";
        NSString *myRequestString = [NSString stringWithFormat:@"space=0&identification=checker&pass=Beneficiario&confirm_password=Beneficiario&terms=1&email=%@&name=%@&name_last=%@&phone=%@&address=checker",_correoTxt.text, _nombresTxt.text, _apellidosTxt.text, _telefonoTxt.text];
        NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"POST"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody: myRequestData];
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        if(returnData){
            [self cargarUsuarios];
            
            for(id usuario in usuariosArray) {
                if ([usuario[@"email"] isEqualToString:_correoTxt.text]) {
                    recienCreado = usuario;
                }
            }
            NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            if(!arrayresponse[@"error"]){
                
                NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/beneficiaryUser";
                NSString *myRequestString = [NSString stringWithFormat:@"beneficiary_id=%@&user_id=%@&code=%@&relationship_id=%@",recienCreado[@"iduser"], [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _code.text, parentezcoSelected];
                
                NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                [request setHTTPMethod: @"POST"];
                [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                [request setHTTPBody: myRequestData];
                NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                if(returnData){
                    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
                    NSLog(@"response %@",responsestring);
                    NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    
                    if(!responseDic[@"error"]){
                        
                        _nombresTxt.text = @"";
                        _correoTxt.text = @"";
                        _apellidosTxt.text = @"";
                        _parentezco_Button.titleLabel.text = @"";
                        _parentescoTxt.placeholder = @"Parentezco";
                        
                        [_reloj setHidden:YES];
                        [_reloj stopAnimating];
                        [self hideProgressHUD];
                        
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Beneficiario"
                                                     contentText:@"El beneficiario se ha guardado con éxito"
                                                     leftButtonTitle:@"Aceptar"
                                                     rightButtonTitle:nil];
                        [alert showInView:self.view];
                        
                        alert.leftBlock = ^() {
                            NSLog(@"left button clicked");
                            [self dismissViewControllerAnimated:YES completion:nil];
                        };
                        
                    }else if(responseDic[@"error"]){
                        id val = nil;
                        NSArray *values = [responseDic[@"error"] allValues];
                        if ([values count] != 0)
                            val = [values objectAtIndex:0];
                        
                        NSLog(@"%@", val);
                        NSString *respuesta=@"";
                        if([val isKindOfClass:[NSArray class]]){
                            respuesta = [val objectAtIndex:0];
                        }else if([val isKindOfClass:[NSString class]]){
                            respuesta = val;
                        }else{
                            respuesta = val;
                        }
                        NSLog(@"%@", respuesta);
                        [self hideProgressHUD];
                        
                        
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Beneficiario"
                                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                                     leftButtonTitle:@"Aceptar"
                                                     rightButtonTitle:nil];
                        [alert showInView:self.view];
                        
                        alert.leftBlock = ^() {
                            NSLog(@"left button clicked");
                            
                        };
                        
                        
                    }
                }
                //[self performSegueWithIdentifier:@"logged" sender:nil];
            }else if(arrayresponse[@"error"]){
                id val = nil;
                NSArray *values = [arrayresponse[@"error"] allValues];
                if ([values count] != 0)
                    val = [values objectAtIndex:0];
                
                NSLog(@"%@", val);
                NSString *respuesta=@"";
                if([val isKindOfClass:[NSArray class]]){
                    respuesta = [val objectAtIndex:0];
                }else if([val isKindOfClass:[NSString class]]){
                    respuesta = val;
                }else{
                    respuesta = val;
                }
                NSLog(@"%@", respuesta);
                [self hideProgressHUD];
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Beneficiario"
                                             contentText:[NSString stringWithFormat:@"%@", respuesta]
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
                
                alert.leftBlock = ^() {
                    NSLog(@"left button clicked");
                    
                };
            }
        }

    }
}

-(IBAction)onMasculino:(id)sender{
    NSLog(@"masculino");
    
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnMasculino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnFemenino setImage:nil forState:UIControlStateNormal];

}

-(IBAction)onFemenino:(id)sender{
    NSLog(@"femenino");
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnFemenino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnMasculino setImage:nil forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    /* if (textField == _Username_TextField) {
     [_Username_TextField resignFirstResponder];
     [_password_TextField becomeFirstResponder];
     } else if (textField == _password_TextField) {*/
    // here you can define what happens
    // when user presses return on the email field
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [textField resignFirstResponder];
    // }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    mytextField = textField;
     [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509+253)];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark NIDropDown

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [_parentescoTxt setPlaceholder:@""];
    
    parentezcoSelected = titulo;
    if(sender == _parentezco_Button)
        [[NSUserDefaults standardUserDefaults] setObject:titulo forKey:@"parentezco_Button"];
    
    //[self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDownPregrado = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
