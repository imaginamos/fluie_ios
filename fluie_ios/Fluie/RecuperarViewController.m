//
//  RecuperarViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/30/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "RecuperarViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"

@interface RecuperarViewController (){
    UITextField *mytextField;
}
@property (weak, nonatomic) IBOutlet UITextField *usrTextField;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UIButton *recordarBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (strong, nonatomic) MBProgressHUD *progressHUD;


@end

@implementation RecuperarViewController

-(void)viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _lbl1.text = @"Remember your password";
        _usrTextField.placeholder = @"Email";
        [_recordarBtn setTitle:@"REMEMBER" forState:UIControlStateNormal];
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)recuperar:(id)sender{
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.dimBackground = YES;
    }
    
    NSString *language1;
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        language1 =@"2";
    }
    else
    {
        language1 =@"1";
    }
    
    NSString *urlString = @"http://fluie.com/fluie/user/recover";
    
    NSLog(@"Link%@",urlString);
    NSString *myRequestString = [NSString stringWithFormat:@"email=%@&language=%@",_usrTextField.text,language1];
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSLog(@"Parameter%@",myRequestData);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil)
        {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
            NSLog(@"response %@",responsestring);
    
            NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Recover password"
                                             contentText:[NSString stringWithFormat:@"%@", arrayresponse[@"msg"]]
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Recuperar Contraseña"
                                             contentText:[NSString stringWithFormat:@"%@", arrayresponse[@"msg"]]
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
           
            
        }
        else
        {
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Recover password"
                                             contentText:@"Connection error"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Recuperar Contraseña"
                                             contentText:@"Error de conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }

            
        }
        
        [self hideProgressHUD];
    }];
    [dataTask resume];
    
}

#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.view setFrame:CGRectMake(0,-110,self.view.frame.size.width,self.view.frame.size.height)];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    
}

-(void)dismissKeyboard {
    [mytextField resignFirstResponder];
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
