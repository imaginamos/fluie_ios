//
//  BeneficiariosListViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeneficiariosListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@end
