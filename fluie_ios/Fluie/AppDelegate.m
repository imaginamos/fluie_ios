//
//  AppDelegate.m
//  Fluie
//
//  Created by Carlos Robinson on 11/20/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "AppDelegate.h"
#import "PayPalMobile.h"
#import "BienvenidoViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [PayPalMobile initializeWithClientIdsForEnvironments:@{PayPalEnvironmentProduction : @"APP-16893367P06764143", PayPalEnvironmentSandbox : @"APP-80W284485P519543T"}];
    // Override point for customization after application launch.
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"])
    {
        
         [[NSUserDefaults standardUserDefaults] setObject:@"alertyes" forKey:@"checkvalue"];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        BienvenidoViewController *viewController = (BienvenidoViewController *)[storyboard instantiateViewControllerWithIdentifier:@"welcomeScreen"];
        [self.window makeKeyAndVisible];
        [self.window.rootViewController presentViewController:viewController
                                                     animated:NO
                                                   completion:nil];
    }
    else
    {
        
        [[NSUserDefaults standardUserDefaults] setObject:@"alertno" forKey:@"checkvalue"];
    }
    
//    NSURL *ubiq = [[NSFileManager defaultManager]
//                   URLForUbiquityContainerIdentifier:nil];
//    if (ubiq) {
//        NSLog(@"iCloud access at %@", ubiq);
//        // TODO: Load document...
//    } else {
//        NSLog(@"No iCloud access");
//    }
//    
//    UIAlertView *alert =[[UIAlertView alloc]initWithTitle:@"complete" message:nil delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil ];
//    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
//                if ([savedValue isEqualToString:@"alertyes"])
//                {
//                    [alert showInView:self.view];
//                }
//                else
//                {
//                    [alert show];
//                };
    
    return YES;
    
   
}

- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     andbox ID:APP-80W284485P519543T
     Live App ID:APP-16893367P06764143
     */
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

@end
