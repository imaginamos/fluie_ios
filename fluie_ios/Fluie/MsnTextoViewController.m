//
//  MsnTextoViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/12/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "MsnTextoViewController.h"
#import "AlmosaferAlertView.h"
#import "UIImage+Resize.h"
#import "MBProgressHUD.h"
#import <CloudKit/CloudKit.h>
#import "ZipArchive.h"
#import "HistorialViewController.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"


@interface MsnTextoViewController ()
{
    BOOL isPhotoSelected;
    BOOL borradorBool;
    UIDatePicker *datePicker;
    NSDate *date;
    UIImage *photoSelected;
    NSData *imageData;
    NSString* fullPath;
    UIToolbar *keyboardToolbar;
    NSMutableArray * receptores;
    NSString *receptoresString;
    NSArray *beneficiarios;
    __weak IBOutlet UIButton *addButon;
    __weak IBOutlet UIButton *attachbtn;
    NIDropDown *dropDown;
    UITapGestureRecognizer *tap;
    NSString * language;
}

@property (weak, nonatomic) IBOutlet UITextView *descripion;
@property (weak, nonatomic) IBOutlet UILabel *agregar;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UITextField *fecha;
@property (weak, nonatomic) IBOutlet UIScrollView *myscrollview;
@property (weak, nonatomic) IBOutlet UITextField *usrTextField;
@property (weak, nonatomic) IBOutlet UITextView *usrTextView;
@property (weak, nonatomic) IBOutlet UITextField *titulo;
@property (weak, nonatomic) IBOutlet UITextField *documento;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UILabel *tooltip;
@property (weak, nonatomic) IBOutlet UIProgressView *progressview;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (strong) NSMetadataQuery *query;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *url;
@property (weak, nonatomic) IBOutlet UIScrollView *receptoresScroll;
@property (weak, nonatomic) IBOutlet UILabel *selectPlaceholder;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;

@end

@implementation MsnTextoViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [_documento setUserInteractionEnabled:NO];
    // Do any additional setup after loading the view.
    
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _lbl1.text = @"Used space";
        _titulo.placeholder = @"Message title";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        _selectPlaceholder.text = @"Program to";
        _agregar.text = @"Add beneficiary";
        [attachbtn setTitle:@"ATTACH" forState:UIControlStateNormal];
        _tooltip.text = @"Write a message";
        
    }
    //[self setNeedsStatusBarAppearanceUpdate];
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    receptores = [[NSMutableArray alloc] init];
    for (UIView *aView in [_receptoresScroll subviews]){
        _agregar.text = @"Agregar beneficiario";
        if([language rangeOfString:@"es"].location == NSNotFound)
            _agregar.text = @"Add beneficiary";
        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
            [aView removeFromSuperview];
        }
    }
    if (keyboardToolbar == nil) {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar setBarTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Cerrar" style:UIBarButtonItemStyleDone target:self action:@selector(closeKeyboard:)];
        
        [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    
    _fecha.inputAccessoryView = keyboardToolbar;
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    _fecha.inputView = datePicker;
    
    if(_esBorrador){
        NSLog(@"%@", _borradorDiccionario[@"title"]);
        NSLog(@"%@", _borradorDiccionario[@"fecha"]);
        NSLog(@"%@", _borradorDiccionario[@"receptores"]);

       _selectPlaceholder.text = @"";
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:_borradorDiccionario[@"archivo"]];
        
        [_borradorDiccionario[@"data"] writeToFile:path atomically:YES];
        fullPath = path;
        
        imageData = _borradorDiccionario[@"data"];
        _titulo.text = _borradorDiccionario[@"title"];
        _fecha.text = _borradorDiccionario[@"fecha"];
        receptores = [_borradorDiccionario[@"receptores"] mutableCopy];
        _descripion.text = _borradorDiccionario[@"description"];
        _documento.text = _borradorDiccionario[@"file"];
        
        if (_borradorDiccionario[@"fecha"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"fecha"];
            
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }
        
        _tooltip.text = @"";
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";
            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
            if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                [aView removeFromSuperview];
            }
        }
        
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text = @"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
    }else if(_borradorDiccionario!=nil){
        _tooltip.text = @"";
        _selectPlaceholder.text = @"";
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:_borradorDiccionario[@"archivo"]];
        
        NSURL *imageURL = [NSURL URLWithString:_borradorDiccionario[@"messageFiles"][0][@"url"]];
        NSLog(@"%@", _borradorDiccionario);
        NSData *dataFile = [NSData dataWithContentsOfURL:imageURL];
        
        [dataFile writeToFile:path atomically:YES];
        fullPath = path;
        
        imageData = dataFile;
        _titulo.text = _borradorDiccionario[@"title"];
        _descripion.text = _borradorDiccionario[@"description"];
        NSLog(@"%@", _borradorDiccionario[@"date_view"]);
        if (_borradorDiccionario[@"date_view"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"date_view"];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }
        _documento.text = _borradorDiccionario[@"messageFiles"][0][@"file"];
        
        [self getBeneficiarios];
    }
    
}

-(void)dismissKeyboard {
    if(_usrTextField!=nil)
        [_usrTextField resignFirstResponder];
    
    if(_usrTextView!=nil)
        [_usrTextView resignFirstResponder];
}



- (IBAction)openImportDocumentPicker:(id)sender {
    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:@[@"public.composite-content", @"public.data", @"public.text"] inMode:UIDocumentPickerModeImport];
    documentPicker.delegate = self;
    
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentAtURL:(NSURL *)url {
    if (controller.documentPickerMode == UIDocumentPickerModeImport) {
        /*NSString *alertMessage = [NSString stringWithFormat:@"Successfully imported %@", [url lastPathComponent]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:@"Import"
                                                  message:alertMessage
                                                  preferredStyle:UIAlertControllerStyleAlert];
            [alertController addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
            [self presentViewController:alertController animated:YES completion:nil];
        
        });*/
        fullPath = [url path];
        
        /*NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* dPath = [paths objectAtIndex:0];
        
        NSString* zipfile = [dPath stringByAppendingPathComponent:@"test.zip"] ;
        SSZipArchive* zip = [[SSZipArchive alloc] init];
        [SSZipArchive createZipFileAtPath: zipfile withContentsOfDirectory:fullPath ];
        */
        
        imageData = [NSData dataWithContentsOfFile:[url path]];
        _lbl2.text = [NSString stringWithFormat:@"%.2f MB", (float)imageData.length/1024.0f/1024.0f];
        NSLog(@"%@", imageData);
        if(imageData)
            _documento.text = [NSString stringWithFormat:@"%@", [url lastPathComponent]];
        else
        {
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Message"
                         contentText:@"The file you want to upload is not allowed"
                         leftButtonTitle:@"Accept"
                         rightButtonTitle:nil];
                
            }
            else
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Mensaje"
                         contentText:@"El archivo que desea subir no está permitido"
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
            }
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            alert.leftBlock = ^() {
                [self performSegueWithIdentifier:@"gotoHistorialDoc" sender:nil];
                //[self performSegueWithIdentifier:@"gotoHistorialDoc" sender:nil]; //[self.navigationController popViewControllerAnimated:YES];
            };
        }
        
    }
}

- (IBAction)openExportDocumentPicker:(id)sender {
    UIDocumentPickerViewController *documentPicker = [[UIDocumentPickerViewController alloc] initWithURL:[[NSBundle mainBundle] URLForResource:@"image" withExtension:@"jpg"]
                                                                                                  inMode:UIDocumentPickerModeExportToService];
    documentPicker.delegate = self;
    documentPicker.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:documentPicker animated:YES completion:nil];
}

-(void) viewWillAppear:(BOOL)animated {
    
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void) PlayerAdded:(BeneficiariosListViewController *)controller andPlayer:(NSDictionary *)beneficiario
{
    if(receptores==nil)
        receptores = [[NSMutableArray alloc] init];
    
    if(![receptores containsObject:beneficiario])
        [receptores addObject:beneficiario];
    
    NSLog(@"%@", receptores);
    for (UIView *aView in [_receptoresScroll subviews]){
        _agregar.text = @"Agregar beneficiario";
        if([language rangeOfString:@"es"].location == NSNotFound)
            _agregar.text = @"Add beneficiary";
        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
            [aView removeFromSuperview];
        }
    }
    
    UIImageView *_miimageView = [[UIImageView alloc] init];
    
    for(int i = 0; i<[receptores count]; i++){
        _agregar.text = @"";
        NSDictionary *receptDictionary = [receptores objectAtIndex:i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:i];
        [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
        button.clipsToBounds = YES;
        
        button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
        button.layer.borderColor=[UIColor grayColor].CGColor;
        button.layer.borderWidth=0.5f;
        
        if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
        }else{
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
        }
        
        [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
        
        UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
        [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
        [_receptoresScroll addSubview:button];
        [_receptoresScroll addSubview:nombreReceptor];
        [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)deleteUser:(id)sender
{
    UIButton * btn = (UIButton *)sender;
    
    AlmosaferAlertView *alert;
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirm"
                 contentText:@"You want to delete this message Beneficiary?"
                 leftButtonTitle:@"Yes"
                 rightButtonTitle:@"No"];
    }
    else
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirmar"
                 contentText:@"Desea borrar de este mensaje al Beneficiario?"
                 leftButtonTitle:@"Si"
                 rightButtonTitle:@"No"];
        
    }
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
    alert.leftBlock = ^() {
        [receptores removeObjectAtIndex:btn.tag];
        
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";
            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
            if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                [aView removeFromSuperview];
            }
        }
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text=@"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
    };
    
}

-(IBAction)onAction:(id)sender {
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        NSLog(@"connection unavailable");
        [self guardarvideo];
    }
    else
    {
        //connection available
        NSLog(@"connection available");
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
        
        if (receptores.count>0) {
            [self guardarvideo];
        }else{
            [self hideProgressHUD];
            
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Text message"
                         contentText:@"You must select who to send the message"
                         leftButtonTitle:@"Accept"
                         rightButtonTitle:nil];
                
            }
            else
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Mensaje de Fotos"
                         contentText:@"Debe seleccionar a quién enviarle el mensaje"
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
            }
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
        }
    }
}

- (void)guardarvideo {
    borradorBool = NO;
    if(_esBorrador){
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
        
        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
        if(jsondata!=nil){
            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
            for (int i = 0; i<json.count; i++) {
                NSMutableDictionary *diccionario = [json objectAtIndex:i];
                if(diccionario == _borradorDiccionario){
                    [json removeObjectAtIndex:i];
                }
            }
        }
    }
    
    receptoresString = @"";
    for (int i = 0; i<receptores.count; i++) {
        if([receptoresString isEqualToString:@""])
            receptoresString = [receptores objectAtIndex:i][@"beneficiary"][@"iduser"];
        else
            receptoresString = [NSString stringWithFormat:@"%@,%@", receptoresString, [receptores objectAtIndex:i][@"beneficiary"][@"iduser"]];
        
    }
  
        
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"] forKey:@"user_id"];
    [_params setObject:[NSString stringWithFormat:@"%@", _titulo.text] forKey:@"m[title]"];
    [_params setObject:@"3" forKey:@"m[type_file_id]"];
    [_params setObject:@"1" forKey:@"m[type_message_id]"];
    [_params setObject:[NSString stringWithFormat:@"%@", _fecha.text] forKey:@"m[date_view]"];
    [_params setObject:_descripion.text forKey:@"m[description]"];
    [_params setObject:receptoresString forKey:@"m[beneficiaries]"];
    
    NSString *urlString = @"http://fluie.com/fluie/messageRestFull/createMessage";
    if(_borradorDiccionario!=nil && !_esBorrador){
        urlString = @"http://fluie.com/fluie/messageRestFull/updateMessage";
        [_params setObject:_borradorDiccionario[@"idmessage"] forKey:@"m[idmessage]"];
    }
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"m[file]"; //MODIFICAR
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // agregar el video, tomar primero su data
    NSString* theFileName = @"";
    
    if (imageData) {
        
        NSURL* fileUrl = [NSURL fileURLWithPath:fullPath];
        //NSURLRequest* fileUrlRequest = [[NSURLRequest alloc] initWithURL:fileUrl];
        NSURLRequest* fileUrlRequest = [[NSURLRequest alloc] initWithURL:fileUrl cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:.1];
        
        NSError* error = nil;
        NSURLResponse* response = nil;
        NSData* fileData = [NSURLConnection sendSynchronousRequest:fileUrlRequest returningResponse:&response error:&error];
        
        
        NSString* mimeType = [NSString stringWithFormat:@"Content-Type: %@\r\n\r\n",[response MIMEType]];
        NSLog(@"%@", mimeType);
        //NSString* theFileName = [[fullPath lastPathComponent] stringByDeletingPathExtension];
        theFileName = [fullPath lastPathComponent];
        NSLog(@"%@", theFileName);
        
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n", FileParamConstant, theFileName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[mimeType dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setURL:requestURL]; // ENVIO EL REQUEST
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        
        if (error == nil)
        {
            if(returnData)
            {
                NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                NSLog(@"response %@",returnString);
                NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                AlmosaferAlertView *alert;
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Accept"
                             rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Aceptar"
                             rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                    
                }//                if([language rangeOfString:@"es"].location == NSNotFound)
//                {
//                    alert = [[AlmosaferAlertView alloc]
//                             initWithTitle:@"Text message"
//                             contentText:responseDic[@"msg"]
//                             leftButtonTitle:@"Accept"
//                             rightButtonTitle:nil];
//                }
//                else
//                {
//                    alert = [[AlmosaferAlertView alloc]
//                             initWithTitle:@"Mensaje de texto"
//                             contentText:responseDic[@"msg"]
//                             leftButtonTitle:@"Aceptar"
//                             rightButtonTitle:nil];
//                    
//                }
                
                
                alert.leftBlock = ^() {
                    for (UIView *aView in [_receptoresScroll subviews]){
                        _agregar.text = @"Agregar beneficiario";
                        if([language rangeOfString:@"es"].location == NSNotFound)
                            _agregar.text = @"Add beneficiary";
                        if ([aView isKindOfClass:[UIButton class]]){
                            [aView removeFromSuperview];
                        }
                        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                            [aView removeFromSuperview];
                        }
                    }
                    if(![responseDic[@"result"] isEqualToString:@"error"]){
                        _titulo.text = @"";
                        _fecha.text = @"";
                        _usrTextView.text=@"";
                        _usrTextField.text=@"";
                        _documento.text=@"";
                        _descripion.text=@"";
                        _fecha.hidden = YES;
                        [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                        if([language rangeOfString:@"es"].location == NSNotFound) {
                            _titulo.placeholder = @"Message title";
                            _selectPlaceholder.text = @"Program to";
                            [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                            [attachbtn setTitle:@"ATTACH" forState:UIControlStateNormal];
                            
                        }
                    
                        if(_esBorrador || _borradorDiccionario!=nil)
                            [self.navigationController popViewControllerAnimated:YES];
                        else
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                    }
                    else
                    {
                        NSError* error;
                        
                        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
                        
                        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
                        if(jsondata!=nil)
                        {
                            
                            
                            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                            if(_esBorrador){
                                int index = (int)[json indexOfObject:_borradorDiccionario];
                                [json removeObjectAtIndex:index];
                                
                            }
                            NSLog(@"theFileName %@",theFileName);

                            [json addObject:@{
                                              @"idtypefile" : @"3",
                                              @"title" : _titulo.text,
                                              @"fecha" : _fecha.text,
                                              @"file" : _documento.text,
                                              @"description" : _descripion.text,
                                              @"url" : urlString,
                                              @"params" : _params,
                                              @"data" : imageData,
                                              @"receptores" : receptores,
                                              @"archivo" : theFileName,
                                              }];
                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                            [data writeToFile:zipFilePath atomically:YES];
                            
                        }
                        else
                        {
                            NSLog(@"theFileName %@",theFileName);

                            NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                            [arrayfavoritos addObject:@{
                                                        @"idtypefile" : @"3",
                                                        @"title" : _titulo.text,
                                                        @"fecha" : _fecha.text,
                                                        @"file" : _documento.text,
                                                        @"description" : _descripion.text,
                                                        @"url" : urlString,
                                                        @"params" : _params,
                                                        @"data" : imageData,
                                                        @"receptores" : receptores,
                                                        @"archivo" : theFileName,
                                                        }];
                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                            [data writeToFile:zipFilePath atomically:YES];
                            
                            
                        }
                        AlmosaferAlertView *alert;
                        if([language rangeOfString:@"es"].location == NSNotFound)
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Connection error"
                                     contentText:@"It has no internet at the time,the message has been saved in drafts,you will be automatically sent once you have connection"
                                     leftButtonTitle:@"Accept"
                                     rightButtonTitle:nil];
                           
                        }
                        else
                        {
                             alert = [[AlmosaferAlertView alloc]
                                                         initWithTitle:@"Error de conexión"
                                                         contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                                                         leftButtonTitle:@"Aceptar"
                                                         rightButtonTitle:nil];
                        }
                         NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                        alert.leftBlock = ^(){
                            borradorBool = YES;
                            for (UIView *aView in [_receptoresScroll subviews]){
                                _agregar.text = @"Agregar beneficiario";
                                if([language rangeOfString:@"es"].location == NSNotFound)
                                    _agregar.text = @"Add beneficiary";
                                if ([aView isKindOfClass:[UIButton class]]){
                                    [aView removeFromSuperview];
                                }
                                if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                                    [aView removeFromSuperview];
                                }
                            }
                            _titulo.text = @"";
                            _fecha.text = @"";
                            _usrTextView.text=@"";
                            _usrTextField.text=@"";
                            _documento.text=@"";
                            _descripion.text=@"";
                            _fecha.hidden = YES;
                            [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                            if([language rangeOfString:@"es"].location == NSNotFound) {
                                _titulo.placeholder = @"Message title";
                                _selectPlaceholder.text = @"Program to";
                                [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                                [attachbtn setTitle:@"ATTACH" forState:UIControlStateNormal];
                            }

                            receptores = nil;
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                        };
                    
                    }
                };
            }
        }else{
            NSError* error;
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
            
            NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
            if(jsondata!=nil)
            {
                
                
                NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                if(_esBorrador)
                {
                    int index = (int)[json indexOfObject:_borradorDiccionario];
                    [json removeObjectAtIndex:index];
                    
                }
                [json addObject:@{
                                  @"idtypefile" : @"3",
                                  @"title" : _titulo.text,
                                  @"fecha" : _fecha.text,
                                  @"file" : _documento.text,
                                  @"description" : _descripion.text,
                                  @"url" : urlString,
                                  @"params" : _params,
                                  @"data" : imageData,
                                  @"receptores" : receptores,
                                  @"archivo" : theFileName,
                                  }];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                [data writeToFile:zipFilePath atomically:YES];
                
            }
            else
            {
                NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                [arrayfavoritos addObject:@{
                                            @"idtypefile" : @"3",
                                            @"title" : _titulo.text,
                                            @"fecha" : _fecha.text,
                                            @"file" : _documento.text,
                                            @"description" : _descripion.text,
                                            @"url" : urlString,
                                            @"params" : _params,
                                            @"data" : imageData,
                                            @"receptores" : receptores,
                                            @"archivo" : theFileName,
                                            }];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                [data writeToFile:zipFilePath atomically:YES];
            }
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                 alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Connection error"
                                             contentText:@"It has no internet at the time,the message has been saved in drafts,you will be automatically sent once you have connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
            }
            else
            {
                 alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Error de conexión"
                                             contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
            }
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };

            alert.leftBlock = ^() {
                borradorBool = YES;
                [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                //[self performSegueWithIdentifier:@"gotoHistorial" sender:nil]; //[self.navigationController popViewControllerAnimated:YES];
            };
        }
    }];
    [dataTask resume];
    
}

- (void)datePickerValueChanged:(id)sender{
    
    date = datePicker.date;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    [_fecha setText:[df stringFromDate:date]];
}

- (void)closeKeyboard:(id)sender{ [_fecha resignFirstResponder]; }

- (void)viewDidAppear:(BOOL)animated {
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        [self performSegueWithIdentifier:@"goLogin" sender:nil];
    }
    else{
        [self loadFreeSpace];
    }
}
-(void)loadFreeSpace {
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/freeSpace/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"GET"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        
        
        [self hideProgressHUD];
        if (error == nil) {
            if(returnData){
                NSArray *freeSpace = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                
                NSLog(@"%@",freeSpace);
                for(id space in freeSpace)
                {
                    _lbl2.text = [NSString stringWithFormat:@"%@ Mb", space[@"size"]];
                    
                    float progressVal = 0;
                    if([space[@"space"] intValue]!=0){
                        progressVal = ([space[@"space"] floatValue] - [space[@"free"] floatValue])/[space[@"space"] floatValue];
                    }
                    [_progressview setProgress:progressVal animated:YES];
                    [_progressview setProgress:progressVal animated:YES];
                }
            }
        }else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
        }
    }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)back:(id)sender {
    if(_esBorrador || _borradorDiccionario!=nil)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"gotoHistorial"]) {
        HistorialViewController *HistorialViewController = segue.destinationViewController;
        HistorialViewController.type_file_id = @"3";
        HistorialViewController.borrador = borradorBool;
    }
    
    if([segue.identifier isEqualToString:@"gotoAdd"]){
        BeneficiariosListViewController *controller = [segue destinationViewController];
        controller.delegado = self;
    }
}


#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.myscrollview setContentSize:CGSizeMake(self.view.bounds.size.width, self.myscrollview.frame.size.height)];
    [self.view setFrame:CGRectMake(0,190 - textField.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height)];
    _usrTextField = textField;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [textField resignFirstResponder];
    _usrTextField = nil;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    _usrTextField = nil;

}

- (void)textViewDidBeginEditing:(UITextView *)textView{
    _tooltip.text = @"";
    [self.view setFrame:CGRectMake(0, - 40,self.view.frame.size.width,self.view.frame.size.height)];
    [self.myscrollview setContentSize:CGSizeMake(self.view.bounds.size.width, self.myscrollview.frame.size.height+253)];
    _usrTextView = textView;


}
-(void)textViewDidEndEditing:(UITextView *)textView{
    if ([textView.text isEqualToString:@""]) {
        if([language rangeOfString:@"es-US"].location == NSNotFound)
        {
            _tooltip.text = @"Write a message";
        }
        else
        {
            _tooltip.text = @"Escribir mensaje";
        }

    }
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    _usrTextView = nil;
}
/*
 * Traigo mis Beneficiarios
 */
-(void)getBeneficiarios {
    //Muestro la vista de cargando
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];        
    }
    
    NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?with=beneficiary&filter=%@", escBox]; //query para pasarle a la url
    
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"beneficiaryUser"] stringByAppendingString:query];
    
    //NSString *urlString = @"http://fluie.com/fluie/api/beneficiaryUser";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];//Todos los beneficiarios de la Base de Datos
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSMutableArray *datasourcetemporal = [NSMutableArray arrayWithObjects:nil];
            if(arrayresponse.count){
                beneficiarios = arrayresponse;
                NSMutableArray *receptoresTemporal = [_borradorDiccionario[@"messageBeneficiaries"] mutableCopy];
                for(int i = 0; i<[receptoresTemporal count]; i++){
                    NSDictionary *receptDictionaryTemporal = [receptoresTemporal objectAtIndex:i];
                    
                    for (int j=0; j<[beneficiarios count]; j++) {
                        NSDictionary *beneficiario = [beneficiarios objectAtIndex:j];
                        NSLog(@"%@ %@", beneficiario[@"beneficiary_id"], receptDictionaryTemporal[@"beneficiary_id"]);
                        if([beneficiario[@"beneficiary_id"] isEqualToString:receptDictionaryTemporal[@"beneficiary_id"]]){
                            [receptores addObject:beneficiario];
                            break;
                        }
                    }
                }
                //receptores
                for (UIView *aView in [_receptoresScroll subviews]){
                    _agregar.text = @"Agregar beneficiario";
                    if([language rangeOfString:@"es"].location == NSNotFound)
                        _agregar.text = @"Add beneficiary";
                    if ([aView isKindOfClass:[UIButton class]]){
                        [aView removeFromSuperview];
                    }
                    if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                        [aView removeFromSuperview];
                    }
                }
                
                UIImageView *_miimageView = [[UIImageView alloc] init];
                
                for(int i = 0; i<[receptores count]; i++){
                    _agregar.text=@"";
                    NSMutableDictionary *receptDictionary = [[receptores objectAtIndex:i] mutableCopy];
                    NSLog(@"%@", receptDictionary);
                    
                    /*
                     * Armo la foto del beneficiario
                     */
                    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", receptDictionary[@"beneficiary_id"]];
                    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                    
                    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
                    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                    [request setHTTPMethod: @"POST"];
                    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                    [request setHTTPBody: myRequestData];
                    
                    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                    NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    NSLog(@"%@", foto[@"image"]);
                    receptDictionary[@"beneficiary"] = [receptDictionary[@"beneficiary"] mutableCopy];
                    [receptDictionary[@"beneficiary"] setObject:foto[@"image"] forKey:@"image"];
                    /*
                     * Fin foto del beneficiario
                     */
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    [button setTag:i];
                    [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
                    button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
                    button.clipsToBounds = YES;
                    
                    button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
                    button.layer.borderColor=[UIColor grayColor].CGColor;
                    button.layer.borderWidth=0.5f;
                    
                    if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
                    }else{
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
                    }
                    
                    [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
                    
                    UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
                    if(receptDictionary[@"beneficiary"][@"name"])
                        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
                    [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
                    [_receptoresScroll addSubview:button];
                    [_receptoresScroll addSubview:nombreReceptor];
                    [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
                }
            }
        }else{
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
    
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [self.view addGestureRecognizer:tap];
    _selectPlaceholder.text = @"";
    if(([titulo isEqualToString:@"Fecha"])||([titulo isEqualToString:@"Date"])){
        _fecha.hidden = NO;
    }else{
        _fecha.text = @"";
        _fecha.hidden = YES;
        
    }
}


- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    NSLog(@"%@", _btnSelect.titleLabel.text);
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (IBAction)selectClicked:(id)sender {
    [self dismissKeyboard];
    [self.view removeGestureRecognizer:tap];
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Ausencia", @"Fecha",nil];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
        arr = [NSArray arrayWithObjects:@"Absence", @"Date",nil];
    if(dropDown == nil) {
        CGFloat f = 80;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

@end
