//
//  HomeViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/8/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface HomeViewController : UIViewController<SlideNavigationControllerDelegate>
- (IBAction)onBtnMenu:(id)sender;
@end
