//
//  AlmacenamientoViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 1/24/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlmacenamientoViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@end
