//
//  BeniViewController.h
//  Fluie
//
//  Created by 01Synergy on 03/06/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BeniViewController : UIViewController<UITableViewDelegate, UITableViewDataSource,UISearchBarDelegate, UISearchDisplayDelegate,UISearchControllerDelegate>

@end
