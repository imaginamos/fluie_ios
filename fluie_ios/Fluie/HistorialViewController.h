//
//  HistorialViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 1/25/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistorialViewController : UIViewController<UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate>

@property (assign, nonatomic) BOOL borrador;
@property (strong, nonatomic) NSString *type_file_id;

@end
