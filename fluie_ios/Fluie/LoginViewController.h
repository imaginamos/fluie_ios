//
//  LoginViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 11/20/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
