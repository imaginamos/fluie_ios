//
//  VerificadorFormViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "VerificadorFormViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"

@interface VerificadorFormViewController (){
    bool masculino;
    UITextField *mytextField;
    NSArray *contentsParentezcoArray;
    NSArray *usuariosArray;
    NSDictionary *recienCreado;
    NSString *parentezcoSelected;
    bool match;
    int beneficiarioId;
    NSString * gender;
        UITapGestureRecognizer *tap;
    
}

@property (weak, nonatomic) IBOutlet UITextField *nombresTxt;
@property (weak, nonatomic) IBOutlet UITextField *apellidosTxt;
@property (weak, nonatomic) IBOutlet UITextField *correoTxt;
@property (weak, nonatomic) IBOutlet UITextField *telefonoTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnMasculino;
@property (weak, nonatomic) IBOutlet UIButton *btnFemenino;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;

@property (weak, nonatomic) IBOutlet UILabel *lbl3;
@property (weak, nonatomic) IBOutlet UILabel *lbl4;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn2;
@property (weak, nonatomic) IBOutlet UIButton *btn3;

@end

@implementation VerificadorFormViewController

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        _lbl1.text = @"Fill the checker information";
        _nombresTxt.placeholder = @"Name";
        _apellidosTxt.placeholder = @"Last Name";
        _correoTxt.placeholder = @"Email";
        _telefonoTxt.placeholder = @"Phone";
       
        _lbl2.text=@"Gender";
        _lbl3.text=@"Male";
        _lbl4.text=@"Female";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        [_btn2 setTitle:@"DELETE" forState:UIControlStateNormal];
        [_btn3 setTitle:@"SAVE" forState:UIControlStateNormal];
    }
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void)dismissKeyboard {
    [mytextField resignFirstResponder];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    masculino = TRUE;
    match = FALSE;
    // Do any additional setup after loading the view.
    
    gender = @"1";
    if(_verificador!=nil){
        _btn1.hidden = NO;
        _btn2.hidden = NO;
        _btn3.hidden = YES;
        NSLog(@"%@", _verificador);
        beneficiarioId = [_verificador[@"user_id"] intValue];
        _nombresTxt.text = _verificador[@"checker"][@"name"];
        _apellidosTxt.text = _verificador[@"checker"][@"name_last"];
        _correoTxt.text = _verificador[@"checker"][@"email"];
        _telefonoTxt.text = _verificador[@"checker"][@"phone"];
        
        if([_verificador[@"checker"][@"gender_id"] isEqualToString:@"1"]){
            masculino = TRUE;
            UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
            if(imageToSet)
            {
                [_btnMasculino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
                
            } else {
                // if you see this line in your console, then you know where to look for a problem
                NSLog( @"why is my image object nil?");
            }
            [_btnFemenino setImage:nil forState:UIControlStateNormal];
        }else{
            masculino = FALSE;
            UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
            if(imageToSet)
            {
                [_btnFemenino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
            } else {
                // if you see this line in your console, then you know where to look for a problem
                NSLog( @"why is my image object nil?");
            }
            [_btnMasculino setImage:nil forState:UIControlStateNormal];
        }
        
        
    }else{
        _btn1.hidden = YES;
        _btn2.hidden = YES;
        _btn3.hidden = NO;
        _verificador = nil;
        _nombresTxt.text = @"";
        _apellidosTxt.text = @"";
        _correoTxt.text = @"";
        _telefonoTxt.text = @"";
    }
        // Do any additional setup after loading the view.
}

-(void)viewWillDisappear:(BOOL)animated {
    _nombresTxt.text = @"";
    _apellidosTxt.text = @"";
    _correoTxt.text = @"";
    _telefonoTxt.text = @"";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)guardar:(id)sender{
    [self dismissKeyboard];
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    if(_verificador!=nil){
        if([_verificador[@"checker"][@"active"] isEqualToString:@"1"]){
            [self hideProgressHUD];
            
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"You can not make changes on an active Verifier"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No puede realizar cambios sobre un Verificador activo"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }

            
            
        }else{
            [self saveaction];
        }
    }else
        [self saveaction];
    

}

-(void) saveaction {
    //for(id user in usuariosArray) {
    //if ([_correoTxt.text isEqualToString:user[@"email"]]) {
    match = TRUE;
    //Creo el beneficiario y lo asigno a mi usuario
    
    NSString *urlString = @"http://fluie.com/fluie/user/SaveChecker";
    if(_verificador!=nil)
        urlString = @"http://fluie.com/fluie/user/EditChecker";
    
    NSLog(@"%@", urlString);
    
    NSString *myRequestString;
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
         myRequestString = [NSString stringWithFormat:@"user_id=%@&v[name]=%@&v[name_last]=%@&v[email]=%@&v[gender_id]=%@&v[phone]=%@&v[language]=2", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _nombresTxt.text, _apellidosTxt.text, _correoTxt.text, gender, _telefonoTxt.text];
    }
    else
    {
         myRequestString = [NSString stringWithFormat:@"user_id=%@&v[name]=%@&v[name_last]=%@&v[email]=%@&v[gender_id]=%@&v[phone]=%@&v[language]=1", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _nombresTxt.text, _apellidosTxt.text, _correoTxt.text, gender, _telefonoTxt.text];
    }
    
    
    
    NSLog(@"requeststring: %@", myRequestString);
    
    
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    
    
    if(_verificador!=nil){
        
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            myRequestString = [NSString stringWithFormat:@"v[iduser]=%@&v[idcheckeruser]=%@&v[name]=%@&v[email]=%@&v[gender_id]=%@&v[phone]=%@&v[language]=2", _verificador[@"checker_id"], _verificador[@"idcheckeruser"], [NSString stringWithFormat:@"%@ %@", _nombresTxt.text, _apellidosTxt.text], _correoTxt.text, gender, _telefonoTxt.text];
        }
        else
        {
            myRequestString = [NSString stringWithFormat:@"v[iduser]=%@&v[idcheckeruser]=%@&v[name]=%@&v[email]=%@&v[gender_id]=%@&v[phone]=%@&v[language]=1", _verificador[@"checker_id"], _verificador[@"idcheckeruser"], [NSString stringWithFormat:@"%@ %@", _nombresTxt.text, _apellidosTxt.text], _correoTxt.text, gender, _telefonoTxt.text];
        }
        
        
        
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:[NSString stringWithFormat:@"%@", _verificador[@"checker_id"]] forKey:@"v[iduser]"];
        [_params setObject:_verificador[@"idcheckeruser"] forKey:@"v[idcheckeruser]"];
        [_params setObject:[NSString stringWithFormat:@"%@", _nombresTxt.text] forKey:@"v[name]"];
        [_params setObject:[NSString stringWithFormat:@"%@",_apellidosTxt.text] forKey:@"v[name_last]"];
        [_params setObject:[NSString stringWithFormat:@"%@", _correoTxt.text] forKey:@"v[email]"];
        [_params setObject:[NSString stringWithFormat:@"%@", gender] forKey:@"v[gender_id]"];
        [_params setObject:[NSString stringWithFormat:@"%@", _telefonoTxt.text] forKey:@"v[phone]"];
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        NSURL* requestURL = [NSURL URLWithString:urlString];
        request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        //[request setTimeoutInterval:30];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setHTTPMethod:@"POST"];
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            NSLog(@"%@", param);
            NSLog(@"%@", [_params objectForKey:param]);
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        [request setURL:requestURL];
        
        
    }
    
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            if(returnData){
                
                NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                if(!responseDic[@"error"])
                {
                    if([responseDic[@"result"] isEqualToString:@"error"])
                    {
                        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                        if([language rangeOfString:@"es"].location == NSNotFound)
                        {
                            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                         initWithTitle:@"Checker"
                                                         contentText:responseDic[@"msg"]
                                                         leftButtonTitle:@"Accept"
                                                         rightButtonTitle:nil];
                            [alert showInView:self.view];
                        }
                        else
                        {
                            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                         initWithTitle:@"Verificador"
                                                         contentText:responseDic[@"msg"]
                                                         leftButtonTitle:@"Aceptar"
                                                         rightButtonTitle:nil];
                            [alert showInView:self.view];
                        }
                        
                        
                    }
                    else
                    {
                        if (![responseDic[@"result"] isEqualToString:@"error"]) {
                            _correoTxt.text = @"";
                            _apellidosTxt.text = @"";
                            _telefonoTxt.text = @"";
                        }
                        _nombresTxt.text = @"";
                       
                        [self hideProgressHUD];
                        AlmosaferAlertView *alert;
                        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                        if([language rangeOfString:@"es"].location == NSNotFound)
                        {
                             alert = [[AlmosaferAlertView alloc]
                                                         initWithTitle:@"Checker"
                                                         contentText:responseDic[@"msg"]
                                                         leftButtonTitle:@"Accept"
                                                         rightButtonTitle:nil];
                        }
                        else
                        {
                             alert = [[AlmosaferAlertView alloc]
                                                         initWithTitle:@"Verificador"
                                                         contentText:responseDic[@"msg"]
                                                         leftButtonTitle:@"Aceptar"
                                                         rightButtonTitle:nil];
                        }
                        [alert showInView:self.view];
                        alert.leftBlock = ^() {
                            if(![responseDic[@"result"] isEqualToString:@"error"])
                                [self dismissViewControllerAnimated:YES completion:nil];
                            
                        };
                    }
                    
                    
                }
                else if(responseDic[@"error"])
                {
                    id val = nil;
                    NSArray *values = [responseDic[@"error"] allValues];
                    if ([values count] != 0)
                        val = [values objectAtIndex:0];
                    
                    NSLog(@"%@", val);
                    NSString *respuesta=@"";
                    if([val isKindOfClass:[NSArray class]]){
                        respuesta = [val objectAtIndex:0];
                    }else if([val isKindOfClass:[NSString class]]){
                        respuesta = val;
                    }else{
                        respuesta = val;
                    }
                    NSLog(@"%@", respuesta);
                    
                    AlmosaferAlertView *alert;
                    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                    if([language rangeOfString:@"es"].location == NSNotFound)
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Checker"
                                 contentText:[NSString stringWithFormat:@"%@", respuesta]
                                 leftButtonTitle:@"Accept"
                                 rightButtonTitle:nil];
                    }
                    else
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Verificador"
                                 contentText:[NSString stringWithFormat:@"%@", respuesta]
                                 leftButtonTitle:@"Aceptar"
                                 rightButtonTitle:nil];
                    }
                    [alert showInView:self.view];

                    alert.leftBlock = ^() {
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [self hideProgressHUD];
                    };
                    
                }
            }
        }else
        {
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
}

-(IBAction)borrar:(id)sender{
        [self dismissKeyboard];
    
    AlmosaferAlertView *alert;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirm"
                 contentText:@"You want to delete the Verifier ?"
                 leftButtonTitle:@"Yes"
                 rightButtonTitle:@"No"];
    }
    else
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirmar"
                 contentText:@"Desea borrar al Verificador?"
                 leftButtonTitle:@"Si"
                 rightButtonTitle:@"No"];
    }
    [alert showInView:self.view];

    alert.leftBlock = ^() {
        
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    if(beneficiarioId){
        
        NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/user/DeleteChecker"];
        //beneficiarioId
        NSString *myRequestString;
        
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            myRequestString= [NSString stringWithFormat:@"user_id=%@&checker_id=%@&language=2", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _verificador[@"checker_id"]];
        }
        else
        {
            myRequestString= [NSString stringWithFormat:@"user_id=%@&checker_id=%@&language=1", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _verificador[@"checker_id"]];
        }
        
        NSLog(@"requeststring: %@", myRequestString);
        NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"POST"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody: myRequestData];

        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        if(returnData){
            //NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
            NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            
            AlmosaferAlertView *alert;
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Checker"
                         contentText:arrayresponse[@"msg"]
                         leftButtonTitle:@"Accept"
                         rightButtonTitle:nil];
            }
            else
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Verificador"
                         contentText:arrayresponse[@"msg"]
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
            }
            [alert showInView:self.view];

            alert.leftBlock = ^() {
                NSLog(@"left button clicked");
                [self dismissViewControllerAnimated:YES completion:nil];
                [self hideProgressHUD];
            };
        }
        
    }else{
        _nombresTxt.text = @"";
        _apellidosTxt.text = @"";
        _correoTxt.text = @"";
        
    }
    };
}

-(IBAction)onMasculino:(id)sender{
    NSLog(@"masculino");
    gender = @"1";
    masculino = TRUE;
    
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnMasculino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnFemenino setImage:nil forState:UIControlStateNormal];
    
}

-(IBAction)onFemenino:(id)sender{
    masculino = FALSE;
    gender = @"2";
    NSLog(@"femenino");
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnFemenino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnMasculino setImage:nil forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [textField resignFirstResponder];
    // }
    mytextField = textField;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509+253)];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

@end
