//
//  BienvenidoViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/20/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "BienvenidoViewController.h"



@interface BienvenidoViewController ()
{
    __weak IBOutlet UIWebView *webView;
    NSDictionary *homedic;
}
@property (weak, nonatomic) IBOutlet UILabel *textoBienvenida;
@property (weak, nonatomic) IBOutlet UILabel *titulo;

@end

@implementation BienvenidoViewController

- (void)webViewDidFinishLoad:(UIWebView *)webViewobj
{
    webView.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [webView setBackgroundColor:[UIColor whiteColor]];
    [webView setOpaque:NO];
    webView.hidden = YES;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/home";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    if(returnData){
    
        NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        for(id home in arrayresponse) {
            
            if ([language rangeOfString:@"es"].location == NSNotFound && [home[@"language_id"] isEqualToString:@"2"]) {
                NSString *youtubeURL = home[@"video"];
                homedic = home;
                _textoBienvenida.text = home[@"description"];
                _titulo.text = home[@"title"];
                NSError *error = NULL;
                NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
                NSRegularExpression *regex =
                [NSRegularExpression regularExpressionWithPattern:regexString
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
                NSTextCheckingResult *match = [regex firstMatchInString:youtubeURL
                                                                options:0
                                                                  range:NSMakeRange(0, [youtubeURL length])];
                if (match) {
                    NSRange videoIDRange = [match rangeAtIndex:0];
                    NSString *substringForFirstMatch = [youtubeURL substringWithRange:videoIDRange];
                    NSLog(@"video id: %@", substringForFirstMatch);
                    
                    [webView loadHTMLString:[NSString stringWithFormat:@"<style>body{margin:0px;}</style><span style=\"background-color: #000000;\"><iframe width=\"%f\" title=\"YouTube video player\" src=\"http://www.youtube.com/embed/%@?HD=1&controls=0;rel=0;showinfo=0;controls=1\" height=\"%f\" frameborder=\"0\"></iframe>",webView.frame.size.width,substringForFirstMatch,webView.frame.size.height] baseURL:[NSURL URLWithString:nil]];
                }
            }
            
            if ([language rangeOfString:@"es"].location != NSNotFound  && [home[@"language_id"] isEqualToString:@"1"]) {
                NSString *youtubeURL = home[@"video"];
                homedic = home;
                _textoBienvenida.text = home[@"description"];
                _titulo.text = home[@"title"];
                NSError *error = NULL;
                NSString *regexString = @"(?<=v(=|/))([-a-zA-Z0-9_]+)|(?<=youtu.be/)([-a-zA-Z0-9_]+)";
                NSRegularExpression *regex =
                [NSRegularExpression regularExpressionWithPattern:regexString
                                                          options:NSRegularExpressionCaseInsensitive
                                                            error:&error];
                NSTextCheckingResult *match = [regex firstMatchInString:youtubeURL
                                                                options:0
                                                                  range:NSMakeRange(0, [youtubeURL length])];
                if (match) {
                    NSRange videoIDRange = [match rangeAtIndex:0];
                    NSString *substringForFirstMatch = [youtubeURL substringWithRange:videoIDRange];
                    NSLog(@"video id: %@", substringForFirstMatch);
                    
                    [webView loadHTMLString:[NSString stringWithFormat:@"<style>body{margin:0px;}</style><span style=\"background-color: #000000;\"><iframe width=\"%f\" title=\"YouTube video player\" src=\"http://www.youtube.com/embed/%@?HD=1;rel=0;showinfo=0;controls=1\" height=\"%f\" frameborder=\"0\"></iframe>",webView.frame.size.width,substringForFirstMatch,webView.frame.size.height] baseURL:[NSURL URLWithString:nil]];
                }
            }
            
        }
    
    }
    
    
    
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
