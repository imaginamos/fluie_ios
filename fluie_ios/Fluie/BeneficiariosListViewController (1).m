//
//  BeneficiariosListViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "BeneficiariosListViewController.h"
#import "BeneficiarioFormViewController.h"
#import "MBProgressHUD.h"

@interface BeneficiariosListViewController ()
{
    NSArray *dataSource;
    NSArray *parentezcoArray;
    NSArray *userArray;
    NSMutableDictionary *beneficiario;

}
@property (weak, nonatomic) IBOutlet UITableView *mytableview;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIButton *BTN1;


@end

@implementation BeneficiariosListViewController

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        [_BTN1 setTitle:@"ADD BENEFICIARY" forState:UIControlStateNormal];
    }
    [self getRelaciones];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _mytableview.backgroundColor = [UIColor clearColor];
    _mytableview.separatorColor = [UIColor clearColor];
    _mytableview.delegate = self;
    _mytableview.dataSource = self;
    [_mytableview setSeparatorColor:[UIColor grayColor]];
    //[self getRelaciones];
    
    // Do any additional setup after loading the view.
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void)getRelaciones {
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Cargando Beneficiarios";
       
    }

    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/relationship";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        parentezcoArray=arrayresponse;
    }
    
    [self getUsuarios];
}

-(void)getUsuarios {
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        userArray=arrayresponse;
    }
    
    [self getdataSource];

}

-(void)getdataSource {
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/beneficiaryUser";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"beneficiarios %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    NSMutableArray *datasourcetemporal = [NSMutableArray arrayWithObjects:nil];
    if(arrayresponse.count){
        for(NSDictionary* dato in arrayresponse) {
            
            NSMutableDictionary *datoTemp = [dato mutableCopy];
            if ([dato[@"user_id"] isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]]) {
                
                for(NSDictionary* relationship in parentezcoArray) {
                    if ([relationship[@"idrelationship"] isEqualToString:dato[@"relationship_id"]]) {
                        [datoTemp setObject:relationship[@"relationship"] forKey:@"relationship"];
                    }
                }
                
                for(NSDictionary* user in userArray) {
                        NSLog(@"%@", userArray);
                    if ([user[@"iduser"] isEqualToString:dato[@"beneficiary_id"]]) {
                        [datoTemp setObject:user forKey:@"user"];
                    }
                }
                
                [datasourcetemporal addObject:datoTemp];
            }
        }
        dataSource=datasourcetemporal;
        [_mytableview reloadData];
        [self hideProgressHUD];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    NSMutableDictionary *diccionario = [dataSource[indexPath.row] mutableCopy];
    NSLog(@"diccionario: %@", diccionario);
    
    NSLog(@"nombre: %@", diccionario[@"user"]);
    
    
    UILabel *nombre = (UILabel *)[cell viewWithTag:101];
    nombre.text = [NSString stringWithFormat:@"%@ %@", diccionario[@"user"][@"name"], diccionario[@"user"][@"name_last"]];
    
    UILabel *relacion = (UILabel *)[cell viewWithTag:102];
    relacion.text = diccionario[@"relationship"];
    
    UIButton *botonEditar = (UIButton *)[cell viewWithTag:100];
    [botonEditar addTarget:self
                     action:@selector(editar:)
           forControlEvents:UIControlEventTouchUpInside];
    
    botonEditar.tag = indexPath.row;
    
    UIImageView *_miimageView = (UIImageView *)[cell viewWithTag:103];
    _miimageView.layer.masksToBounds = YES;
    // border radius
    [_miimageView.layer setCornerRadius:23.0f];
    // border
    [_miimageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_miimageView.layer setBorderWidth:0.5f];
    // drop shadow
    [_miimageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_miimageView.layer setShadowOpacity:0.8];
    [_miimageView.layer setShadowRadius:3.0];
    [_miimageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    return cell;
}

-(IBAction)editar:(id)sender{
    UIButton *tempotal = (UIButton *) sender;
    NSLog(@"%ld", (long)tempotal.tag);
    beneficiario = [[dataSource objectAtIndex:tempotal.tag] mutableCopy];
    [self performSegueWithIdentifier:@"editarbeneficiario" sender:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"editarbeneficiario"]) {
        BeneficiarioFormViewController *BeneficiarioDetailsViewController = segue.destinationViewController;
        BeneficiarioDetailsViewController.beneficiario = beneficiario;
    }
}


@end
