//
//  MsnVozViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/12/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MsnVozViewController : UIViewController <AVAudioRecorderDelegate, AVAudioPlayerDelegate>
- (IBAction)recordPauseTapped:(id)sender;
@end
