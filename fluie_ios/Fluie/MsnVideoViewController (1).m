//
//  MsnVideoViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/11/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "MsnVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIImage+Resize.h"

@interface MsnVideoViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *videoThumbView;
@property (strong, nonatomic) NSURL *videoURL;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UITextField *fecha;
@property (strong, nonatomic) MPMoviePlayerController *videoController;
@property (weak, nonatomic) IBOutlet UITextField *titulo;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@end

@implementation MsnVideoViewController

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _lbl1.text = @"Used space";
        _titulo.placeholder = @"Message title";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)captureVideo:(id)sender {
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        
        [self presentViewController:picker animated:YES completion:NULL];
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self.videoView setHidden:NO];
    self.videoURL = info[UIImagePickerControllerMediaURL];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    self.videoController = [[MPMoviePlayerController alloc] init];
    
    [self.videoController setContentURL:self.videoURL];
    
    UIImage *thumbnail = [self.videoController thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    [self.videoController stop];
    UIImage *scaledImage = [thumbnail resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(319, 164) interpolationQuality:kCGInterpolationHigh];
    // Crop the image to a square (yikes, fancy!)
    UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width - 319)/2, (scaledImage.size.height -164)/2, 319, 164)];

    
    [self.videoThumbView setImage:croppedImage];
    
}

-(IBAction)playvideo:(id)sender {
    
    if(self.videoURL) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:self.videoURL];
        self.videoController.controlStyle = MPMovieControlStyleFullscreen;
        //self.videoController.view.transform = CGAffineTransformConcat(self.videoController.view.transform, CGAffineTransformMakeRotation(M_PI));
        [self.videoController.view setFrame: self.view.bounds];
        [self.view addSubview: self.videoController.view];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doneButtonClick:)
                                                     name:MPMoviePlayerWillExitFullscreenNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(videoPlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.videoController];
        
        [self.videoController play];
    }
    
}

-(IBAction)borrarvideo:(id)sender {
    [_videoThumbView setImage:nil];
    [_videoView setHidden:YES];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    // Stop the video player and remove it from view
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    
}


- (void)doneButtonClick:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey object:nil];
    
    // Stop the video player and remove it from view
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
