//
//  HistorialViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 1/25/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import "HistorialViewController.h"
#import "MBProgressHUD.h"
#import "AlmosaferAlertView.h"
#import "MsnFotoViewController.h"
#import "MsnTextoViewController.h"
#import "MsnVideoViewController.h"
#import "MsnVozViewController.h"

static NSString* const kBaseURL = @"http://fluie.com/fluie/api/";
static NSString* const kInvitaciones = @"message";

@interface HistorialViewController ()
{
    NSArray *dataSource;
    IBOutlet UISearchBar *candySearchBar;
    NSMutableArray *filteredCandyArray;
    NSString *language_id;
    
    IBOutlet UIButton *buymorespace;
}

@property (weak, nonatomic) IBOutlet UITableView *mytableview;
@property (weak, nonatomic) NSDictionary *borradorDiccionario;
@property (strong, nonatomic) MBProgressHUD *progressHUD;

@end

@implementation HistorialViewController

- (void)viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    language_id = @"1";
    if([language rangeOfString:@"es"].location == NSNotFound) {
        language_id = @"2";
        candySearchBar.placeholder = @"Search";
        [buymorespace setTitle:@"BUY MORE SPACE" forState:UIControlStateNormal];
    }
    [self getdataSource];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _mytableview.backgroundColor = [UIColor clearColor];
    _mytableview.separatorColor = [UIColor clearColor];
    _mytableview.delegate = self;
    _mytableview.dataSource = self;
    [_mytableview setSeparatorColor:[UIColor grayColor]];

}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void)getdataSource {
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    if(_borrador==NO){
        NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\", \"type_file_id\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _type_file_id];
        NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                                 (CFStringRef) boxQuery,
                                                                                                 NULL,
                                                                                                 (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                                 kCFStringEncodingUTF8));
        NSString* query = [NSString stringWithFormat:@"?filter=%@", escBox]; //query para pasarle a la url
        
        
        
        //NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/message?filter={'user_id' : '%@'}", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
        
        NSString* urlStr = [[kBaseURL stringByAppendingPathComponent:kInvitaciones] stringByAppendingString:query];
        
        //NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        NSURL* url = [NSURL URLWithString:urlStr];
        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        [request setHTTPMethod: @"GET"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
            [self hideProgressHUD];
            if (error == nil) {

                NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                NSLog(@"%@", arrayresponse);
                if(arrayresponse.count)
                {
                    if (arrayresponse.count>0)
                    {
                        dataSource=arrayresponse;
                        [_mytableview reloadData];

                    }
                    else
                    {
                        [self.navigationController popViewControllerAnimated:YES];
                    }

                    
                }
                else
                {
                   [self.navigationController popViewControllerAnimated:YES];
                }
                
            }else{
                
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notification"
                                                 contentText:@"Could not establish communication. Check your connection"
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notificacion"
                                                 contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                
                NSLog(@"%@", error);
            }
        }];
        [dataTask resume];
        [_mytableview reloadData];
    }else{
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
        NSMutableArray *datasourceTemp = [[NSMutableArray alloc] init];
        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
        if(jsondata!=nil){
            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
            
            for (int i = 0; i<json.count; i++) {
                NSMutableDictionary *diccionario = [[json objectAtIndex:i] mutableCopy];
                if(diccionario !=nil){
                    NSData *mydata= diccionario[@"data"];
                    [diccionario setObject:[NSString stringWithFormat:@"%.2f MB",(float)mydata.length/1024.0f/1024.0f] forKey:@"size"];
                    NSLog(@"%@", diccionario[@"size"]);
                    if([diccionario[@"idtypefile"] isEqualToString:_type_file_id]){
                        [datasourceTemp addObject:diccionario];
                    }
                }
            }
            [self hideProgressHUD];
            datasourceTemp=[[[datasourceTemp reverseObjectEnumerator] allObjects] mutableCopy];
            
            dataSource = datasourceTemp;
            [_mytableview reloadData];
        }
    }
    [_mytableview reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.

    if ([segue.identifier isEqualToString:@"pushtovideo"]) {
        MsnVideoViewController *destino = segue.destinationViewController;
        destino.borradorDiccionario = _borradorDiccionario;
        destino.esBorrador =FALSE;// &(_borrador);
    }
    if ([segue.identifier isEqualToString:@"pushtoaudio"]) {
        MsnVozViewController *destino = segue.destinationViewController;
        destino.borradorDiccionario = _borradorDiccionario;
        destino.esBorrador = FALSE;//&(_borrador);
    }
    if ([segue.identifier isEqualToString:@"pushtofoto"]) {
        MsnFotoViewController *destino = segue.destinationViewController;
        destino.borradorDiccionario = _borradorDiccionario;
        destino.esBorrador = FALSE;//&(_borrador);
    }
    if ([segue.identifier isEqualToString:@"pushtotexto"]) {
        MsnTextoViewController *destino = segue.destinationViewController;
        destino.borradorDiccionario = _borradorDiccionario;
        destino.esBorrador =FALSE;// &(_borrador);
    }
}

 
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //return dataSource.count;
    NSLog(@"%lu", (unsigned long)filteredCandyArray.count);
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        return filteredCandyArray.count + 1;
    } else {
        return dataSource.count + 1;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 62;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [_mytableview dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        cell = [_mytableview dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    
    UILabel *titulo = (UILabel *)[cell viewWithTag:101];
    UILabel *fecha = (UILabel *)[cell viewWithTag:102];
    UILabel *texto1 = (UILabel *)[cell viewWithTag:500];
    UILabel *texto2 = (UILabel *)[cell viewWithTag:501];
    UILabel *editar = (UILabel *)[cell viewWithTag:1010];
    UIButton *botonBorrar = (UIButton *)[cell viewWithTag:104];
    
    if([language_id isEqualToString:@"2"])
    {
        editar.text = @"Edit";
        [botonBorrar setTitle:@"Delete" forState:UIControlStateNormal];
        texto1.text =@"Consumer guide messages and advice 50MB life";
        texto2.text = @"To edit these messages entering the WEB";
    }
    titulo.hidden = NO;
    fecha.hidden = NO;
    editar.hidden = NO;
    botonBorrar.hidden = NO;
    texto1.hidden = YES;
    texto2.hidden = YES;
    
    if ( cell == nil )
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
       // [cell.contentView addSubview:botonBorrar];
    }
    else
    {
        //[cell.contentView addSubview:botonBorrar];
    }
    
    if (indexPath.row == dataSource.count && indexPath.row!=0)
    {

        texto1.hidden = NO;
        texto2.hidden = NO;
        
        titulo.hidden = YES;
        fecha.hidden = YES;
        editar.hidden = YES;
        botonBorrar.hidden = YES;
        
        /*if ([cell.contentView subviews]){
            for (UIView *subview in [cell.contentView subviews]) {
                if ([subview isKindOfClass:[UIButton class]])
                    [subview removeFromSuperview];
            }
        }*/
        
        return cell;
    }
    
    NSMutableDictionary *diccionario = [dataSource[indexPath.row] mutableCopy];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        diccionario = [filteredCandyArray[indexPath.row] mutableCopy];
        //cell.textLabel.text = filteredCandyArray[indexPath.row][@"beneficiary"][@"name"];
        if (indexPath.row == filteredCandyArray.count && indexPath.row!=0) {
            texto1.hidden = NO;
            texto2.hidden = NO;
                return cell;
        }
    }
    
    //NSLog(@"%@",diccionario[@"title"]);
    
    
    titulo.text = diccionario[@"title"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:diccionario[@"date_created"]];
    //NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:162000];
    
    NSString *formattedDateString = [dateFormatter stringFromDate:date];
    //NSLog(@"formattedDateString: %@", formattedDateString);
    
    
    fecha.text = formattedDateString;
    
    UILabel *peso = (UILabel *)[cell viewWithTag:103];
    peso.text = [NSString stringWithFormat:@"%@", diccionario[@"size"]];
    
    if(_borrador==YES)
        fecha.text = [NSString stringWithFormat:@"%@", diccionario[@"size"]];
    
    //NSLog(@"%@", diccionario);
    [botonBorrar addTarget:self
                    action:@selector(borrarMensaje:)
          forControlEvents:UIControlEventTouchUpInside];
    
    botonBorrar.tag = indexPath.row;
    
    /*
    
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTag:indexPath.row];
    [button addTarget:self action:@selector(borrarMensaje:) forControlEvents:UIControlEventTouchUpInside];
    button.frame = botonBorrar.frame;
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitle:@"Borrar" forState:UIControlStateNormal];
    [cell.contentView addSubview:button];
    */
    
    
    return cell;
}

-(IBAction)borrarMensaje:(id)sender {
    
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.mytableview];
    NSIndexPath *indexPath = [self.mytableview indexPathForRowAtPoint:buttonPosition];
    
    //NSLog(@"%i", indexPath.row);
    
    NSString *Confirmar = @"Confirmar";
    NSString *contenido = @"Desea borrar el mensaje?";
    NSString *si = @"Si";
    NSString *no = @"No";
    
    if([language_id isEqualToString:@"2"])
    {
        Confirmar = @"Confirm";
        contenido = @"Do you want to delete message?";
        si = @"Yes";
        no = @"No";
    }
    
    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:Confirmar
                                 contentText:contenido
                                 leftButtonTitle:si
                                 rightButtonTitle:no];
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
    
    alert.leftBlock = ^() {
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
        
        
        
        UIButton *tempotal = (UIButton *) sender;
 
        NSMutableDictionary *diccionario = [dataSource[indexPath.row] mutableCopy];
        NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/message/%@", diccionario[@"idmessage"]];
        //beneficiarioId
        //NSLog(@"%@", urlString);
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"DELETE"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        
        //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
            
            [self hideProgressHUD];
            
            if (error == nil) {
                NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
                NSLog(@"%@", responsestring);
                
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Beneficiary"
                                                 contentText:@"Message deleted"
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Beneficiario"
                                                 contentText:@"Mensaje Borrado"
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                [self getdataSource];
                
                
            }else{
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notification"
                                                 contentText:@"Could not establish communication. Check your connection"
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notificacion"
                                                 contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                //NSLog(@"%@", error);
            }
        }];
        [dataTask resume];
    };
    
    

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    _borradorDiccionario = dataSource[indexPath.row];
    
    
    if(_borrador == YES)
    {
        if([_borradorDiccionario[@"idtypefile"] isEqualToString:@"1"])
            [self performSegueWithIdentifier:@"pushtovideo" sender:nil];
        
        if([_borradorDiccionario[@"idtypefile"] isEqualToString:@"2"])
            [self performSegueWithIdentifier:@"pushtoaudio" sender:nil];
        
        if([_borradorDiccionario[@"idtypefile"] isEqualToString:@"4"])
            [self performSegueWithIdentifier:@"pushtofoto" sender:nil];
        
        if([_borradorDiccionario[@"idtypefile"] isEqualToString:@"3"])
            [self performSegueWithIdentifier:@"pushtotexto" sender:nil];
    }else{
        
        
        [self editarMensaje:_borradorDiccionario];
    }
}

-(IBAction)editarMensaje:(NSDictionary *)diccionario{
    /*UIButton *tempotal = (UIButton *) sender;
    NSLog(@"%ld", (long)tempotal.tag);
    NSMutableDictionary *diccionario = [dataSource[tempotal.tag] mutableCopy];*/
    
    //http://fluie.com/fluie/api/message?with=messageFiles,messageBeneficiaries&filter={"user_id" : "98","idmessage":"39"}
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];        
    }
    NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\", \"idmessage\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], diccionario[@"idmessage"]];
    
    
   
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?with=messageFiles,messageBeneficiaries&filter=%@", escBox]; //query para pasarle a la url
    
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"message"] stringByAppendingString:query];
    
    //NSLog(@"%@", urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            
            //NSLog(@"%@", arrayresponse);
            _borradorDiccionario = [arrayresponse objectAtIndex:0];
            if([_borradorDiccionario[@"type_file_id"] isEqualToString:@"1"])
                [self performSegueWithIdentifier:@"pushtovideo" sender:nil];
            
            if([_borradorDiccionario[@"type_file_id"] isEqualToString:@"2"])
                [self performSegueWithIdentifier:@"pushtoaudio" sender:nil];
            
            if([_borradorDiccionario[@"type_file_id"] isEqualToString:@"4"])
                [self performSegueWithIdentifier:@"pushtofoto" sender:nil];
            
            if([_borradorDiccionario[@"type_file_id"] isEqualToString:@"3"])
                [self performSegueWithIdentifier:@"pushtotexto" sender:nil];

        }else{
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
    }];
    [dataTask resume];
}

-(IBAction)eliminar:(id)sender{
    UIButton *tempotal = (UIButton *) sender;
    //NSLog(@"%ld", (long)tempotal.tag);
}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    //searchResults = [recipes filteredArrayUsingPredicate:resultPredicate];
    
    [filteredCandyArray removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@",searchText];
    NSArray *myarray = dataSource;
    
    filteredCandyArray = [NSMutableArray arrayWithArray:[dataSource filteredArrayUsingPredicate:predicate]];
    //NSLog(@"filteredCandyArray %@", filteredCandyArray);
    //dataSource = [NSMutableArray arrayWithArray:[myarray filteredArrayUsingPredicate:predicate]];
    [_mytableview reloadData];
}

@end
