//
//  MsnFotoViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/12/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "MsnFotoViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"
#import "HistorialViewController.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"

@interface MsnFotoViewController () {
    BOOL borradorBool;
    UIDatePicker *datePicker;
    NSDate *date;
    UIToolbar *keyboardToolbar;
    NSMutableArray * receptores;
    NSString *receptoresString;
    __weak IBOutlet UIButton *addButon;
    UITextField *mytextField;
    NSMutableArray *thumbsArray;
    int selectedPreview;
    NSArray *beneficiarios;
    NIDropDown *dropDown;
    NSString * language;
    UITapGestureRecognizer *tap;
}
@property (weak, nonatomic) IBOutlet UIImageView *fotoPreview;
@property (weak, nonatomic) IBOutlet UIProgressView *progressview;
@property (weak, nonatomic) IBOutlet UIButton *delBtn;
@property (weak, nonatomic) IBOutlet UIView *preview;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UITextField *titulo;
@property (weak, nonatomic) IBOutlet UITextField *fecha;
@property (weak, nonatomic) IBOutlet UIScrollView *thumbScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *receptoresScroll;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UILabel *selectPlaceholder;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UILabel *agregar;


@end

@implementation MsnFotoViewController


-(void)dismissKeyboard {
    if(mytextField!=nil)
        [mytextField resignFirstResponder];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _lbl1.text = @"Used space";
        _titulo.placeholder = @"Message title";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        _selectPlaceholder.text = @"Program to";
        _agregar.text = @"Add beneficiary";
    }
    
    thumbsArray = [NSMutableArray array];
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    receptores = [[NSMutableArray alloc] init];
    for (UIView *aView in [_receptoresScroll subviews]){
        _agregar.text = @"Agregar beneficiario";
        if([language rangeOfString:@"es"].location == NSNotFound)
            _agregar.text = @"Add beneficiary";

        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
            [aView removeFromSuperview];
        }
    }
    
    if (keyboardToolbar == nil) {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar setBarTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Cerrar" style:UIBarButtonItemStyleDone target:self action:@selector(closeKeyboard:)];
        
        [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    
    _fecha.inputAccessoryView = keyboardToolbar;
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    _fecha.inputView = datePicker;
    
    if(_esBorrador){
        _selectPlaceholder.text = @"";
        thumbsArray = _borradorDiccionario[@"thumbs"];
        _titulo.text = _borradorDiccionario[@"title"];
        _fecha.text = _borradorDiccionario[@"fecha"];
        receptores = [_borradorDiccionario[@"receptores"] mutableCopy];
        _selectPlaceholder.text = @"";
        if (_borradorDiccionario[@"fecha"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"fecha"];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                 [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
            
            
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }
        
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";

            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
            if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                [aView removeFromSuperview];
            }
        }
        
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text = @"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            //[_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]]placeholderImage:[UIImage imageNamed:@"avatar"]];
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
        
        for (UIView *aView in [_thumbScroll subviews]){
            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
        }
        for(int i = 0; i<[thumbsArray count]; i++){
            NSDictionary *imageDictionary = [thumbsArray objectAtIndex:i];
            UIImage *selectedImage = [UIImage imageWithData:imageDictionary[@"data"]];
            UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
            // Crop the image to a square (yikes, fancy!)
            UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
            CGImageRef imageRef = [selectedImage CGImage];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(onPreview:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(65), 0, 63.0, 63.0);//width and height
            [button setImage:croppedImage forState:UIControlStateNormal];
            [_thumbScroll addSubview:button];
            
            if(i>4){
                [_thumbScroll setContentSize:CGSizeMake(((i*65) + 65), _thumbScroll.frame.size.height)];
            }
        }
        _preview.hidden = YES;
    }
    else if(_borradorDiccionario!=nil)
    {
        _selectPlaceholder.text = @"";
        for(int i = 0; i<[_borradorDiccionario[@"messageFiles"] count]; i++){
            NSDictionary *tempthumbsDictionary = [_borradorDiccionario[@"messageFiles"] objectAtIndex:i];
            NSURL *imageURL = [NSURL URLWithString:tempthumbsDictionary[@"url"]];
            NSData *dataFile = [NSData dataWithContentsOfURL:imageURL];
            [thumbsArray addObject:@{
                                     @"data" : dataFile,
                                     }];
        }
        _titulo.text = _borradorDiccionario[@"title"];
        NSLog(@"%@", _borradorDiccionario[@"date_view"]);
        if (_borradorDiccionario[@"date_view"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"date_view"];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }
        
        
        [self getBeneficiarios];
        
        for (UIView *aView in [_thumbScroll subviews]){
            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
        }
        for(int i = 0; i<[thumbsArray count]; i++){
            NSDictionary *imageDictionary = [thumbsArray objectAtIndex:i];
            UIImage *selectedImage = [UIImage imageWithData:imageDictionary[@"data"]];
            UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
            // Crop the image to a square (yikes, fancy!)
            UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
            CGImageRef imageRef = [selectedImage CGImage];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(onPreview:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(65), 0, 63.0, 63.0);//width and height
            [button setImage:croppedImage forState:UIControlStateNormal];
            [_thumbScroll addSubview:button];
            
            if(i>4){
                [_thumbScroll setContentSize:CGSizeMake(((i*65) + 65), _thumbScroll.frame.size.height)];
            }
        }
        _preview.hidden = YES;
    }
}


- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}


-(void) PlayerAdded:(BeneficiariosListViewController *)controller andPlayer:(NSDictionary *)beneficiario
{
    if(receptores==nil)
        receptores = [[NSMutableArray alloc] init];
     
    if(![receptores containsObject:beneficiario])
        [receptores addObject:beneficiario];


    for (UIView *aView in [_receptoresScroll subviews]){
        _agregar.text = @"Agregar beneficiario";
        if([language rangeOfString:@"es"].location == NSNotFound)
            _agregar.text = @"Add beneficiary";

        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
            [aView removeFromSuperview];
        }
    }
    
    UIImageView *_miimageView = [[UIImageView alloc] init];
    
    for(int i = 0; i<[receptores count]; i++){
        _agregar.text=@"";
        NSDictionary *receptDictionary = [receptores objectAtIndex:i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:i];
        [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
        button.clipsToBounds = YES;
        
        button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
        button.layer.borderColor=[UIColor grayColor].CGColor;
        button.layer.borderWidth=0.5f;
        
        if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
        }else{
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
        }
        
        [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
        
        UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
        [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
        [_receptoresScroll addSubview:button];
        [_receptoresScroll addSubview:nombreReceptor];
        [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)deleteUser:(id)sender {
    UIButton * btn = (UIButton *)sender;
    
    AlmosaferAlertView *alert;
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirm"
                 contentText:@"You want to delete this message Beneficiary?"
                 leftButtonTitle:@"Yes"
                 rightButtonTitle:@"No"];
    }
    else
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirmar"
                 contentText:@"Desea borrar de este mensaje al Beneficiario?"
                 leftButtonTitle:@"Si"
                 rightButtonTitle:@"No"];
        
    }
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
    alert.leftBlock = ^() {
        [receptores removeObjectAtIndex:btn.tag];
        
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";

            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
            if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                [aView removeFromSuperview];
            }
        }
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text=@"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
    };

}

-(IBAction)onAction:(id)sender
{
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        NSLog(@"connection unavailable");
        [self guardarvideo];
    }
    else
    {
        //connection available
        NSLog(@"connection available");
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
        
        
        if (receptores.count>0)
        {
            [self guardarvideo];
        }
        else
        {
            [self hideProgressHUD];
            
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Picture message"
                         contentText:@"You must select who to send the message"
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
                
            }
            else
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Mensaje de Fotos"
                         contentText:@"Debe seleccionar a quién enviarle el mensaje"
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
            }
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
        }
    }
    
}

- (void)guardarvideo {
    borradorBool = NO;
    receptoresString = @"";
    for (int i = 0; i<receptores.count; i++) {
        if([receptoresString isEqualToString:@""])
            receptoresString = [receptores objectAtIndex:i][@"beneficiary"][@"iduser"];
        else
            receptoresString = [NSString stringWithFormat:@"%@,%@", receptoresString, [receptores objectAtIndex:i][@"beneficiary"][@"iduser"]];
        
    }
    
    NSLog(@"%@", receptoresString);
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"] forKey:@"user_id"];
    [_params setObject:[NSString stringWithFormat:@"%@", _titulo.text] forKey:@"m[title]"];
    [_params setObject:@"4" forKey:@"m[type_file_id]"];
    [_params setObject:@"1" forKey:@"m[type_message_id]"];
    [_params setObject:[NSString stringWithFormat:@"%@", _fecha.text] forKey:@"m[date_view]"];
    [_params setObject:@"" forKey:@"m[description]"];
    [_params setObject:receptoresString forKey:@"m[beneficiaries]"];
    
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        [_params setObject:@"2" forKey:@"m[language]"];
    }
    else
    {
        [_params setObject:@"1" forKey:@"m[language]"];
    }
    
    
    NSString *urlString = @"http://fluie.com/fluie/messageRestFull/createMessage";
    
    if(_borradorDiccionario!=nil && !_esBorrador)
    {
        urlString = @"http://fluie.com/fluie/messageRestFull/updateMessage";
        [_params setObject:_borradorDiccionario[@"idmessage"] forKey:@"m[idmessage]"];
    }
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
        NSLog(@"%@", param);
        NSLog(@"%@", [_params objectForKey:param]);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    for(int i = 0; i<[thumbsArray count]; i++){
        NSDictionary *imageDictionary = [thumbsArray objectAtIndex:i];
        NSData *imageData = imageDictionary[@"data"];
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        NSString* FileParamConstant = @"m[file][]"; //MODIFICAR
        
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"imagen%i.jpg\"\r\n", FileParamConstant, i] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setURL:requestURL]; // ENVIO EL REQUEST
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            
            if(returnData) {
                
                NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                AlmosaferAlertView *alert;
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Accept"
                             rightButtonTitle:nil];
                    
                }
                else
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Aceptar"
                             rightButtonTitle:nil];
                    
                }
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
//                AlmosaferAlertView *alert;
//                if([language rangeOfString:@"es"].location == NSNotFound)
//                {
//                    alert = [[AlmosaferAlertView alloc]
//                             initWithTitle:@"Picture message"
//                             contentText:responseDic[@"msg"]
//                             leftButtonTitle:@"Accept"
//                             rightButtonTitle:nil];
//                }
//                else
//                {
//                    alert = [[AlmosaferAlertView alloc]
//                             initWithTitle:@"Mensaje de Fotos"
//                             contentText:responseDic[@"msg"]
//                             leftButtonTitle:@"Aceptar"
//                             rightButtonTitle:nil];
//                    
//                }
                
                alert.leftBlock = ^() {
                    for (UIView *aView in [_receptoresScroll subviews]){
                        _agregar.text = @"Agregar beneficiario";
                        if([language rangeOfString:@"es"].location == NSNotFound)
                            _agregar.text = @"Add beneficiary";

                        if ([aView isKindOfClass:[UIButton class]]){
                            [aView removeFromSuperview];
                        }
                        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                            [aView removeFromSuperview];
                        }
                    }
                    if(![responseDic[@"result"] isEqualToString:@"error"]){
                        _titulo.text = @"";
                        _fecha.text = @"";
                        _fecha.hidden = YES;
                        [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                        if([language rangeOfString:@"es"].location == NSNotFound) {
                            _titulo.placeholder = @"Message title";
                            _selectPlaceholder.text = @"Program to";
                            [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                            
                        }
                        for (UIView *aView in [_thumbScroll subviews]){
                            if ([aView isKindOfClass:[UIButton class]]){
                                [aView removeFromSuperview];
                            }
                        }
                        [_preview setHidden:YES];
                        if(_esBorrador || _borradorDiccionario!=nil)
                            [self.navigationController popViewControllerAnimated:YES];
                        else
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                        
                        
                    }
                    else
                    {
                        NSError* error;
                        
                        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
                        
                        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
                        if(jsondata!=nil){
                            
                            NSMutableDictionary* thumbs = [[NSMutableDictionary alloc] init];
                            
                            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                            if(_esBorrador)
                            {
                                int index = (int)[json indexOfObject:_borradorDiccionario];
                                [json removeObjectAtIndex:index];
                                
                            }
                            [json addObject:@{
                                                  @"idtypefile" : @"4",
                                                  @"title" : _titulo.text,
                                                  @"fecha" : _fecha.text,
                                                  @"url" : urlString,
                                                  @"params" : _params,
                                                  @"data" : @"",
                                                  @"receptores" : receptores,
                                                  @"thumbs" : thumbsArray
                                                  }];
                                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                                [data writeToFile:zipFilePath atomically:YES];
                            
                            
                            
                            
                            
                        }
                        else
                        {
                            NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                            
                            NSMutableDictionary* thumbs = [[NSMutableDictionary alloc] init];
                            
                            [arrayfavoritos addObject:@{
                                                        @"idtypefile" : @"4",
                                                        @"title" : _titulo.text,
                                                        @"fecha" : _fecha.text,
                                                        @"url" : urlString,
                                                        @"params" : _params,
                                                        @"data" : @"",
                                                        @"receptores" : receptores,
                                                        @"thumbs" : thumbsArray
                                                        }];
                            
                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                            [data writeToFile:zipFilePath atomically:YES];
                            
                            
                        }
                        
                        AlmosaferAlertView *alert;
                        if([language rangeOfString:@"es"].location == NSNotFound)
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Connection error"
                                     contentText:@"It has no internet at the time,the message has been saved in drafts,you will be automatically sent once you have connection"
                                     leftButtonTitle:@"Accept"
                                     rightButtonTitle:nil];
                            
                        }
                        else
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Error de conexión"
                                     contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
                        }
                        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                        alert.leftBlock = ^(){
                            borradorBool = YES;
                            for (UIView *aView in [_receptoresScroll subviews]){
                                _agregar.text = @"Agregar beneficiario";
                                if([language rangeOfString:@"es"].location == NSNotFound)
                                    _agregar.text = @"Add beneficiary";

                                if ([aView isKindOfClass:[UIButton class]]){
                                    [aView removeFromSuperview];
                                }
                                if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                                    [aView removeFromSuperview];
                                }
                            }
                            
                            _titulo.text = @"";
                            _fecha.text = @"";
                            _fecha.hidden = YES;
                            [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                            if([language rangeOfString:@"es"].location == NSNotFound) {
                                _titulo.placeholder = @"Message title";
                                _selectPlaceholder.text = @"Program to";
                                [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                                
                            }
                            for (UIView *aView in [_thumbScroll subviews]){
                                if ([aView isKindOfClass:[UIButton class]]){
                                    [aView removeFromSuperview];
                                }
                            }
                            [_preview setHidden:YES];
                            receptores = nil;
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                        };                    }
                    
                };
            }
        }else{
           
            NSError* error;
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
            
            NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
            if(jsondata!=nil){
                
                NSMutableDictionary* thumbs = [[NSMutableDictionary alloc] init];
                
                NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                if(_esBorrador){
                    int index = (int)[json indexOfObject:_borradorDiccionario];
                    [json removeObjectAtIndex:index];
                    
                }
                [json addObject:@{
                                  @"idtypefile" : @"4",
                                  @"title" : _titulo.text,
                                  @"fecha" : _fecha.text,
                                  @"url" : urlString,
                                  @"params" : _params,
                                  @"data" : @"",
                                  @"receptores" : receptores,
                                  @"thumbs" : thumbsArray
                                  }];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                [data writeToFile:zipFilePath atomically:YES];
                
            }else{
                NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                
                NSMutableDictionary* thumbs = [[NSMutableDictionary alloc] init];
                
                [arrayfavoritos addObject:@{
                                            @"idtypefile" : @"4",
                                            @"title" : _titulo.text,
                                            @"fecha" : _fecha.text,
                                            @"url" : urlString,
                                            @"params" : _params,
                                            @"data" : @"",
                                            @"receptores" : receptores,
                                            @"thumbs" : thumbsArray
                                  }];

                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                [data writeToFile:zipFilePath atomically:YES];
                
                
            }
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                 alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Connection error"
                                             contentText:@"It has no internet at the time,the message has been saved in drafts,you will be automatically sent once you have connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
            }
            else
            {
                 alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Error de conexión"
                                             contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
               
            }
             NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            alert.leftBlock = ^() {
                //[self performSegueWithIdentifier:@"gotoHistorialVid" sender:nil];
                borradorBool = YES;
                [self performSegueWithIdentifier:@"gotoHistorial" sender:nil]; //[self.navigationController popViewControllerAnimated:YES];
            };
        }
    }];
    [dataTask resume];
    
}

- (IBAction)onPreview:(id)sender {
    UIButton * btn = (UIButton *)sender;
    selectedPreview = (int)btn.tag;
    _preview.hidden = NO;
    
    _fotoPreview.image = [btn imageForState:UIControlStateNormal];
    
    [_preview setAlpha:0.0];
    [_preview setHidden:NO];
    
    [UIView animateWithDuration:0.5
                          delay:0.3
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         [_preview setAlpha:1.0];
     }
                     completion:^(BOOL finished)
     {
         if(finished)
         {
             
             if(finished)
                 NSLog(@"Hurray. Label fadedIn & fadedOut");
             
         }
     }];
    
}

- (void)datePickerValueChanged:(id)sender{
    
    date = datePicker.date;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    [_fecha setText:[df stringFromDate:date]];
}

- (void)closeKeyboard:(id)sender{ [_fecha resignFirstResponder]; }

- (void)viewDidAppear:(BOOL)animated {
    
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        [self performSegueWithIdentifier:@"goLogin" sender:nil];
    }else{
        [self loadFreeSpace];
    }
}

-(void)loadFreeSpace {
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/freeSpace/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"GET"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            if(returnData){
                NSArray *freeSpace = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                for(id space in freeSpace) {
                    _lbl2.text = [NSString stringWithFormat:@"%@ Mb", space[@"size"]];
                    float progressVal = 0;
                    
                    NSLog(@"%@",space);
                    
                    if([space[@"space"] intValue]!=0){
                        progressVal = ([space[@"space"] floatValue] - [space[@"free"] floatValue])/[space[@"space"] floatValue];
                        
                    }
                    
                    [_progressview setProgress:progressVal animated:YES];
                    
                    NSLog(@"%f",progressVal);
                    [_progressview setProgress:progressVal animated:YES];
                    
                }
            }
        }else{
            
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
    }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    if(_esBorrador || _borradorDiccionario!=nil)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)backThumb:(id)sender {
    [_preview setAlpha:1.0];
    [_preview setHidden:NO];
    
    [UIView animateWithDuration:0.5
                          delay:0.3
                        options:UIViewAnimationOptionCurveLinear | UIViewAnimationOptionAllowUserInteraction
                     animations:^(void)
     {
         [_preview setAlpha:0.0];
     }
                     completion:^(BOOL finished)
     {
         if(finished)
         {
             
             if(finished){
                 [_preview setHidden:YES];
                 [_fotoPreview setImage:nil];
             }

             
         }
     }];
}


-(IBAction)delThumb:(id)sender {
    UIButton * btn = (UIButton *)sender;
    
    [thumbsArray removeObjectAtIndex:selectedPreview];
    for (UIView *aView in [_thumbScroll subviews]){
        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
    }
    for(int i = 0; i<[thumbsArray count]; i++){
        NSDictionary *imageDictionary = [thumbsArray objectAtIndex:i];
        UIImage *selectedImage = [UIImage imageWithData:imageDictionary[@"data"]];
        UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
        // Crop the image to a square (yikes, fancy!)
        UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
        CGImageRef imageRef = [selectedImage CGImage];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:i];
        [button addTarget:self action:@selector(onPreview:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(i*(65), 0, 63.0, 63.0);//width and height
        [button setImage:croppedImage forState:UIControlStateNormal];
        [_thumbScroll addSubview:button];
        
        if(i>4){
            [_thumbScroll setContentSize:CGSizeMake(((i*65) + 65), _thumbScroll.frame.size.height)];
        }
    }
    _preview.hidden = YES;
}

- (IBAction)onBtnTakePhoto:(id)sender
{
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        UIActionSheet *actionSheetView = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:@"Take photo"
                                          otherButtonTitles:@"Take existing photo", nil];
        
        [actionSheetView showInView:[self view]];
    }
    else
    {
        UIActionSheet *actionSheetView = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          destructiveButtonTitle:@"Tomar foto"
                                          otherButtonTitles:@"Tomar foto existente", nil];
        
        [actionSheetView showInView:[self view]];
    }
}

-(void)selectPhoto {
    
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    [pickerController setAllowsEditing:NO];
    [pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [self presentViewController:pickerController animated:YES completion:^{}];
    //}
    
}

-(void)takePhoto {
    
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    [pickerController setAllowsEditing:NO];
    [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    
    [self presentViewController:pickerController animated:YES completion:NULL];
    //}
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    for (UIView *aView in [_thumbScroll subviews]){
        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
    }
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    
    NSData* imageData = UIImageJPEGRepresentation(selectedImage, 0.6);
    
    [thumbsArray addObject:@{
                             @"data" : imageData,
                             }];
    
    NSLog(@"%lu",(unsigned long)thumbsArray.count);
    
    for(int i = 0; i<[thumbsArray count]; i++){
        NSDictionary *imageDictionary = [thumbsArray objectAtIndex:i];
        UIImage *selectedImage = [UIImage imageWithData:imageDictionary[@"data"]];
        UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
        // Crop the image to a square (yikes, fancy!)
        UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
        CGImageRef imageRef = [selectedImage CGImage];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:i];
        [button addTarget:self action:@selector(onPreview:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(i*(65), 0, 63.0, 63.0);//width and height
        [button setImage:croppedImage forState:UIControlStateNormal];
        [_thumbScroll addSubview:button];
        
        if(i>4){
            [_thumbScroll setContentSize:CGSizeMake(((i*65) + 65), _thumbScroll.frame.size.height)];
        }
    }

    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
    
    //}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
    
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self takePhoto];
            break;
        }
        case 1: {
            [self selectPhoto];
            break;
        }
        default:
            break;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"gotoHistorial"]) {
        HistorialViewController *HistorialViewController = segue.destinationViewController;
        HistorialViewController.type_file_id = @"4";
        HistorialViewController.borrador = borradorBool;
    }
    
    if([segue.identifier isEqualToString:@"gotoAdd"]){
        BeneficiariosListViewController *controller = [segue destinationViewController];
        controller.delegado = self;
    }
}

#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.view setFrame:CGRectMake(0,190 - textField.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height)];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    mytextField = nil;
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    mytextField = nil;
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
/*
 * Traigo mis Beneficiarios
 */
-(void)getBeneficiarios {
    //Muestro la vista de cargando
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];        
    }
    
    NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?with=beneficiary&filter=%@", escBox]; //query para pasarle a la url
    
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"beneficiaryUser"] stringByAppendingString:query];
    
    //NSString *urlString = @"http://fluie.com/fluie/api/beneficiaryUser";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];//Todos los beneficiarios de la Base de Datos
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSMutableArray *datasourcetemporal = [NSMutableArray arrayWithObjects:nil];
            if(arrayresponse.count){
                beneficiarios = arrayresponse;
                NSMutableArray *receptoresTemporal = [_borradorDiccionario[@"messageBeneficiaries"] mutableCopy];
                for(int i = 0; i<[receptoresTemporal count]; i++){
                    NSDictionary *receptDictionaryTemporal = [receptoresTemporal objectAtIndex:i];
                    
                    for (int j=0; j<[beneficiarios count]; j++) {
                        NSDictionary *beneficiario = [beneficiarios objectAtIndex:j];
                        NSLog(@"%@ %@", beneficiario[@"beneficiary_id"], receptDictionaryTemporal[@"beneficiary_id"]);
                        if([beneficiario[@"beneficiary_id"] isEqualToString:receptDictionaryTemporal[@"beneficiary_id"]]){
                            [receptores addObject:beneficiario];
                            break;
                        }
                    }
                }
                //receptores
                for (UIView *aView in [_receptoresScroll subviews]){
                    _agregar.text = @"Agregar beneficiario";
                    if([language rangeOfString:@"es"].location == NSNotFound)
                        _agregar.text = @"Add beneficiary";

                    if ([aView isKindOfClass:[UIButton class]]){
                        [aView removeFromSuperview];
                    }
                    if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                        [aView removeFromSuperview];
                    }
                }
                
                UIImageView *_miimageView = [[UIImageView alloc] init];
                
                for(int i = 0; i<[receptores count]; i++){
                    _agregar.text = @"";
                    NSMutableDictionary *receptDictionary = [[receptores objectAtIndex:i] mutableCopy];
                    NSLog(@"%@", receptDictionary);
                    
                    /*
                     * Armo la foto del beneficiario
                     */
                    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", receptDictionary[@"beneficiary_id"]];
                    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                    
                    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
                    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                    [request setHTTPMethod: @"POST"];
                    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                    [request setHTTPBody: myRequestData];
                    
                    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                    NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    NSLog(@"foto%@", foto[@"image"]);
                    receptDictionary[@"beneficiary"] = [receptDictionary[@"beneficiary"] mutableCopy];
                    [receptDictionary[@"beneficiary"] setObject:foto[@"image"] forKey:@"image"];
                    /*
                     * Fin foto del beneficiario
                     */
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    [button setTag:i];
                    [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
                    button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
                    button.clipsToBounds = YES;
                    
                    button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
                    button.layer.borderColor=[UIColor grayColor].CGColor;
                    button.layer.borderWidth=0.5f;
                    
                    if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
                    }else{
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
                    }
                    
                    [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
                    
                    UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
                    if(receptDictionary[@"beneficiary"][@"name"])
                        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
                    [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
                    [_receptoresScroll addSubview:button];
                    [_receptoresScroll addSubview:nombreReceptor];
                    [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
                }
            }
        }else{
            
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
    
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [self.view addGestureRecognizer:tap];
    _selectPlaceholder.text = @"";
    if(([titulo isEqualToString:@"Fecha"])||([titulo isEqualToString:@"Date"]))
    {
        _fecha.hidden = NO;
    }else{
        _fecha.text = @"";
        _fecha.hidden = YES;
        
    }
}


- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    NSLog(@"%@", _btnSelect.titleLabel.text);
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (IBAction)selectClicked:(id)sender {
    [self.view removeGestureRecognizer:tap];
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Ausencia", @"Fecha",nil];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
        arr = [NSArray arrayWithObjects:@"Absence", @"Date",nil];
    
    if(dropDown == nil) {
        CGFloat f = 80;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else
    {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}


@end
