//
//  RegistroViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 11/24/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"


@interface RegistroViewController : UIViewController<NIDropDownDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    NIDropDown *dropDownPregrado;
}

@end
