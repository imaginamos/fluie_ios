//
//  RegistroViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/24/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "RegistroViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"

@interface RegistroViewController (){
    __weak IBOutlet UIActivityIndicatorView *reloj;
    NSArray *contentsPaisArray;
    NSString *paisSelected;
    UITextField *mytextField;
    BOOL isPhotoSelected;
    BOOL aceptoTerminos;
    NSData *image;
    
}

@property (weak, nonatomic) IBOutlet UIButton *aceptoBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UITextField *nombre;
@property (weak, nonatomic) IBOutlet UITextField *apellido;
@property (weak, nonatomic) IBOutlet UITextField *telefono;
@property (weak, nonatomic) IBOutlet UITextField *movil;
@property (weak, nonatomic) IBOutlet UITextField *pais;
@property (weak, nonatomic) IBOutlet UITextField *direccion;
@property (weak, nonatomic) IBOutlet UITextField *mail;
@property (weak, nonatomic) IBOutlet UITextField *mail2;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *password2;
@property (weak, nonatomic) IBOutlet UIButton *pais_Button;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIButton *cancel;


@end

@implementation RegistroViewController

-(void)viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _nombre.placeholder = @"Name";
        _apellido.placeholder = @"Lastname";
        _telefono.placeholder = @"Phone";
        _movil.placeholder = @"Mobile";
        [_pais_Button setTitle:@"Country" forState:UIControlStateNormal];
        _direccion.placeholder = @"Address";
        _mail.placeholder = @"Email";
        _mail2.placeholder = @"Optional Email";
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
    }
}

-(IBAction)onMasculino:(id)sender{
    NSLog(@"masculino");
    
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(!aceptoTerminos)
    {
        [_aceptoBtn setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        [_aceptoBtn setImage:nil forState:UIControlStateNormal];
    }
    aceptoTerminos = !aceptoTerminos;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [reloj setHidden:YES];
    
    //_miimageView.layer.cornerRadius = 100;
    self.imgPhoto.layer.masksToBounds = YES;
    // border radius
    [self.imgPhoto.layer setCornerRadius:45.0f];
    // border
    [self.imgPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.imgPhoto.layer setBorderWidth:2.5f];
    // drop shadow
    [self.imgPhoto.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.imgPhoto.layer setShadowOpacity:0.8];
    [self.imgPhoto.layer setShadowRadius:3.0];
    [self.imgPhoto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    /**/
    [self cargaPaises];
    dropDownPregrado.delegate = self;
    
    [self.myscroll setScrollEnabled:YES];
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609)];
    
    aceptoTerminos = false;
    
    // Do any additional setup after loading the view.
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void) cargaPaises {
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/country/";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        contentsPaisArray=arrayresponse;
    }
}

-(IBAction)onregister:(id)sender
{
    
    
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Cargando Beneficiarios";
        
    }
    
    
    if(![_pais_Button.titleLabel.text isEqualToString:@""]){
        for (int i=0; i<contentsPaisArray.count; i++) {
            NSDictionary *diccionario = [contentsPaisArray objectAtIndex:i];
            NSLog(@"%@", diccionario);
            NSLog(@"title label %@", paisSelected);

            if([diccionario[@"country"] isEqualToString:[paisSelected stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                paisSelected = diccionario[@"idcountry"];
        }
        
    }
    
    NSLog(@"%@", paisSelected);
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user";
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:@"0" forKey:@"space"];
    [_params setObject:[NSString stringWithFormat:@"%i", [paisSelected intValue]] forKey:@"country_id"];
    [_params setObject:[NSString stringWithFormat:@"%@", _telefono.text] forKey:@"phone"];
    [_params setObject:[NSString stringWithFormat:@"%@", _password.text] forKey:@"pass"];
    [_params setObject:[NSString stringWithFormat:@"%@", _password2.text] forKey:@"confirm_password"];
    [_params setObject:[NSString stringWithFormat:@"123123"] forKey:@"identification"];
    [_params setObject:[NSString stringWithFormat:@"1"] forKey:@"terms"];
    [_params setObject:[NSString stringWithFormat:@"%@", _mail.text] forKey:@"email"];
    [_params setObject:[NSString stringWithFormat:@"%@", _mail2.text] forKey:@"email_opcional"];
    [_params setObject:[NSString stringWithFormat:@"%@", _nombre.text] forKey:@"nombre"];
    [_params setObject:[NSString stringWithFormat:@"%@", _nombre.text] forKey:@"name"];
    [_params setObject:[NSString stringWithFormat:@"%@", _apellido.text] forKey:@"name_last"];
    [_params setObject:[NSString stringWithFormat:@"%@", _direccion.text] forKey:@"address"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"image";
    
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    //[request setTimeoutInterval:30];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    NSData *imageData = UIImageJPEGRepresentation(self.imgPhoto.image, 1.0);
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    
    if(aceptoTerminos){
        [request setURL:requestURL];
        
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
        NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        NSLog(@"response %@",returnString);
        NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        
        if(!arrayresponse[@"error"]){
            [reloj setHidden:YES];
            [reloj stopAnimating];
            [self hideProgressHUD];
            
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Registro"
                                         contentText:@"Ud se ha registrado con éxito"
                                         leftButtonTitle:@"Aceptar"
                                         rightButtonTitle:nil];
            [alert showInView:self.view];
            
            alert.leftBlock = ^() {
                NSLog(@"left button clicked");
                //arrayresponse[@"iduser"]
                [[NSUserDefaults standardUserDefaults] setObject:arrayresponse[@"iduser"] forKey:@"idUser"];
                [self dismissViewControllerAnimated:YES completion:nil];
                
            };
            
            //[self performSegueWithIdentifier:@"logged" sender:nil];
        }else if(arrayresponse[@"error"]){
            id val = nil;
            NSArray *values = [arrayresponse[@"error"] allValues];
            if ([values count] != 0)
                val = [values objectAtIndex:0];
            
            NSLog(@"%@", val);
            NSString *respuesta=@"";
            if([val isKindOfClass:[NSArray class]]){
                respuesta = [val objectAtIndex:0];
            }else if([val isKindOfClass:[NSString class]]){
                respuesta = val;
            }else{
                respuesta = val;
            }
            NSLog(@"%@", respuesta);
            [self hideProgressHUD];
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Registro"
                                         contentText:[NSString stringWithFormat:@"%@", respuesta]
                                         leftButtonTitle:@"Aceptar"
                                         rightButtonTitle:nil];
            [alert showInView:self.view];
            
            alert.leftBlock = ^() {
                NSLog(@"left button clicked");
                
            };
            [reloj setHidden:YES];
            [reloj stopAnimating];
        }
    }else{
        [self hideProgressHUD];
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Registro"
                                     contentText:[NSString stringWithFormat:@"Debe aceptar los Términos y Condiciones"]
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
            
        };
    }
    
    
    
    
}

- (IBAction)onPais:(id)sender
{
    [mytextField resignFirstResponder];

    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsPaisArray valueForKey:@"country"];
    if(dropDownPregrado == nil) {
        CGFloat f = 80;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}

#pragma mark Photo

- (IBAction)onBtnTakePhoto:(id)sender {
    UIActionSheet *actionSheetView = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Tomar foto" otherButtonTitles:@"Tomar foto existente", nil];
    
    [actionSheetView showInView:[self view]];
}

-(void)selectPhoto {
   /* if (IS_IPAD)
    {
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary];
        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = hasCamera ? UIImagePickerControllerSourceTypePhotoLibrary :    UIImagePickerControllerSourceTypePhotoLibrary;
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:picker];
        CGRect popoverRect = [self.view convertRect:[yourBtn frame]
                                           fromView:[yourBtn superview]];
        
        popoverRect.size.width = MIN(popoverRect.size.width, 100) ;
        popoverRect.origin.x = popoverRect.origin.x;
        
        [popoverController
         presentPopoverFromRect:popoverRect
         inView:self.view
         permittedArrowDirections:UIPopoverArrowDirectionAny
         animated:YES];
    }
    else{*/
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        [pickerController setAllowsEditing:NO];
        [pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
        [self presentViewController:pickerController animated:YES completion:^{}];
    //}
    
}

-(void)takePhoto {
    /*if (IS_IPAD)
    {
        BOOL hasCamera = [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
        UIImagePickerController* picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = hasCamera ? UIImagePickerControllerSourceTypeCamera :    UIImagePickerControllerSourceTypeCamera;
        
        popoverController = [[UIPopoverController alloc] initWithContentViewController:picker];
        CGRect popoverRect = [self.view convertRect:[yourBtn frame]
                                           fromView:[yourBtn superview]];
        
        popoverRect.size.width = MIN(popoverRect.size.width, 100) ;
        popoverRect.origin.x = popoverRect.origin.x;
        
        [popoverController
         presentPopoverFromRect:popoverRect
         inView:self.view
         permittedArrowDirections:UIPopoverArrowDirectionAny
         animated:YES];
    }
    else{*/
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        [pickerController setAllowsEditing:NO];
        [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        
        [self presentViewController:pickerController animated:YES completion:NULL];
    //}
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    /*if (IS_IPAD) {
        [picker dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }];
        UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
        isPhotoSelected = YES;
        
        [self.imgPhoto setImage:selectedImage];
        if (popoverController != nil) {
            [popoverController dismissPopoverAnimated:YES];
            popoverController=nil;
        }
        
    }
    else{*/
        UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
        isPhotoSelected = YES;
    
        UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
        // Crop the image to a square (yikes, fancy!)
        UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
    
        [self.imgPhoto setImage:croppedImage];
        
        NSData* imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
        NSString* imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/saved_example_image.png"];
        [imageData writeToFile:imagePath atomically:YES];
        [picker dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }];
        
    //}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    /*if (IS_IPAD) {
        [picker dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }];
        if (popoverController != nil) {
            [popoverController dismissPopoverAnimated:YES];
            popoverController=nil;
        }
    }
    else{*/
        [picker dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }];
    //}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self takePhoto];
            break;
        }
        case 1: {
            [self selectPhoto];
            break;
        }
        default:
            break;
    }
}

#pragma mark NIDropDown

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    paisSelected = titulo;
    if(sender == _pais_Button)
        [[NSUserDefaults standardUserDefaults] setObject:titulo forKey:@"pais_Button"];
    
    //[self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDownPregrado = nil;
}


-(void)dismissKeyboard {
    [mytextField resignFirstResponder];
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609+253)];
    
   /* if (textField == _Username_TextField) {
        [self.scrollView setContentOffset:CGPointMake(0, 120) animated:YES];
        
    }else{
        [self.scrollView setContentOffset:CGPointMake(0, 140) animated:YES];
    }*/
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   /* if (textField == _Username_TextField) {
        [_Username_TextField resignFirstResponder];
        [_password_TextField becomeFirstResponder];
    } else if (textField == _password_TextField) {*/
        // here you can define what happens
        // when user presses return on the email field
        [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609)];
        [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
        
        [textField resignFirstResponder];
   // }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
}



@end
