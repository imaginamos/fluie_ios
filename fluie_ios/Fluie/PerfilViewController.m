//
//  PerfilViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/27/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "PerfilViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"
#import "NIDropDown.h"
#import "UIImageView+WebCache.h"
#import "CambiarPswViewController.h"

@interface PerfilViewController ()
{
    NSString *language_id;
    NSDictionary *usuario;
    NSArray *contentsPaisArray;
    NSString *paisSelected;
    UITextField *mytextField;
    BOOL isPhotoSelected;
    NSData *image;
    NSString * language;
    NSArray *contentsTipoArray;
    NSString *tipoSelected;
    NIDropDown *dropDownPregrado;
    UITapGestureRecognizer *tap;
}

@property (weak, nonatomic) IBOutlet UIImageView *grupo;
@property (weak, nonatomic) IBOutlet UIButton *cambiarbtn;
@property (weak, nonatomic) IBOutlet UILabel *aceptoLbl;
@property (weak, nonatomic) IBOutlet UILabel *terminosLbl;
@property (weak, nonatomic) IBOutlet UIButton *comprasBtn;
@property (weak, nonatomic) IBOutlet UIButton *miscardsBtn;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UITextField *nombre;
@property (weak, nonatomic) IBOutlet UITextField *direccion;
@property (weak, nonatomic) IBOutlet UITextField *mail;
@property (weak, nonatomic) IBOutlet UITextField *movil;
@property (weak, nonatomic) IBOutlet UITextField *apellido;
@property (weak, nonatomic) IBOutlet UITextField *tel;
@property (weak, nonatomic) IBOutlet UITextField *pais;
@property (weak, nonatomic) IBOutlet UITextField *mail2;
@property (weak, nonatomic) IBOutlet UIButton *pais_button;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UITextField *documento;
@property (weak, nonatomic) IBOutlet UITextField *tipoddocumento;
@property (weak, nonatomic) IBOutlet UIButton *tipo_Button;

@end

@implementation PerfilViewController

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    dropDownPregrado.delegate = self;
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"gender_id"] isEqualToString:@"1"])
        [_imgPhoto sd_setImageWithURL:_fotoUrl placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
    else
        [_imgPhoto sd_setImageWithURL:_fotoUrl placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
    
    self.imgPhoto.layer.masksToBounds = YES;
    // border radius
    [self.imgPhoto.layer setCornerRadius:45.0f];
    // border
    [self.imgPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.imgPhoto.layer setBorderWidth:2.5f];
    // drop shadow
    [self.imgPhoto.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.imgPhoto.layer setShadowOpacity:0.8];
    [self.imgPhoto.layer setShadowRadius:3.0];
    [self.imgPhoto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    
    /**/
    
    dropDownPregrado.delegate = self;
    
    [self.myscroll setScrollEnabled:YES];
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 739)];
    
//    [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"];
    
    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    if(returnData){
        NSDictionary *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        
        usuario = response;
        NSLog(@"%@", usuario);
        _nombre.text = usuario[@"name"];
        _direccion.text = usuario[@"address"];
        _mail.text = usuario[@"email"];
        _mail2.text = usuario[@"email_optional"];
        _movil.text = usuario[@"phone"];
        _apellido.text = usuario[@"name_last"];
        _tel.text = usuario[@"phone"];
        _documento.text = usuario[@"identification"];
        paisSelected = usuario[@"country_id"];
        tipoSelected = usuario[@"type_identification_id"];
        
        
        
        [self hideProgressHUD];
    
        
        [self cargaPaises];
        
    }
    // Do any additional setup after loading the view.
}


-(void)dismissKeyboard {
    if(mytextField!=nil){
        [mytextField resignFirstResponder];
        [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 739)];
        [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];//[self dismissViewControllerAnimated:YES completion:nil];
}

-(void)viewWillAppear:(BOOL)animated {
    
    /*
     Verifico el idioma del teléfono
     */
    
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    language_id = @"1";
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        language_id = @"2";
        _nombre.placeholder = @"Name";
        _apellido.placeholder = @"Lastname";
        _tel.placeholder = @"Phone";
        _movil.placeholder = @"Mobile";
        if (_tipo_Button.titleLabel.text.length == 0)
        {
            _tipoddocumento.placeholder = @"Document type";
        }
        if (_pais_button.titleLabel.text.length == 0)
        {
            _pais.placeholder = @"Country";
        }
        _direccion.placeholder = @"Address";
        _mail.placeholder = @"Email";
        _mail2.placeholder = @"Optional Email";
        _aceptoLbl.text = @"I accept ours";
        _terminosLbl.text = @"Terms & Conditions";
        [_saveBtn setTitle:@"SAVE" forState:UIControlStateNormal];
        [_cambiarbtn setTitle:@"Change  password" forState:UIControlStateNormal];
        _grupo.image = [UIImage imageNamed: @"grupoingles.png"];
    }
    
}


-(void) cargaPaises
{
    NSString *urlString = @"http://fluie.com/fluie/api/country/";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    //NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        contentsPaisArray=arrayresponse;
        
        for (int i=0; i<contentsPaisArray.count; i++) {
            NSDictionary *diccionario = [contentsPaisArray objectAtIndex:i];
            NSLog(@"diccionario[idcountry]: %@", diccionario[@"idcountry"]);
            NSLog(@"usuario[country_id]: %@", usuario[@"country_id"]);
            if([diccionario[@"idcountry"] isEqualToString: usuario[@"country_id"]]){
                [_pais_button setTitle:diccionario[@"country"] forState:UIControlStateNormal];
                _pais.placeholder = @"";

            }
        
        }
    }
    
    [self cargaTipo];
}

-(void) cargaTipo {
    /*
     * Hago el request de las notificaciones
     */
    
  //  NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    language_id = @"1";
    if([language rangeOfString:@"es"].location == NSNotFound) {
        language_id = @"2";
    }
    
    NSString *myRequestString = [NSString stringWithFormat:@"language_id=%@", language_id];
    NSLog(@"requeststring: %@", myRequestString);
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/typeIdentification/getList"];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"POST"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [requestFS setHTTPBody: myRequestData];
    
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSLog(@"%@", arrayresponse);
            
            if(arrayresponse.count){
                contentsTipoArray=arrayresponse;
                for (int i=0; i<contentsTipoArray.count; i++) {
                    NSDictionary *diccionario = [contentsTipoArray objectAtIndex:i];
                    if([diccionario[@"idtype_identification"] isEqualToString: usuario[@"type_identification_id"]]){
                        [_tipo_Button setTitle:diccionario[@"name"] forState:UIControlStateNormal];
                        _tipoddocumento.placeholder = @"";
                    }
                    
                }
            }
        }else
        {
           // NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
        }
    }];
    [dataTask resume];
}


-(void)subirdatos
{
    
    [self dismissKeyboard];
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    
    if(![_pais_button.titleLabel.text isEqualToString:@""]){
        for (int i=0; i<contentsPaisArray.count; i++) {
            NSDictionary *diccionario = [contentsPaisArray objectAtIndex:i];
            NSLog(@"%@", diccionario);
            NSLog(@"title label %@", paisSelected);
            
            if([diccionario[@"country"] isEqualToString:[paisSelected stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                paisSelected = diccionario[@"idcountry"];
        }
        
    }
    
    if(![_tipo_Button.titleLabel.text isEqualToString:@""]){
        for (int i=0; i<contentsTipoArray.count; i++) {
            NSDictionary *diccionario = [contentsTipoArray objectAtIndex:i];
            NSLog(@"%@", diccionario[@"name"]);
            NSLog(@"title label %@", tipoSelected);
            
            if([diccionario[@"name"] isEqualToString:tipoSelected])
                tipoSelected = diccionario[@"idtype_identification"];
            
        }
        
    }
    
    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/user/updateUser"];
    
    
    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@&country_id=%i&phone=%@&identification=%@&terms=1&email=%@&email_optional=%@&name=%@&name_last=%@&address=%@&type_identification_id=%i", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"],  [paisSelected intValue], _movil.text, _documento.text, _mail.text, _mail2.text, _nombre.text, _apellido.text, _direccion.text, [tipoSelected intValue]];
    
    
    NSLog(@"%@", myRequestString);
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            NSLog(@"response %@",returnString);
            NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            //[self subirfoto];
            if(!arrayresponse[@"error"])
            {
                AlmosaferAlertView *alert;
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                     alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Profile"
                                                 contentText:arrayresponse[@"msg"]
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                }
                else
                {
                     alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Perfil"
                                                 contentText:arrayresponse[@"msg"]
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    
                }
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                alert.leftBlock = ^() {
                    [self.navigationController popViewControllerAnimated:YES];
                };
                
                //[self performSegueWithIdentifier:@"logged" sender:nil];
            }else if(arrayresponse[@"error"])
            {
                
                id val = nil;
                NSArray *values = [arrayresponse[@"error"] allValues];
                if ([values count] != 0)
                    val = [values objectAtIndex:0];
                
                NSString *respuesta=@"";
                if([val isKindOfClass:[NSArray class]]){
                    respuesta = [val objectAtIndex:0];
                }else if([val isKindOfClass:[NSString class]]){
                    respuesta = val;
                }else{
                    respuesta = val;
                }
                NSLog(@"%@", respuesta);
                
                AlmosaferAlertView *alert;
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    alert = [[AlmosaferAlertView alloc]initWithTitle:@"Registry"
                                                         contentText:[NSString stringWithFormat:@"%@", respuesta]
                                                     leftButtonTitle:@"Accept"
                                                    rightButtonTitle:nil];
                    
                }
                else
                {
                    alert = [[AlmosaferAlertView alloc]initWithTitle:@"Registro"
                                                         contentText:[NSString stringWithFormat:@"%@", respuesta]
                                                     leftButtonTitle:@"Aceptar"
                                                    rightButtonTitle:nil];
                }
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
        else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                
                
               NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            
        }
        
        [self hideProgressHUD];
    }];
    [dataTask resume];
    
    
    
}
-(IBAction)onregister:(id)sender
{
    //primero subimos la foto si existe
    if(isPhotoSelected){
        [self dismissKeyboard];
        NSString *urlString = @"http://fluie.com/fluie/user/SavePhotoUser";
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"] forKey:@"user_id"];
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        NSString* FileParamConstant = @"image";
        NSURL* requestURL = [NSURL URLWithString:urlString];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            NSLog(@"%@", param);
            NSLog(@"%@", [_params objectForKey:param]);
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        if(isPhotoSelected){
            NSData *imageData = UIImageJPEGRepresentation(self.imgPhoto.image, 1.0);
            image = imageData;
            if (imageData) {
                [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                [body appendData:imageData];
                [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            }
            
            [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            
            // setting the body of the post to the reqeust
            [request setHTTPBody:body];
            
            // set the content-length
            NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setURL:requestURL];
            
            NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
                if (error == nil) {
                    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                    NSLog(@"response %@",returnString);
                    NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    [self subirdatos];
                    if(!arrayresponse[@"error"]){
                        
                    }else if(arrayresponse[@"error"]){
                        //[self subirdatos];
                    }
                }else{
                    if([language rangeOfString:@"es"].location == NSNotFound)
                    {
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Notification"
                                                     contentText:@"Could not establish communication. Check your connection"
                                                     leftButtonTitle:@"Accept"
                                                     rightButtonTitle:nil];
                        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                    }
                    else
                    {
                        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                     initWithTitle:@"Notificacion"
                                                     contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                                     leftButtonTitle:@"Aceptar"
                                                     rightButtonTitle:nil];
                        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                    }
                    [self hideProgressHUD];
                }
            }];
            [dataTask resume];

        }else{
            
           [self subirdatos];
        }
    }else
        [self subirdatos];
    
}

- (IBAction)onTipo:(id)sender
{
    [self.view removeGestureRecognizer:tap];
    [mytextField resignFirstResponder];
    
    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsTipoArray valueForKey:@"name"];
    if(dropDownPregrado == nil) {
        CGFloat f = 100;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)onPais:(id)sender
{
    [self.view removeGestureRecognizer:tap];
    [mytextField resignFirstResponder];
    
    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsPaisArray valueForKey:@"country"];
    if(dropDownPregrado == nil) {
        CGFloat f = 100;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}

#pragma mark Photo

- (IBAction)onBtnTakePhoto:(id)sender {
    UIActionSheet *actionSheetView = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Tomar foto" otherButtonTitles:@"Tomar foto existente", nil];
    
    [actionSheetView showInView:[self view]];
}

-(void)selectPhoto {
   
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    [pickerController setAllowsEditing:NO];
    [pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
    [self presentViewController:pickerController animated:YES completion:^{}];
    
}

-(void)takePhoto {
   
    UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
    pickerController.delegate = self;
    [pickerController setAllowsEditing:NO];
    [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    
    [self presentViewController:pickerController animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {

    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    isPhotoSelected = YES;
    
    UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
    // Crop the image to a square (yikes, fancy!)
    UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
    
    [self.imgPhoto setImage:croppedImage];
    
    NSData* imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    NSString* imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/saved_example_image.png"];
    [imageData writeToFile:imagePath atomically:YES];
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
   
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self takePhoto];
            break;
        }
        case 1: {
            [self selectPhoto];
            break;
        }
        default:
            break;
    }
}

#pragma mark NIDropDown

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [self.view addGestureRecognizer:tap];
    if(sender == _pais_button){
        [[NSUserDefaults standardUserDefaults] setObject:titulo forKey:@"pais_Button"];
        _pais.placeholder = @"";
        paisSelected = titulo;
        
    }else{
        _tipoddocumento.placeholder = @"";
        tipoSelected = titulo;
    }
    //[self rel];
}

-(void)rel{
    dropDownPregrado = nil;
}


#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 739+253)];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 739)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    mytextField = nil;
    [textField resignFirstResponder];
    // }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    mytextField=nil;
    
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
     if ([segue.identifier isEqualToString:@"cambiar"]) {
         CambiarPswViewController *vc = segue.destinationViewController;
         vc.usuario = usuario;
     }
 }
 


@end
