//
//  LeftViewController.m
//  CESA
//
//  Created by daniel on 9/18/14.
//  Copyright (c) 2014 daniel. All rights reserved.
//

#import "LeftViewController.h"
#import "SlideNavigationContorllerAnimatorFade.h"
#import "SlideNavigationContorllerAnimatorSlide.h"
#import "SlideNavigationContorllerAnimatorScale.h"
#import "SlideNavigationContorllerAnimatorScaleAndFade.h"
#import "SlideNavigationContorllerAnimatorSlideAndFade.h"



@interface LeftViewController ()

@end

@implementation LeftViewController

#pragma mark - UIViewController Methods -

- (id)initWithCoder:(NSCoder *)aDecoder
{
	self.slideOutAnimationEnabled = YES;
	
	return [super initWithCoder:aDecoder];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
    CGSize result = [[UIScreen mainScreen] bounds].size;
    int scrollint = self.tableView.frame.size.height;
    if (result.height == 480){
        scrollint = 480;
    }
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width,scrollint);
    NSLog(@"%f", self.tableView.frame.size.height);
	self.tableView.separatorColor = [UIColor lightGrayColor];
	
	UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftMenu.jpg"]];
	self.tableView.backgroundView = imageView;
	
	self.view.layer.borderWidth = .6;
	self.view.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return 11;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
	UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"NavigationBar.png"]];
    // 3. Add a label
    UILabel* headerLabel = [[UILabel alloc] init];
    headerLabel.frame = CGRectMake(15, 20, tableView.frame.size.width - 5, 35);
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.textColor = [UIColor whiteColor];
    headerLabel.text = @"AECESA";
    headerLabel.textAlignment = NSTextAlignmentLeft;
   
        headerLabel.font=[UIFont fontWithName:@"Roboto" size:20];

    // 4. Add the label to the header view
    [imgView addSubview:headerLabel];
    
	return imgView;
   // return nil;
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch(buttonIndex) {
        case 0: //"No" pressed
            //do something?
            break;
        case 1: //"Yes" pressed
            //here you pop the viewController
            [[NSUserDefaults standardUserDefaults] setObject:@"NO" forKey:@"sesion"];
            [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
            [[SlideNavigationController sharedInstance] popToRootViewControllerAnimated:YES];
            break;
    }
}


@end
