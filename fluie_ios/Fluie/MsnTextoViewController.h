//
//  MsnTextoViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/12/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BeneficiariosListViewController.h"
#import "NIDropDown.h"

@interface MsnTextoViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UIDocumentPickerDelegate, BeneficiariosListViewControllerDelegate, NIDropDownDelegate>{
    NIDropDown *dropDownPregrado;
}


@property (strong, nonatomic) NSDictionary *borradorDiccionario;
@property (assign, nonatomic) BOOL *esBorrador;

@end
