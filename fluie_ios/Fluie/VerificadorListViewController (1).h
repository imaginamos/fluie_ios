//
//  VerificadorListViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/11/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerificadorListViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>

@end
