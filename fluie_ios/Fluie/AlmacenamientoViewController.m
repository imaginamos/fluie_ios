//
//  AlmacenamientoViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 1/24/16.
//  Copyright © 2016 Carlos Robinson. All rights reserved.
//

#import "AlmacenamientoViewController.h"
#import "MBProgressHUD.h"
#import "AlmosaferAlertView.h"
#import "HistorialViewController.h"

@interface AlmacenamientoViewController ()
{
    NSArray *dataSource;
    NSArray *espacioArray;
    NSArray *userArray;
    NSMutableDictionary *beneficiario;
    NSString *_type_file_id;
    NSMutableArray *borrador;
    NSMutableArray* jsonBorrador;
    BOOL borradorBool;
        NSString * language;
}
@property (weak, nonatomic) IBOutlet UILabel *free;
@property (weak, nonatomic) IBOutlet UILabel *lbl;
@property (weak, nonatomic) IBOutlet UIButton *buyBtn;
@property (weak, nonatomic) IBOutlet UIView *vistaBorradores;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;
@property (weak, nonatomic) IBOutlet UITableView *myTableView;
@property (weak, nonatomic) IBOutlet UITableView *myTableViewBorrador;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIProgressView *progressview;
@property (weak, nonatomic) IBOutlet UILabel *taza;
@property (weak, nonatomic) IBOutlet UILabel *porcentage;

@end

@implementation AlmacenamientoViewController

-(void)viewWillAppear:(BOOL)animated
{
    [_progressview setProgress:1 animated:NO];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        [_buyBtn setTitle:@"Buy more space" forState:UIControlStateNormal];
        [_segmentedControl setTitle:@"My Messages" forSegmentAtIndex:0];
        [_segmentedControl setTitle:@"Drafts" forSegmentAtIndex:1];
        _lbl.text=@"Required Space";
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    jsonBorrador = [[NSMutableArray alloc] init];
    // Do any additional setup after loading the view.
    //Seteamos la Tabla 1
    _myTableView.backgroundColor = [UIColor clearColor];
    _myTableView.separatorColor = [UIColor clearColor];
    _myTableView.delegate = self;
    _myTableView.dataSource = self;
    self.myTableView.separatorColor = [UIColor clearColor];
    //Seteamos la Tabla de borradores
    _myTableViewBorrador.backgroundColor = [UIColor clearColor];
    _myTableViewBorrador.separatorColor = [UIColor clearColor];
    _myTableViewBorrador.delegate = self;
    _myTableViewBorrador.dataSource = self;
    self.myTableViewBorrador.separatorColor = [UIColor clearColor];
    //Seteamos el segmented control
    _segmentedControl.selectedSegmentIndex = 0;
    [_segmentedControl addTarget:self action:@selector(MySegmentControlAction:) forControlEvents: UIControlEventValueChanged];
    
}

- (void)MySegmentControlAction:(UISegmentedControl *)segment
{
    if(segment.selectedSegmentIndex == 0)
    {
        [_vistaBorradores setHidden:YES];
    }else{
        [_vistaBorradores setHidden:NO];
        
    }
}

-(void)loadFreeSpace {
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/freeSpace/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"GET"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            if(returnData){
                NSArray *freeSpace = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                for(id space in freeSpace) {
                    NSLog(@"%@",space);
                    //_free.text = [NSString stringWithFormat:@"%@ MB", space[@"free"]];
                    float progressVal = 0;
                    
                    NSLog(@"%@",space);
                    _taza.text = [NSString stringWithFormat:@"%@ MB/%@ MB", space[@"size"], space[@"space"]];
                    float porcentage = 100*[space[@"size"] floatValue]/[space[@"space"] floatValue];
                    _porcentage.text = [NSString stringWithFormat:@"%.2f %%", porcentage];
                    
                    if([space[@"space"] intValue]!=0){
                        progressVal = ([space[@"space"] floatValue] - [space[@"free"] floatValue])/[space[@"space"] floatValue];
                    }
                    
                    [_progressview setProgress:progressVal animated:YES];
                    
                    NSLog(@"%f",progressVal);
                    [_progressview setProgress:progressVal animated:YES];
                }
            }
        }
        else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
        }
    }];
    [dataTask resume];
}

- (void)viewDidAppear:(BOOL)animated {
    
    [self loadFreeSpace];
    [self getdataSource];
}

-(void)getdataSourceBorradores {
    
}

-(void)getdataSource {
    
    borrador = [[NSMutableArray alloc] init];
    jsonBorrador = [[NSMutableArray alloc] init];
    [borrador removeAllObjects];
    [jsonBorrador removeAllObjects];
    NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
    
    NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
    if(jsondata!=nil){
        NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
        
        //NSLog(@"json: %@", json);
        float videoSize=0;
        float fotoSize=0;
        float textoSize=0;
        float audioSize=0;
        
        //[jsonBorrador addObject:];
        for (int i = 0; i<json.count; i++) {
            NSMutableDictionary *diccionario = [json objectAtIndex:i];
            if(diccionario !=nil){
                NSData *mydata= diccionario[@"data"];
                
                if([diccionario[@"idtypefile"] isEqualToString:@"1"]){
                    videoSize = videoSize + (float)mydata.length/1024.0f/1024.0f;
                }
                if([diccionario[@"idtypefile"] isEqualToString:@"2"]){
                    audioSize = audioSize + (float)mydata.length/1024.0f/1024.0f;
                }
                if([diccionario[@"idtypefile"] isEqualToString:@"3"]){
                    textoSize = textoSize + (float)mydata.length/1024.0f/1024.0f;
                }
                if([diccionario[@"idtypefile"] isEqualToString:@"4"]){
                    NSArray *thumbsArray = diccionario[@"thumbs"];
                    for(int i = 0; i<[thumbsArray count]; i++){
                        NSDictionary *imageDictionary = [thumbsArray objectAtIndex:i];
                        mydata= imageDictionary[@"data"];
                        NSLog(@"Tamano original: %f", (float)mydata.length);
                        NSLog(@"File size is : %.2f MB",(float)mydata.length/1024.0f/1024.0f);
                        
                        fotoSize = fotoSize + (float)mydata.length/1024.0f/1024.0f;
                        NSLog(@"Foto size is %.2f MB", fotoSize);
                    }
                    
                }
            }
        }
        
        NSDictionary *videodic =@{
                                  @"idtypefile" : @"1",
                                  @"iduser" : @"87",
                                  @"size" : [NSString stringWithFormat:@"%.2f", videoSize],
                                  @"type" : @"Video",
                                  };
        
        NSDictionary *audiodic =@{
                                  @"idtypefile" : @"2",
                                  @"iduser" : @"87",
                                  @"size" : [NSString stringWithFormat:@"%.2f", audioSize],
                                  @"type" : @"Audio",
                                  };
        NSDictionary *archivodic =@{
                                    @"idtypefile" : @"3",
                                    @"iduser" : @"87",
                                    @"size" : [NSString stringWithFormat:@"%.2f", textoSize],
                                    @"type" : @"Archivo",
                                    };
        
        
        NSDictionary *fotodic =@{
                                 @"idtypefile" : @"4",
                                 @"iduser" : @"87",
                                 @"size" : [NSString stringWithFormat:@"%.2f", fotoSize],
                                 @"type" : @"Foto",
                                 };
        
        _free.text = [NSString stringWithFormat:@"%.2f MB", (fotoSize+videoSize+audioSize+textoSize)];
        
        if(fotoSize!=0.0)
            [jsonBorrador addObject:fotodic];
        
        if(videoSize!=0.0)
            [jsonBorrador addObject:videodic];
        
        if(textoSize!=0.0)
            [jsonBorrador addObject:archivodic];
        
        if(audioSize!=0.0)
            [jsonBorrador addObject:audiodic];
        
        [_myTableViewBorrador reloadData];
    }
    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/sizeFiles/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSLog(@"%@", urlString);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        [self getdataSourceBorradores];
        if (error == nil) {
           
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            //NSLog(@"%@", arrayresponse);
            if(arrayresponse.count){
                
                NSMutableArray *temporal = [arrayresponse mutableCopy];
                for (int i=0; i<temporal.count; i++) {
                    NSDictionary *dicTempo = (NSDictionary *) [temporal objectAtIndex:i];
                    if (dicTempo[@"idtypefile"] == (id)[NSNull null]){
                        [temporal removeObject:dicTempo];
                    }
                }
                
                dataSource=temporal;
                [_myTableView reloadData];
                
            }
        }else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView==_myTableViewBorrador)
            return jsonBorrador.count;
    
    return dataSource.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 56;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    NSMutableDictionary *diccionario = [[NSMutableDictionary alloc] init];
    
    //Si es la tabla de borradores o no, escogemos la fuente de datos
    if(tableView==_myTableViewBorrador)
         diccionario = [jsonBorrador[indexPath.row] mutableCopy];
    else
        diccionario = [dataSource[indexPath.row] mutableCopy];
    
    //NSLog(@"%@",diccionario);
    
    
    UILabel *nombre = (UILabel *)[cell viewWithTag:102];
    nombre.text = diccionario[@"type"];
    
    UILabel *espacio = (UILabel *)[cell viewWithTag:103];
    espacio.text = [NSString stringWithFormat:@"%@ Mb", diccionario[@"size"]];
    
    UIImageView *miimageView = (UIImageView *)[cell viewWithTag:101];
    if(diccionario[@"idtypefile"])
    if (diccionario[@"idtypefile"] != (id)[NSNull null]){
    
        if([diccionario[@"idtypefile"] intValue] == 1)
            miimageView.image = [UIImage imageNamed:@"Fill 205.png"];
        
        
        if([diccionario[@"idtypefile"] intValue] == 2)
            miimageView.image = [UIImage imageNamed:@"Fill 200.png"];
        
        if([diccionario[@"idtypefile"] intValue] == 3)
            miimageView.image = [UIImage imageNamed:@"Fill 156.png"];
        
        if([diccionario[@"idtypefile"] intValue] == 4)
            miimageView.image = [UIImage imageNamed:@"Fill 95.png"];
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *diccionario = [[NSMutableDictionary alloc] init];
    if(tableView == _myTableView){
        borradorBool = NO;
        diccionario = [dataSource[indexPath.row] mutableCopy];
    }else{
        borradorBool = YES;
        diccionario = [jsonBorrador[indexPath.row] mutableCopy];
    }

    _type_file_id = diccionario[@"idtypefile"];
    [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"gotoHistorial"]) {
        HistorialViewController *HistorialViewController = segue.destinationViewController;
        HistorialViewController.type_file_id = _type_file_id;
        HistorialViewController.borrador = borradorBool; 
    }
    
}


@end
