//
//  MsnVideoViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/11/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "MsnVideoViewController.h"
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "AlmosaferAlertView.h"
#import "UIImage+Resize.h"
#import "MBProgressHUD.h"
#import "HistorialViewController.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"


@interface MsnVideoViewController ()
{
    UIDatePicker *datePicker;
    NSDate *date;
    UIToolbar *keyboardToolbar;
    NSMutableArray * receptores;
    NSString *receptoresString;
    __weak IBOutlet UIButton *addButon;
    NSMutableArray *borrador;
    UITextField *mytextField;
    NSArray *beneficiarios;
    NIDropDown *dropDown;
    UITapGestureRecognizer *tap;
    NSString * language;
    BOOL borradorBool;

}
@property (weak, nonatomic) IBOutlet UIImageView *videoThumbView;
@property (strong, nonatomic) NSURL *videoURL;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UILabel *selectPlaceholder;
@property (weak, nonatomic) IBOutlet UILabel *agregar;
@property (weak, nonatomic) IBOutlet UITextField *fecha;
@property (strong, nonatomic) MPMoviePlayerController *videoController;
@property (weak, nonatomic) IBOutlet UITextField *titulo;
@property (weak, nonatomic) IBOutlet UIProgressView *progressview;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIScrollView *receptoresScroll;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;

@end

@implementation MsnVideoViewController

-(void) viewWillAppear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:YES];

        [_progressview setProgress:1 animated:NO];
}
-(void)dismissKeyboard {
    if(mytextField!=nil)
        [mytextField resignFirstResponder];
    
}

- (IBAction)selectClicked:(id)sender {
    [self.view removeGestureRecognizer:tap];
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Ausencia", @"Fecha",nil];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
        arr = [NSArray arrayWithObjects:@"Absence", @"Date",nil];
    if(dropDown == nil) {
        CGFloat f = 80;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _lbl1.text = @"Used space";
        _titulo.placeholder = @"Message title";
        _selectPlaceholder.text = @"Program to";
        _agregar.text = @"Add beneficiary";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        
    }

    // Do any additional setup after loading the view.
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    receptores = [[NSMutableArray alloc] init];
    
    if (keyboardToolbar == nil) {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar setBarTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Cerrar" style:UIBarButtonItemStyleDone target:self action:@selector(closeKeyboard:)];
        
        [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
        
        
    }
    
    _fecha.inputAccessoryView = keyboardToolbar;
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    _fecha.inputView = datePicker;
    
    if(_esBorrador){
        _selectPlaceholder.text = @"";
        NSLog(@"%@", _borradorDiccionario[@"title"]);
                NSLog(@"%@", _borradorDiccionario[@"fecha"]);
                NSLog(@"%@", _borradorDiccionario[@"receptores"]);
        [_videoView setHidden:NO];
        self.videoController = [[MPMoviePlayerController alloc] init];
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"myMove.mp4"];
        
        [_borradorDiccionario[@"data"] writeToFile:path atomically:YES];
        NSURL *moveUrl = [NSURL fileURLWithPath:path];
        self.videoURL = moveUrl;
        [self.videoController setContentURL:self.videoURL];
        
        UIImage *thumbnail = [self.videoController thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
        [self.videoController stop];
        UIImage *scaledImage = [thumbnail resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(319, 164) interpolationQuality:kCGInterpolationHigh];
        // Crop the image to a square (yikes, fancy!)
        UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width - 319)/2, (scaledImage.size.height -164)/2, 319, 164)];
        
        [self.videoThumbView setImage:croppedImage];
        _titulo.text = _borradorDiccionario[@"title"];
        _fecha.text = _borradorDiccionario[@"fecha"];
        
        if (_borradorDiccionario[@"fecha"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"fecha"];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }

        receptores = [_borradorDiccionario[@"receptores"] mutableCopy];
        
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";
            
            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
            if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                [aView removeFromSuperview];
            }
        }
        
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text = @"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
    }else if(_borradorDiccionario!=nil){
        _selectPlaceholder.text = @"";
        
        NSLog(@"%@", _borradorDiccionario);
        [_videoView setHidden:NO];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button addTarget:self action:@selector(descargarvideo:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(0, 0, _videoView.frame.size.width, _videoView.frame.size.height);//width and height
        //[button setImage:croppedImage forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor blackColor]];
        [button setTitle:@"Descargar Video" forState:UIControlStateNormal];
        [_videoView addSubview:button];
        [button addTarget:self action:@selector(descargarvideo:) forControlEvents:UIControlEventTouchUpInside];
        /**/
        _titulo.text = _borradorDiccionario[@"title"];
        NSLog(@"%@", _borradorDiccionario[@"date_view"]);
        if (_borradorDiccionario[@"date_view"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"date_view"];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }
        
        
        
        [self getBeneficiarios];
        
        
    }
    
    dropDownPregrado.delegate = self;
    
}

- (IBAction)descargarvideo:(id)sender{
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    UIButton *tempotal = (UIButton *) sender;
    
    self.videoController = [[MPMoviePlayerController alloc] init];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:_borradorDiccionario[@"messageFiles"][0][@"file"]];
    NSURL *imageURL = [NSURL URLWithString:_borradorDiccionario[@"messageFiles"][0][@"url"]];
    NSLog(@"%@", _borradorDiccionario);
    NSLog(@"imagen: %@", _borradorDiccionario[@"messageFiles"]);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: imageURL];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        
        if (error == nil) {
            [returnData writeToFile:path atomically:YES];
            NSURL *moveUrl = [NSURL fileURLWithPath:path];
            self.videoURL = moveUrl;
            [self.videoController setContentURL:self.videoURL];
            
            UIImage *thumbnail = [self.videoController thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
            [self.videoController stop];
            UIImage *scaledImage = [thumbnail resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(319, 164) interpolationQuality:kCGInterpolationHigh];
            // Crop the image to a square (yikes, fancy!)
            UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width - 319)/2, (scaledImage.size.height -164)/2, 319, 164)];
            [self.videoThumbView setImage:croppedImage];
            [tempotal removeFromSuperview];
            [self hideProgressHUD];
        }else{
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
    //NSData *dataFile = [NSData dataWithContentsOfURL:imageURL];
    
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void) PlayerAdded:(BeneficiariosListViewController *)controller andPlayer:(NSDictionary *)beneficiario
{
    if(receptores==nil)
        receptores = [[NSMutableArray alloc] init];
        
    
    if(![receptores containsObject:beneficiario])
        [receptores addObject:beneficiario];

    
    for (UIView *aView in [_receptoresScroll subviews]){
        _agregar.text = @"Agregar beneficiario";
        if([language rangeOfString:@"es"].location == NSNotFound)
            _agregar.text = @"Add beneficiary";
        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
            [aView removeFromSuperview];
        }
    }
    
    UIImageView *_miimageView = [[UIImageView alloc] init];
    
    for(int i = 0; i<[receptores count]; i++){
        _agregar.text = @"";
        NSDictionary *receptDictionary = [receptores objectAtIndex:i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:i];
        [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
        button.clipsToBounds = YES;
        
        button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
        button.layer.borderColor=[UIColor grayColor].CGColor;
        button.layer.borderWidth=0.5f;
        
        if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
        }else{
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
        }
        
        [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
        
        UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
        [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
        [_receptoresScroll addSubview:button];
        [_receptoresScroll addSubview:nombreReceptor];
        [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)deleteUser:(id)sender
{
    UIButton * btn = (UIButton *)sender;
    AlmosaferAlertView *alert;
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
         alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Confirm"
                                     contentText:@"You want to delete this message Beneficiary?"
                                     leftButtonTitle:@"Yes"
                                     rightButtonTitle:@"No"];
        
    }
    else
    {
         alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Confirmar"
                                     contentText:@"Desea borrar de este mensaje al Beneficiario?"
                                     leftButtonTitle:@"Si"
                                     rightButtonTitle:@"No"];
    }
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
    alert.leftBlock = ^() {
        [receptores removeObjectAtIndex:btn.tag];
        
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";
            if ([aView isKindOfClass:[UIButton class]] || [aView isKindOfClass:[UILabel class]]){
                if(aView != _agregar)
                [aView removeFromSuperview];
            }
        }
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text = @"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
    };
    
}

-(IBAction)onAction:(id)sender {
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        NSLog(@"connection unavailable");
        [self guardarvideo];
    }
    else
    {
        //connection available
        NSLog(@"connection available");
    
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                _progressHUD.detailsLabelText = @"Sending the message may take some time.";
            }
            else
            {
                _progressHUD.detailsLabelText = @"El envío del mensaje puede demorar un poco";
            }
            
        }
        
        if (receptores.count>0)
        {
            [self guardarvideo];
        }
        else
        {
            [self hideProgressHUD];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Picture message"
                                             contentText:@"You must select who to send the message"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Mensaje de Fotos"
                                             contentText:@"Debe seleccionar a quién enviarle el mensaje"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
        }
    }
}

- (void)guardarvideo {
    borradorBool = NO;

    if(_esBorrador){
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
        
        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
        if(jsondata!=nil){
            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
            for (int i = 0; i<json.count; i++) {
                NSMutableDictionary *diccionario = [json objectAtIndex:i];
                if(diccionario == _borradorDiccionario){
                    [json removeObjectAtIndex:i];
                }
            }
        }
    }
    
    receptoresString = @"";
    for (int i = 0; i<receptores.count; i++) {
        if([receptoresString isEqualToString:@""])
            receptoresString = [receptores objectAtIndex:i][@"beneficiary"][@"iduser"];
        else
            receptoresString = [NSString stringWithFormat:@"%@,%@", receptoresString, [receptores objectAtIndex:i][@"beneficiary"][@"iduser"]];
        
    }
    NSLog(@"%@", receptores);
    NSLog(@"%@", receptoresString);
    
    
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"] forKey:@"user_id"];
    [_params setObject:[NSString stringWithFormat:@"%@", _titulo.text] forKey:@"m[title]"];
    [_params setObject:@"1" forKey:@"m[type_file_id]"];
    [_params setObject:@"1" forKey:@"m[type_message_id]"];
    [_params setObject:[NSString stringWithFormat:@"%@", _fecha.text] forKey:@"m[date_view]"];
    [_params setObject:@"" forKey:@"m[description]"];
    [_params setObject:receptoresString forKey:@"m[beneficiaries]"];
    
    
    NSString *urlString = @"http://fluie.com/fluie/messageRestFull/createMessage";
    
    if(_borradorDiccionario!=nil && !_esBorrador){
        urlString = @"http://fluie.com/fluie/messageRestFull/updateMessage";
        [_params setObject:_borradorDiccionario[@"idmessage"] forKey:@"m[idmessage]"];
    }
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"m[file]"; //MODIFICAR
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
       // NSLog(@"%@", param);
       // NSLog(@"%@", [_params objectForKey:param]);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // agregar el video, tomar primero su data
    NSData *videoData = [NSData dataWithContentsOfFile:[self.videoURL path]];
    //NSLog(@"%@", videoData);
    
    if (videoData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"video.mp4\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]]; //video/mp4
        [body appendData:[@"Content-Type: video/mp4\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:videoData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setURL:requestURL]; // ENVIO EL REQUEST
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            
            
            
            if(returnData) {
                NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                NSLog(@"response %@",returnString);
                
                NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                
                AlmosaferAlertView *alert;
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Accept"
                             rightButtonTitle:nil];
                }
                else
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Aceptar"
                             rightButtonTitle:nil];
                    
                }
//                if([language rangeOfString:@"es"].location == NSNotFound)
//                {
//                    alert = [[AlmosaferAlertView alloc]
//                             initWithTitle:@"Video Message"
//                             contentText:responseDic[@"msg"]
//                             leftButtonTitle:@"Accept"
//                             rightButtonTitle:nil];
//                }
//                else
//                {
//                    alert = [[AlmosaferAlertView alloc]
//                             initWithTitle:@"Mensaje de Video"
//                             contentText:responseDic[@"msg"]
//                             leftButtonTitle:@"Aceptar"
//                             rightButtonTitle:nil];
//                }
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };

                alert.leftBlock = ^() {
                    for (UIView *aView in [_receptoresScroll subviews]){
                        _agregar.text = @"Agregar beneficiario";
                        if([language rangeOfString:@"es"].location == NSNotFound)
                            _agregar.text = @"Add beneficiary";
                        if ([aView isKindOfClass:[UIButton class]]){
                            [aView removeFromSuperview];
                        }
                        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                            [aView removeFromSuperview];
                        }
                    }
                    if(![responseDic[@"result"] isEqualToString:@"error"]){
                        _titulo.text = @"";
                        _fecha.text = @"";
                        _selectPlaceholder.text = @"Programar para";
                        if([language rangeOfString:@"es"].location == NSNotFound) {
                            _titulo.placeholder = @"Message title";
                            _selectPlaceholder.text = @"Program to";
                            [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                            
                        }
                        _fecha.hidden = YES;
                        [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                        receptores = nil;
                        //receptores
                        
                        [_videoView setHidden:YES];
                        
                        if(_esBorrador || _borradorDiccionario!=nil)
                            [self.navigationController popViewControllerAnimated:YES];
                        else
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                    }else{
                        NSError* error;
                        
                        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
                        
                        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
                        if(jsondata!=nil){
                            
                            
                            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                            if(_esBorrador){
                                int index = (int)[json indexOfObject:_borradorDiccionario];
                                [json removeObjectAtIndex:index];
                                
                            }
                            [json addObject:@{
                                              @"idtypefile" : @"1",
                                              @"title" : _titulo.text,
                                              @"fecha" : _fecha.text,
                                              @"url" : urlString,
                                              @"params" : _params,
                                              @"receptores" : receptores,
                                              @"data" : [NSData dataWithContentsOfFile:[self.videoURL path]],
                                              }];
                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                            [data writeToFile:zipFilePath atomically:YES];
                        }else{
                            NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                            [arrayfavoritos addObject:@{
                                                        @"idtypefile" : @"1",
                                                        @"title" : _titulo.text,
                                                        @"fecha" : _fecha.text,
                                                        @"url" : urlString,
                                                        @"params" : _params,
                                                        @"receptores" : receptores,
                                                        @"data" : [NSData dataWithContentsOfFile:[self.videoURL path]],
                                                        }];
                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                            [data writeToFile:zipFilePath atomically:YES];
                            
                        }
                        
                        AlmosaferAlertView *alert;
                        if([language rangeOfString:@"es"].location == NSNotFound)
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Connection error"
                                     contentText:@"It has no internet at the time , the message has been saved in drafts , you will be automatically sent once you have connection"
                                     leftButtonTitle:@"Accept"
                                     rightButtonTitle:nil];
                        }
                        else
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Error de conexión"
                                     contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
                            
                        }
                        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                        alert.leftBlock = ^(){
                            borradorBool = YES;

                            for (UIView *aView in [_receptoresScroll subviews]){
                                _agregar.text = @"Agregar beneficiario";
                                if([language rangeOfString:@"es"].location == NSNotFound)
                                    _agregar.text = @"Add beneficiary";
                                if ([aView isKindOfClass:[UIButton class]]){
                                    [aView removeFromSuperview];
                                }
                                if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                                    [aView removeFromSuperview];
                                }
                            }
                            _titulo.text = @"";
                            _fecha.text = @"";
                            _selectPlaceholder.text = @"Programar para";
                            if([language rangeOfString:@"es"].location == NSNotFound) {
                                _titulo.placeholder = @"Message title";
                                _selectPlaceholder.text = @"Program to";
                                [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                                
                            }
                            _fecha.hidden = YES;
                            [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                            receptores = nil;
                            //receptores
                            
                            [_videoView setHidden:YES];
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                        };
                        
                    }
                
                    //[self performSegueWithIdentifier:@"gotoHistorial" sender:nil]; //[self.navigationController popViewControllerAnimated:YES];
                };
                
                
                
            }
        }else{
            NSError* error;
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
            
            NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
            if(jsondata!=nil){

                
                NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                if(_esBorrador){
                    int index = (int)[json indexOfObject:_borradorDiccionario];
                    [json removeObjectAtIndex:index];
                    
                }
                [json addObject:@{
                                  @"idtypefile" : @"1",
                                  @"title" : _titulo.text,
                                  @"fecha" : _fecha.text,
                                  @"url" : urlString,
                                  @"params" : _params,
                                  @"receptores" : receptores,
                                  @"data" : [NSData dataWithContentsOfFile:[self.videoURL path]],
                                  }];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                [data writeToFile:zipFilePath atomically:YES];
            }else{
                NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                [arrayfavoritos addObject:@{
                                @"idtypefile" : @"1",
                                @"title" : _titulo.text,
                                @"fecha" : _fecha.text,
                                  @"url" : urlString,
                                  @"params" : _params,
                                  @"receptores" : receptores,
                                  @"data" : [NSData dataWithContentsOfFile:[self.videoURL path]],
                                  }];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                [data writeToFile:zipFilePath atomically:YES];
                
            }
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Connection error"
                         contentText:@"It has no internet at the time , the message has been saved in drafts , you will be automatically sent once you have connection"
                         leftButtonTitle:@"Accept"
                         rightButtonTitle:nil];

            }
            else
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Error de conexión"
                         contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
                

            }
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            alert.leftBlock = ^() {
                borradorBool = YES;

                //[self performSegueWithIdentifier:@"gotoHistorialVid" sender:nil];
                [self performSegueWithIdentifier:@"gotoHistorial" sender:nil]; //[self.navigationController popViewControllerAnimated:YES];
            };
            
        }
    }];
    [dataTask resume];
    
}

- (void)datePickerValueChanged:(id)sender{
    
    date = datePicker.date;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    [_fecha setText:[df stringFromDate:date]];
}

- (void)closeKeyboard:(id)sender{ [_fecha resignFirstResponder]; }

- (void)viewDidAppear:(BOOL)animated {
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        [self performSegueWithIdentifier:@"goLogin" sender:nil];
    }else{
        [self loadFreeSpace];
    }
}

-(void)loadFreeSpace {
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/freeSpace/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"GET"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        
        
        [self hideProgressHUD];
        if (error == nil) {
            if(returnData){
                NSArray *freeSpace = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                
                NSLog(@"%@",freeSpace);
                for(id space in freeSpace)
                {
                     _lbl2.text = [NSString stringWithFormat:@"%@ Mb", space[@"size"]];
                    
                    float progressVal = 0;
                    if([space[@"space"] intValue]!=0){
                        progressVal = ([space[@"space"] floatValue] - [space[@"free"] floatValue])/[space[@"space"] floatValue];
                    }
                    [_progressview setProgress:progressVal animated:YES];
                    [_progressview setProgress:progressVal animated:YES];
                }
            }
        }else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
        }
    }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    if(_esBorrador || _borradorDiccionario!=nil)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}

-(IBAction)captureVideo:(id)sender
{
    
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        UIActionSheet *actionSheetView = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          destructiveButtonTitle:@"Record a video"
                                          otherButtonTitles:@"Use Existing Video", nil];
        
        [actionSheetView showInView:[self view]];
    }
    else
    {
        UIActionSheet *actionSheetView = [[UIActionSheet alloc]
                                          initWithTitle:nil
                                          delegate:self
                                          cancelButtonTitle:@"Cancelar"
                                          destructiveButtonTitle:@"Grabar Video"
                                          otherButtonTitles:@"Usar Video existente", nil];
        
        [actionSheetView showInView:[self view]];
    }
    
    /**/
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self takeVideo];
            break;
        }
        case 1: {
            [self selectVideo];
            break;
        }
        default:
            break;
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    [self.videoView setHidden:NO];
    self.videoURL = info[UIImagePickerControllerMediaURL];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
    self.videoController = [[MPMoviePlayerController alloc] init];
    
    [self.videoController setContentURL:self.videoURL];
    
    UIImage *thumbnail = [self.videoController thumbnailImageAtTime:1.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
    [self.videoController stop];
    UIImage *scaledImage = [thumbnail resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(319, 164) interpolationQuality:kCGInterpolationHigh];
    // Crop the image to a square (yikes, fancy!)
    UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width - 319)/2, (scaledImage.size.height -164)/2, 319, 164)];
    
    [self.videoThumbView setImage:croppedImage];
    NSData *videoData = [NSData dataWithContentsOfFile:[self.videoURL path]];
    _lbl2.text = [NSString stringWithFormat:@"%.2f MB", (float)videoData.length/1024.0f/1024.0f];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    }];
}

-(void)selectVideo {
    
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    videoPicker.delegate = self;
    videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    videoPicker.mediaTypes =[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
    videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:videoPicker animated:YES completion:nil];
    NSData *videoData = [NSData dataWithContentsOfFile:[self.videoURL path]];
    _lbl2.text = [NSString stringWithFormat:@"%.2f MB", (float)videoData.length/1024.0f/1024.0f];
    
}

-(void)takeVideo {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        
        [self presentViewController:picker animated:YES completion:NULL];
        NSData *videoData = [NSData dataWithContentsOfFile:[self.videoURL path]];
        _lbl2.text = [NSString stringWithFormat:@"%.2f MB", (float)videoData.length/1024.0f/1024.0f];
    }
    //}
    
}

-(IBAction)playvideo:(id)sender {
    
    if(self.videoURL) {
        self.videoController = [[MPMoviePlayerController alloc] init];
        [self.videoController setContentURL:self.videoURL];
        self.videoController.controlStyle = MPMovieControlStyleFullscreen;
        //self.videoController.view.transform = CGAffineTransformConcat(self.videoController.view.transform, CGAffineTransformMakeRotation(M_PI));
        [self.videoController.view setFrame: self.view.bounds];
        [self.view addSubview: self.videoController.view];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(doneButtonClick:)
                                                     name:MPMoviePlayerWillExitFullscreenNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(videoPlayBackDidFinish:)
                                                     name:MPMoviePlayerPlaybackDidFinishNotification
                                                   object:self.videoController];
        
        [self.videoController play];
    }
    
}

-(IBAction)borrarvideo:(id)sender {
    [_videoThumbView setImage:nil];
    [_videoView setHidden:YES];
}

- (void)videoPlayBackDidFinish:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    
    // Stop the video player and remove it from view
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    
}


- (void)doneButtonClick:(NSNotification *)notification {
    
    [[NSNotificationCenter defaultCenter]removeObserver:self name:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey object:nil];
    
    // Stop the video player and remove it from view
    [self.videoController stop];
    [self.videoController.view removeFromSuperview];
    self.videoController = nil;
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"gotoHistorial"]) {
        HistorialViewController *HistorialViewController = segue.destinationViewController;
        HistorialViewController.type_file_id = @"1";
        HistorialViewController.borrador = borradorBool;
    }

    if([segue.identifier isEqualToString:@"gotoAdd"]){
        BeneficiariosListViewController *controller = [segue destinationViewController];
        controller.delegado = self;
    }
}


#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.view setFrame:CGRectMake(0,190 - textField.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height)];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [textField resignFirstResponder];
    mytextField = nil;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    mytextField = nil;
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

#pragma mark NIDropDown


- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [self.view addGestureRecognizer:tap];
    _selectPlaceholder.text = @"";
    NSLog(@"%@", titulo);
    if(([titulo isEqualToString:@"Fecha"])||([titulo isEqualToString:@"Date"])){
        _fecha.hidden = NO;
    }else{
        _fecha.text = @"";
        _fecha.hidden = YES;
        
    }
}


- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    NSLog(@"%@", _btnSelect.titleLabel.text);
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

/*
 * Traigo mis Beneficiarios
 */
-(void)getBeneficiarios {
    //Muestro la vista de cargando
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?with=beneficiary&filter=%@", escBox]; //query para pasarle a la url
    
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"beneficiaryUser"] stringByAppendingString:query];
    
    //NSString *urlString = @"http://fluie.com/fluie/api/beneficiaryUser";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];//Todos los beneficiarios de la Base de Datos
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSMutableArray *datasourcetemporal = [NSMutableArray arrayWithObjects:nil];
            if(arrayresponse.count){
                beneficiarios = arrayresponse;
                NSMutableArray *receptoresTemporal = [_borradorDiccionario[@"messageBeneficiaries"] mutableCopy];
                for(int i = 0; i<[receptoresTemporal count]; i++){
                    NSDictionary *receptDictionaryTemporal = [receptoresTemporal objectAtIndex:i];
                    
                    for (int j=0; j<[beneficiarios count]; j++) {
                        NSDictionary *beneficiario = [beneficiarios objectAtIndex:j];
                        NSLog(@"%@ %@", beneficiario[@"beneficiary_id"], receptDictionaryTemporal[@"beneficiary_id"]);
                        if([beneficiario[@"beneficiary_id"] isEqualToString:receptDictionaryTemporal[@"beneficiary_id"]]){
                            [receptores addObject:beneficiario];
                            break;
                        }
                    }
                }
                //receptores
                for (UIView *aView in [_receptoresScroll subviews]){
                    _agregar.text = @"Agregar beneficiario";
                    if([language rangeOfString:@"es"].location == NSNotFound)
                        _agregar.text = @"Add beneficiary";
                    if ([aView isKindOfClass:[UIButton class]]){
                        [aView removeFromSuperview];
                    }
                    if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                        [aView removeFromSuperview];
                    }
                }
                
                UIImageView *_miimageView = [[UIImageView alloc] init];
                
                for(int i = 0; i<[receptores count]; i++){
                    _agregar.text = @"";
                    NSMutableDictionary *receptDictionary = [[receptores objectAtIndex:i] mutableCopy];
                    NSLog(@"%@", receptDictionary);
                   
                    /*
                     * Armo la foto del beneficiario
                     */
                    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", receptDictionary[@"beneficiary_id"]];
                    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                    
                    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
                    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                    [request setHTTPMethod: @"POST"];
                    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                    [request setHTTPBody: myRequestData];
                    
                    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                    NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    NSLog(@"%@", foto[@"image"]);
                    receptDictionary[@"beneficiary"] = [receptDictionary[@"beneficiary"] mutableCopy];
                    [receptDictionary[@"beneficiary"] setObject:foto[@"image"] forKey:@"image"];
                    /*
                     * Fin foto del beneficiario
                     */
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    [button setTag:i];
                    [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
                    button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
                    button.clipsToBounds = YES;
                    
                    button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
                    button.layer.borderColor=[UIColor grayColor].CGColor;
                    button.layer.borderWidth=0.5f;
                    
                    if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:foto[@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
                    }else{
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:foto[@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
                    }
                    
                    [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
                    
                    UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
                    if(receptDictionary[@"beneficiary"][@"name"])
                        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
                    [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
                    [_receptoresScroll addSubview:button];
                    [_receptoresScroll addSubview:nombreReceptor];
                    [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
                }
            }
        }else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
    
}



@end
