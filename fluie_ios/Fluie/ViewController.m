//
//  ViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/20/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "ViewController.h"
#import "AlmosaferAlertView.h"


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Ingreso"
                                 contentText:@"sfsfsf"
                                 leftButtonTitle:@"Aceptar"
                                 rightButtonTitle:nil];
    [alert showInView:self.view];
    
    alert.leftBlock = ^() {
        NSLog(@"left button clicked");
    };
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end