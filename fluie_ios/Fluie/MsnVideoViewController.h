//
//  MsnVideoViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/11/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "BeneficiariosListViewController.h"
#import "NIDropDown.h"

@interface MsnVideoViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, BeneficiariosListViewControllerDelegate, NIDropDownDelegate,UIActionSheetDelegate>{
    NIDropDown *dropDownPregrado;
}

@property (strong, nonatomic) NSDictionary *borradorDiccionario;
@property (assign, nonatomic) BOOL *esBorrador;

@end
