//
//  BeneficiarioFormViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "BeneficiarioFormViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"


@interface BeneficiarioFormViewController () {
    bool masculino;
    UITextField *mytextField;
    NSArray *contentsParentezcoArray;
    NSArray *usuariosArray;
    NSDictionary *recienCreado;
    NSString *parentezcoSelected;
    bool match;
    int beneficiarioId;
    NSMutableArray *borrador;
    NSString * gender;
    UITapGestureRecognizer *tap;
}

@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UIButton *parentezco_Button;
@property (weak, nonatomic) IBOutlet UITextField *nombresTxt;
@property (weak, nonatomic) IBOutlet UITextField *apellidosTxt;
@property (weak, nonatomic) IBOutlet UITextField *correoTxt;
@property (weak, nonatomic) IBOutlet UITextField *parentescoTxt;
@property (weak, nonatomic) IBOutlet UIButton *btnMasculino;
@property (weak, nonatomic) IBOutlet UIButton *btnFemenino;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *reloj;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UITextField *code;
@property (weak, nonatomic) IBOutlet UITextField *telefonoTxt;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UIButton *btn3;
@property (weak, nonatomic) IBOutlet UIButton *btn2;

@property (weak, nonatomic) IBOutlet UILabel *lbl3;
@property (weak, nonatomic) IBOutlet UILabel *lbl4;

@end

@implementation BeneficiarioFormViewController


-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        _lbl1.text = @"Fill the beneficiary information";
        _nombresTxt.placeholder = @"Name";
        _apellidosTxt.placeholder = @"Last Name";
        _correoTxt.placeholder = @"Email";
        if (_parentezco_Button.titleLabel.text.length == 0)
        {
            _parentescoTxt.placeholder = @"Relation";
        }
        _telefonoTxt.placeholder =@"Phone";
        _lbl2.text=@"Gender";
        _lbl3.text=@"Male";
        _lbl4.text=@"Female";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        [_btn2 setTitle:@"DELETE" forState:UIControlStateNormal];
        [_btn3 setTitle:@"SAVE" forState:UIControlStateNormal];
        
    }
}
- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}


- (void)viewDidLoad
{
    
    [super viewDidLoad];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
   
    [_reloj setHidden:YES];
    [_reloj stopAnimating];
    masculino = TRUE;
        gender = @"1";
    match = FALSE;
    // Do any additional setup after loading the view.
    [self cargaParentezco];
    if(_beneficiario!=nil){
        _btn1.hidden = NO;
        _btn2.hidden = NO;
        _btn3.hidden = YES;
        NSLog(@"%@", _beneficiario);
        _parentescoTxt.placeholder = @"";
        beneficiarioId = [_beneficiario[@"user_id"] intValue];
        _nombresTxt.text = _beneficiario[@"beneficiary"][@"name"];
        _apellidosTxt.text = _beneficiario[@"beneficiary"][@"name_last"];
        _correoTxt.text = _beneficiario[@"beneficiary"][@"email"];
        _telefonoTxt.text = _beneficiario[@"beneficiary"][@"phone"];
        _code.text = _beneficiario[@"code"];
        
        if([_beneficiario[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
            masculino = TRUE;
            UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
            if(imageToSet)
            {
                [_btnMasculino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
                
            } else {
                // if you see this line in your console, then you know where to look for a problem
                NSLog( @"why is my image object nil?");
            }
            [_btnFemenino setImage:nil forState:UIControlStateNormal];
        }else{
            masculino = FALSE;
            UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
            if(imageToSet)
            {
                [_btnFemenino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
            } else {
                // if you see this line in your console, then you know where to look for a problem
                NSLog( @"why is my image object nil?");
            }
            [_btnMasculino setImage:nil forState:UIControlStateNormal];
        }
        
        
    }
    else
    {
        _btn1.hidden = YES;
        _btn2.hidden = YES;
        _btn3.hidden = NO;
        _parentescoTxt.placeholder = @"Parentezco";
        beneficiarioId = nil;
        _beneficiario = nil;
        _nombresTxt.text = @"";
        _apellidosTxt.text = @"";
        _correoTxt.text = @"";
        _telefonoTxt.text = @"";
        _code.text = @"";
    }
    
    dropDownPregrado.delegate = self;
    
    
}


-(void) cargaParentezco {
    NSString *urlString = @"http://fluie.com/fluie/api/relationship";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        contentsParentezcoArray=arrayresponse;
    }
    
    if(_beneficiario!=nil){
        
        for (int i=0; i<contentsParentezcoArray.count; i++) {
            NSDictionary *diccionario = [contentsParentezcoArray objectAtIndex:i];
            NSLog(@"%@", _beneficiario[@"relationship_id"]);
            if([diccionario[@"idrelationship"] isEqualToString:_beneficiario[@"relationship_id"]]){
                [_parentezco_Button setTitle:diccionario[@"relationship"] forState:UIControlStateNormal];
                _parentezco_Button.titleLabel.text = diccionario[@"relationship"];
            }
        }
    }
    
    //[self cargarUsuarios];
}

- (IBAction)onPais:(id)sender
{
    [self.view removeGestureRecognizer:tap];

    [mytextField resignFirstResponder];
    
    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsParentezcoArray valueForKey:@"relationship"];
    if(dropDownPregrado == nil) {
        CGFloat f = 100;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}


-(void)dismissKeyboard {
    [mytextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)borrar:(id)sender{
    [self dismissKeyboard];
    
    AlmosaferAlertView *alert;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
         alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Confirm"
                                     contentText:@"Beneficiary want to delete?"
                                     leftButtonTitle:@"Yes"
                                     rightButtonTitle:@"No"];
    }
    else
    {
         alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Confirmar"
                                     contentText:@"Desea borrar al Beneficiario?"
                                     leftButtonTitle:@"Si"
                                     rightButtonTitle:@"No"];
    }
    [alert showInView:self.view];
    alert.leftBlock = ^() {
    
    if(_beneficiario!=nil){
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
        }
        
        NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/user/DeleteBeneficiary"];
        //beneficiarioId
        
        NSString *myRequestString;
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
             myRequestString = [NSString stringWithFormat:@"user_id=%@&beneficiary_id=%@&language=2", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _beneficiario[@"beneficiary_id"]];
        }
        else
        {
            myRequestString = [NSString stringWithFormat:@"user_id=%@&beneficiary_id=%@&language=1", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"], _beneficiario[@"beneficiary_id"]];
        }
        NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
        [request setHTTPMethod: @"POST"];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [request setHTTPBody: myRequestData];
        
        //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
            _beneficiario = nil;
            if (error == nil) {
                NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                AlmosaferAlertView *alert;
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                     alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Beneficiary"
                                                 contentText:arrayresponse[@"msg"]
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                }
                else
                {
                     alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Beneficiario"
                                                 contentText:arrayresponse[@"msg"]
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    
                }
                [alert showInView:self.view];
                alert.leftBlock = ^() {
                    NSLog(@"left button clicked");
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [self hideProgressHUD];
                };
            }
            else
            {
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notification"
                                                 contentText:@"Could not establish communication. Check your connection"
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notificacion"
                                                 contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                [self hideProgressHUD];
            }
        }];
        [dataTask resume];
        
        
    }else{
        _nombresTxt.text = @"";
        _correoTxt.text = @"";
        _apellidosTxt.text = @"";
        _parentezco_Button.titleLabel.text = @"";
        _parentescoTxt.placeholder = @"Parentezco";

    }
    
    };
}

-(void)viewWillDisappear:(BOOL)animated {
    _nombresTxt.text = @"";
    _correoTxt.text = @"";
    _apellidosTxt.text = @"";
    _parentezco_Button.titleLabel.text = @"";
    _parentescoTxt.placeholder = @"Parentezco";
}

-(IBAction)guardar:(id)sender{
    
    [self dismissKeyboard];
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
        
    match = TRUE;
    if(_beneficiario!=nil){
        if([_beneficiario[@"beneficiary"][@"active"] isEqualToString:@"1"]){
             [self hideProgressHUD];
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"You can not make changes on an active recipient"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No puede realizar cambios sobre un Beneficiario activo"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
            
            
            
            
        }else{
            [self saveaction];
        }
    }else
        [self saveaction];

    
}

-(void)saveaction {
    if(![_parentezco_Button.titleLabel.text isEqualToString:@""]){
        for (int i=0; i<contentsParentezcoArray.count; i++) {
            NSDictionary *diccionario = [contentsParentezcoArray objectAtIndex:i];
            
            if([diccionario[@"relationship"] isEqualToString:[_parentezco_Button.titleLabel.text stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                parentezcoSelected = diccionario[@"idrelationship"];
        }
        
    }
    NSString *urlString = @"http://fluie.com/fluie/user/SaveBeneficiary";
    if(_beneficiario!=nil)
        urlString = @"http://fluie.com/fluie/user/EditBeneficiary";
    
    
    NSString *myRequestString;
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        myRequestString = [NSString stringWithFormat:@"user_id=%@&b[name]=%@&b[name_last]=%@&b[relationship_id]=%@&b[email]=%@&b[gender_id]=%@&b[language]=2", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"],_nombresTxt.text,_apellidosTxt.text,parentezcoSelected,_correoTxt.text,gender];
    }
    else
    {
          myRequestString = [NSString stringWithFormat:@"user_id=%@&b[name]=%@&b[name_last]=%@&b[relationship_id]=%@&b[email]=%@&b[gender_id]=%@&b[language]=1", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"],  _nombresTxt.text, _apellidosTxt.text, parentezcoSelected, _correoTxt.text, gender];
    }
    
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        if(_beneficiario!=nil)
        myRequestString = [NSString stringWithFormat:@"b[iduser]=%@&b[idbeneficiaryuser]=%@&b[name]=%@&b[name_last]=%@&b[relationship_id]=%@&b[email]=%@&b[gender_id]=%@b[language]=2", _beneficiario[@"beneficiary_id"], _beneficiario[@"idbeneficiaryuser"], _nombresTxt.text, _apellidosTxt.text, parentezcoSelected, _correoTxt.text, gender];
    }
    else
    {
        if(_beneficiario!=nil)
            
            myRequestString = [NSString stringWithFormat:@"b[iduser]=%@&b[idbeneficiaryuser]=%@&b[name]=%@&b[name_last]=%@&b[relationship_id]=%@&b[email]=%@&b[gender_id]=%@&b[language]=1", _beneficiario[@"beneficiary_id"], _beneficiario[@"idbeneficiaryuser"], _nombresTxt.text, _apellidosTxt.text, parentezcoSelected, _correoTxt.text, gender];
    }
   
    
    NSLog(@"%@", myRequestString);
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            if(returnData){
                NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
                NSLog(@"response %@",responsestring);
                NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                
                if(!responseDic[@"error"]){
                    if (![responseDic[@"result"] isEqualToString:@"error"]) {
                        _beneficiario = nil;
                        _nombresTxt.text = @"";
                        _correoTxt.text = @"";
                        _apellidosTxt.text = @"";
                        _code.text = @"";
                        _parentezco_Button.titleLabel.text = @"";
                        _parentescoTxt.placeholder = @"Parentezco";
                    }
                    
                    
                    [_reloj setHidden:YES];
                    [_reloj stopAnimating];
                    [self hideProgressHUD];
                    
                    AlmosaferAlertView *alert;
                    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                    if([language rangeOfString:@"es"].location == NSNotFound)
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Beneficiary"
                                 contentText:responseDic[@"msg"]
                                 leftButtonTitle:@"Accept"
                                 rightButtonTitle:nil];
                    }
                    else
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Beneficiario"
                                 contentText:responseDic[@"msg"]
                                 leftButtonTitle:@"Aceptar"
                                 rightButtonTitle:nil];
                    }
                    [alert showInView:self.view];

                    
                    alert.leftBlock = ^() {
                        NSLog(@"left button clicked");
                        if(![responseDic[@"result"] isEqualToString:@"error"])
                            [self dismissViewControllerAnimated:YES completion:nil];
                        [self hideProgressHUD];
                    };
                    
                    
                    
                }else if(responseDic[@"error"]){
                    id val = nil;
                    NSArray *values = [responseDic[@"error"] allValues];
                    if ([values count] != 0)
                        val = [values objectAtIndex:0];
                    
                    NSLog(@"%@", val);
                    NSString *respuesta=@"";
                    if([val isKindOfClass:[NSArray class]]){
                        respuesta = [val objectAtIndex:0];
                    }else if([val isKindOfClass:[NSString class]]){
                        respuesta = val;
                    }else{
                        respuesta = val;
                    }
                    NSLog(@"%@", respuesta);
                    [self hideProgressHUD];
                    
                    AlmosaferAlertView *alert;
                    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                    if([language rangeOfString:@"es"].location == NSNotFound)
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Beneficiary"
                                 contentText:[NSString stringWithFormat:@"%@", respuesta]
                                 leftButtonTitle:@"Accept"
                                 rightButtonTitle:nil];
                    }
                    else
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Beneficiario"
                                 contentText:[NSString stringWithFormat:@"%@", respuesta]
                                 leftButtonTitle:@"Aceptar"
                                 rightButtonTitle:nil];
                    }
                    [alert showInView:self.view];

                    alert.leftBlock = ^() {
                        NSLog(@"left button clicked");
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [self hideProgressHUD];
                    };
                    
                }
            }
        }else
        {
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
}

-(IBAction)onMasculino:(id)sender{
    NSLog(@"masculino");
        gender = @"1";
    masculino = TRUE;
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnMasculino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
        
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnFemenino setImage:nil forState:UIControlStateNormal];

}

-(IBAction)onFemenino:(id)sender{
    NSLog(@"femenino");
    gender = @"2";
    masculino = FALSE;
    UIImage * imageToSet = [UIImage imageNamed: @"chulo.png"];
    if(imageToSet)
    {
        [_btnFemenino setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        // if you see this line in your console, then you know where to look for a problem
        NSLog( @"why is my image object nil?");
    }
    [_btnMasculino setImage:nil forState:UIControlStateNormal];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    /* if (textField == _Username_TextField) {
     [_Username_TextField resignFirstResponder];
     [_password_TextField becomeFirstResponder];
     } else if (textField == _password_TextField) {*/
    // here you can define what happens
    // when user presses return on the email field
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [textField resignFirstResponder];
    // }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    mytextField = textField;
     [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 509+253)];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark NIDropDown

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [self.view addGestureRecognizer:tap];
    [_parentescoTxt setPlaceholder:@""];
    
    parentezcoSelected = titulo;
    if(sender == _parentezco_Button)
        [[NSUserDefaults standardUserDefaults] setObject:titulo forKey:@"parentezco_Button"];
    
    //[self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDownPregrado = nil;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
