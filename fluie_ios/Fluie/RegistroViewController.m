//
//  RegistroViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 11/24/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "RegistroViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"

@interface RegistroViewController (){
    __weak IBOutlet UIActivityIndicatorView *reloj;
    NSArray *contentsPaisArray;
    NSString *paisSelected;
    NSArray *contentsTipoArray;
    NSString *tipoSelected;
    UITextField *mytextField;
    BOOL isPhotoSelected;
    BOOL aceptoTerminos;
    NSData *image;
    NSString *language_id;
    UITapGestureRecognizer *tap;
    
}
@property (weak, nonatomic) IBOutlet UILabel *aceptoLbl;
@property (weak, nonatomic) IBOutlet UILabel *terminosLbl;
@property (weak, nonatomic) IBOutlet UIButton *aceptoBtn;
@property (weak, nonatomic) IBOutlet UIButton *registroBtn;
@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (weak, nonatomic) IBOutlet UITextField *nombre;
@property (weak, nonatomic) IBOutlet UITextField *apellido;
@property (weak, nonatomic) IBOutlet UITextField *telefono;
@property (weak, nonatomic) IBOutlet UITextField *movil;
@property (weak, nonatomic) IBOutlet UITextField *pais;
@property (weak, nonatomic) IBOutlet UITextField *documento;
@property (weak, nonatomic) IBOutlet UITextField *tipoddocumento;
@property (weak, nonatomic) IBOutlet UITextField *direccion;
@property (weak, nonatomic) IBOutlet UITextField *mail;
@property (weak, nonatomic) IBOutlet UITextField *mail2;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *password2;
@property (weak, nonatomic) IBOutlet UIButton *pais_Button;
@property (weak, nonatomic) IBOutlet UIButton *tipo_Button;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIButton *cancel;


@end

@implementation RegistroViewController

-(void)viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    language_id = @"1";
    if([language rangeOfString:@"es"].location == NSNotFound) {
        language_id = @"2";
        _nombre.placeholder = @"Name";
        _apellido.placeholder = @"Lastname";
        _telefono.placeholder = @"Phone";
        _movil.placeholder = @"Mobile";
        
        _documento.placeholder =@"Document";
        //[_pais_Button setTitle:@"Country" forState:UIControlStateNormal];
        _direccion.placeholder = @"Address";
        _mail.placeholder = @"Email";
        _mail2.placeholder = @"Optional Email";
        _aceptoLbl.text = @"I accept ours";
        _terminosLbl.text = @"Terms & Conditions";
        _password.placeholder =@"Password";
        _password2.placeholder =@"Confirm Password";
        [_registroBtn setTitle:@"REGISTER" forState:UIControlStateNormal];
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        
        if (self.pais_Button.currentTitle.length == 0)
        {
             _pais.placeholder = @"Country";
        }
        if (self.tipo_Button.currentTitle.length == 0)
        {
            _tipoddocumento.placeholder = @"Document type";
        }
        
    }
}

-(IBAction)onMasculino:(id)sender{
    NSLog(@"masculino");
    
    if(!aceptoTerminos)
    {
        [_aceptoBtn setImage:[UIImage imageNamed:@"chulo.png"] forState:UIControlStateNormal];
    } else {
        [_aceptoBtn setImage:nil forState:UIControlStateNormal];
    }
    aceptoTerminos = !aceptoTerminos;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [reloj setHidden:YES];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    //_miimageView.layer.cornerRadius = 100;
    self.imgPhoto.layer.masksToBounds = YES;
    // border radius
    [self.imgPhoto.layer setCornerRadius:45.0f];
    // border
    [self.imgPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.imgPhoto.layer setBorderWidth:2.5f];
    // drop shadow
    [self.imgPhoto.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.imgPhoto.layer setShadowOpacity:0.8];
    [self.imgPhoto.layer setShadowRadius:3.0];
    [self.imgPhoto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    [self.imgPhoto setImage:[UIImage imageNamed:@"avatar.png"]];

    /**/
    [self cargaPaises];
    dropDownPregrado.delegate = self;
    
    [self.myscroll setScrollEnabled:YES];
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 749)];
    
    aceptoTerminos = false;
    
    // Do any additional setup after loading the view.
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void) cargaPaises {
    NSString *urlString = @"http://fluie.com/fluie/api/country/";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if(arrayresponse.count){
        contentsPaisArray=arrayresponse;
    }
    
    [self cargaTipo];
}

-(void) cargaTipo {
        /*
         * Hago el request de las notificaciones
         */
    
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    language_id = @"1";
    if([language rangeOfString:@"es"].location == NSNotFound) {
        language_id = @"2";
    }
        
        NSString *myRequestString = [NSString stringWithFormat:@"language_id=%@", language_id];
        NSLog(@"requeststring: %@", myRequestString);
        NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
        
        NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/typeIdentification/getList"];
        NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
        [requestFS setHTTPMethod: @"POST"];
        [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
        [requestFS setHTTPBody: myRequestData];
        
        //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
            if (error == nil) {
                
                NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                NSLog(@"%@", arrayresponse);
    
                if(arrayresponse.count){
                    contentsTipoArray=arrayresponse;
                }
            }else{
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notification"
                                                 contentText:@"Could not establish communication. Check your connection"
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notificacion"
                                                 contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
            }
        }];
        [dataTask resume];
}

-(IBAction)onregister:(id)sender
{
    
    if ((_nombre.text.length ==0)||(_apellido.text.length ==0)||(_movil.text.length ==0)||(self.pais_Button.currentTitle.length == 0)||(_documento.text.length ==0)||(self.tipo_Button.currentTitle.length == 0)||(_direccion.text.length ==0)||(_mail.text.length ==0)||(_password.text.length ==0)||(_password2.text.length ==0))
    {
        AlmosaferAlertView *alert;
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Registry"
                     contentText:@"Please fill all the above details first"
                     leftButtonTitle:@"Accept"
                     rightButtonTitle:nil];
            
        }
        else
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Registro"
                     contentText:@"Por favor, rellene todos los datos mencionados, primera"
                     leftButtonTitle:@"Aceptar"
                     rightButtonTitle:nil];
        }
        [alert showInView:self.view];
    }
    else if (![self EmailValidationL:_mail.text])
    {
        
        AlmosaferAlertView *alert;
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Registry"
                     contentText:@"Email field is not valid"
                     leftButtonTitle:@"Accept"
                     rightButtonTitle:nil];
            
        }
        else
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Registro"
                     contentText:@"Campo de correo electrónico no es válida"
                     leftButtonTitle:@"Aceptar"
                     rightButtonTitle:nil];
        }
        [alert showInView:self.view];
    }
    else
    {
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
        }
        
        
        if(![_pais_Button.titleLabel.text isEqualToString:@""]){
            for (int i=0; i<contentsPaisArray.count; i++) {
                NSDictionary *diccionario = [contentsPaisArray objectAtIndex:i];
                NSLog(@"%@", diccionario);
                NSLog(@"title label %@", paisSelected);
                
                if([diccionario[@"country"] isEqualToString:[paisSelected stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                    paisSelected = diccionario[@"idcountry"];
            }
            
        }
        
        if(![_tipo_Button.titleLabel.text isEqualToString:@""]){
            for (int i=0; i<contentsTipoArray.count; i++) {
                NSDictionary *diccionario = [contentsTipoArray objectAtIndex:i];
                NSLog(@"%@", diccionario);
                NSLog(@"title label %@", tipoSelected);
                
                if([diccionario[@"name"] isEqualToString:[tipoSelected stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceCharacterSet]]])
                    paisSelected = diccionario[@"idtype_identification"];
            }
            
        }
        
        tipoSelected = @"1";
        
        NSLog(@"%@", paisSelected);
        NSString *urlString = @"http://fluie.com/fluie/user/saveUser";
        
        // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:@"0" forKey:@"space"];
        [_params setObject:[NSString stringWithFormat:@"%i", [paisSelected intValue]] forKey:@"country_id"];
        [_params setObject:[NSString stringWithFormat:@"%@", _telefono.text] forKey:@"phone"];
        [_params setObject:[NSString stringWithFormat:@"%@", _password.text] forKey:@"pass"];
        [_params setObject:[NSString stringWithFormat:@"%@", _password2.text] forKey:@"confirm_password"];
        [_params setObject:[NSString stringWithFormat:@"%@", _documento.text] forKey:@"identification"];
        [_params setObject:[NSString stringWithFormat:@"%i", [tipoSelected intValue]] forKey:@"type_identification_id"];
        [_params setObject:[NSString stringWithFormat:@"1"] forKey:@"terms"];
        [_params setObject:[NSString stringWithFormat:@"%@", _mail.text] forKey:@"email"];
        [_params setObject:[NSString stringWithFormat:@"%@", _mail2.text] forKey:@"email_opcional"];
        [_params setObject:[NSString stringWithFormat:@"%@", _nombre.text] forKey:@"nombre"];
        [_params setObject:[NSString stringWithFormat:@"%@", _nombre.text] forKey:@"name"];
        [_params setObject:[NSString stringWithFormat:@"%@", _apellido.text] forKey:@"name_last"];
        [_params setObject:[NSString stringWithFormat:@"%@", _direccion.text] forKey:@"address"];
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        NSString* FileParamConstant = @"image";
        
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:urlString];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        //[request setTimeoutInterval:30];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            NSLog(@"%@", param);
            NSLog(@"%@", [_params objectForKey:param]);
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        NSData *imageData = UIImageJPEGRepresentation(self.imgPhoto.image, 1.0);
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        // set URL
        
        if(aceptoTerminos){
            [request setURL:requestURL];
            
            NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
            NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
            
            NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
                if (error == nil) {
                    NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                    NSLog(@"response %@",returnString);
                    NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    
                    if(!arrayresponse[@"error"]){
                        [reloj setHidden:YES];
                        [reloj stopAnimating];
                        [[NSUserDefaults standardUserDefaults] setObject:arrayresponse[@"iduser"] forKey:@"idUser"];
                        [self subirfoto];
                        
                        
                        //[self performSegueWithIdentifier:@"logged" sender:nil];
                    }else if(arrayresponse[@"error"]){
                        id val = nil;
                        NSArray *values = [arrayresponse[@"error"] allValues];
                        if ([values count] != 0)
                            val = [values objectAtIndex:0];
                        
                        NSLog(@"%@", val);
                        NSString *respuesta=@"";
                        if([val isKindOfClass:[NSArray class]]){
                            respuesta = [val objectAtIndex:0];
                        }else if([val isKindOfClass:[NSString class]]){
                            respuesta = val;
                        }else{
                            respuesta = val;
                        }
                        NSLog(@"%@", respuesta);
                        [self hideProgressHUD];
                        AlmosaferAlertView *alert;
                        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                        if([language rangeOfString:@"es"].location == NSNotFound)
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Registry"
                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                     leftButtonTitle:@"Accept"
                                     rightButtonTitle:nil];
                        }
                        else
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Registro"
                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
                        }
                        [alert showInView:self.view];

                        
                        alert.leftBlock = ^() {
                            NSLog(@"left button clicked");
                            
                        };
                        
                    }
                }else{
                    [self hideProgressHUD];
                }
            }];
            [dataTask resume];
            
        }
        else
        {
            [self hideProgressHUD];
            AlmosaferAlertView *alert;
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Registry"
                         contentText:[NSString stringWithFormat:@"You must accept the Terms and Conditions"]
                         leftButtonTitle:@"Accept"
                         rightButtonTitle:nil];
            }
            else
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Registro"
                         contentText:[NSString stringWithFormat:@"Debe aceptar los Términos y Condiciones"]
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
            }
            
            [alert showInView:self.view];

            alert.leftBlock = ^() {
                NSLog(@"left button clicked");
                
            };
        }
    }
    
}

-(void)subirfoto {
    if(!isPhotoSelected){
        
            [self doLogin];
        
    }else{
        NSString *urlString = @"http://fluie.com/fluie/user/SavePhotoUser";
        NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
        [_params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"] forKey:@"user_id"];
        
        // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        NSString* FileParamConstant = @"image";
        
        // the server url to which the image (or the media) is uploaded. Use your server url here
        NSURL* requestURL = [NSURL URLWithString:urlString];
        
        // create request
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in _params) {
            NSLog(@"%@", param);
            NSLog(@"%@", [_params objectForKey:param]);
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        // add image data
        NSData *imageData = UIImageJPEGRepresentation(self.imgPhoto.image, 1.0);
        if (imageData) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:imageData];
            [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        
        // setting the body of the post to the reqeust
        [request setHTTPBody:body];
        
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        [request setURL:requestURL];
        
        NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
        NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
        
        NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
            if (error == nil) {
                NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                NSLog(@"response %@",returnString);
                NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                
                if(!arrayresponse[@"error"]){
                    [reloj setHidden:YES];
                    [reloj stopAnimating];
                    [self hideProgressHUD];
                    AlmosaferAlertView *alert;
                    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                    if([language rangeOfString:@"es"].location == NSNotFound)
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Registry"
                                 contentText:@"You are successfully registered"
                                 leftButtonTitle:@"Accept"
                                 rightButtonTitle:nil];
                    }
                    else
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Registro"
                                 contentText:@"Ud se ha registrado con éxito"
                                 leftButtonTitle:@"Aceptar"
                                 rightButtonTitle:nil];
                    }
                    [alert showInView:self.view];

                    alert.leftBlock = ^() {
                        NSLog(@"left button clicked");
                        
                        [self doLogin];
                        
                    };
                    
                    //[self performSegueWithIdentifier:@"logged" sender:nil];
                }else if(arrayresponse[@"error"]){
                    id val = nil;
                    NSArray *values = [arrayresponse[@"error"] allValues];
                    if ([values count] != 0)
                        val = [values objectAtIndex:0];
                    
                    NSLog(@"%@", val);
                    NSString *respuesta=@"";
                    if([val isKindOfClass:[NSArray class]]){
                        respuesta = [val objectAtIndex:0];
                    }else if([val isKindOfClass:[NSString class]]){
                        respuesta = val;
                    }else{
                        respuesta = val;
                    }
                    NSLog(@"%@", respuesta);
                    [self hideProgressHUD];
                    AlmosaferAlertView *alert;
                    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                    if([language rangeOfString:@"es"].location == NSNotFound)
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Registry"
                                 contentText:[NSString stringWithFormat:@"%@", respuesta]
                                 leftButtonTitle:@"Accept"
                                 rightButtonTitle:nil];
                    }
                    else
                    {
                        alert = [[AlmosaferAlertView alloc]
                                 initWithTitle:@"Registro"
                                 contentText:[NSString stringWithFormat:@"%@", respuesta]
                                 leftButtonTitle:@"Aceptar"
                                 rightButtonTitle:nil];
                    }
                    [alert showInView:self.view];

                    
                    alert.leftBlock = ^() {
                        NSLog(@"left button clicked");
                        
                    };
                    
                }
            }else{
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notification"
                                                 contentText:@"Could not establish communication. Check your connection"
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notificacion"
                                                 contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
            }
        }];
        [dataTask resume];
    }
    
}

-(void)doLogin
{
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.dimBackground = YES;
    }
    NSString *urlString = @"http://fluie.com/fluie/api/user/login";
    NSString *myRequestString = [NSString stringWithFormat:@"email=%@&password=%@",_mail.text, _password.text];
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if([arrayresponse[@"status"] isEqualToString:@"err"]){
        [self hideProgressHUD];
        id val = nil;
        NSArray *values = [arrayresponse[@"message"] allValues];
        if ([values count] != 0)
            val = [values objectAtIndex:0];
        
        NSString *respuesta=@"";
        if([val isKindOfClass:[NSArray class]]){
            respuesta = [val objectAtIndex:0];
        }else if([val isKindOfClass:[NSString class]]){
            respuesta = val;
        }else{
            respuesta = val;
        }
        
        AlmosaferAlertView *alert;
        NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Entry"
                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                     leftButtonTitle:@"Accept"
                     rightButtonTitle:nil];
        }
        else
        {
            alert = [[AlmosaferAlertView alloc]
                     initWithTitle:@"Ingreso"
                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                     leftButtonTitle:@"Aceptar"
                     rightButtonTitle:nil];
        }
        [alert showInView:self.view];

        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
        };
        
    }else{
        
        [[NSUserDefaults standardUserDefaults] setObject:arrayresponse[@"iduser"] forKey:@"idUser"];
        if(arrayresponse[@"image"]!= (id)[NSNull null]){
            NSURL *imageURL = [NSURL URLWithString:arrayresponse[@"image"]];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            [[NSUserDefaults standardUserDefaults] setObject:imageData forKey:@"imageUser"];
        }
        
        
        [self hideProgressHUD];
        
        [self dismissViewControllerAnimated:YES completion:nil];
        
    }
}

- (IBAction)onPais:(id)sender
{
    [self.view removeGestureRecognizer:tap];
    [mytextField resignFirstResponder];

    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsPaisArray valueForKey:@"country"];
    if(dropDownPregrado == nil) {
        CGFloat f = 100;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}


- (IBAction)onTipo:(id)sender
{
    [self.view removeGestureRecognizer:tap];
    [mytextField resignFirstResponder];
    
    NSMutableArray *IndustriaArray = [[NSMutableArray alloc]init];
    IndustriaArray = [contentsTipoArray valueForKey:@"name"];
    if(dropDownPregrado == nil) {
        CGFloat f = 100;
        dropDownPregrado = [[NIDropDown alloc]showDropDown:sender :&f :IndustriaArray :nil :@"down"];
        dropDownPregrado.delegate = self;
    }
    else {
        [dropDownPregrado hideDropDown:sender];
        [self rel];
    }
}

#pragma mark Photo

- (IBAction)onBtnTakePhoto:(id)sender {
    [self dismissKeyboard];
    UIActionSheet *actionSheetView = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:@"Tomar foto" otherButtonTitles:@"Tomar foto existente", nil];
    
    [actionSheetView showInView:[self view]];
}

-(void)selectPhoto {
   
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        [pickerController setAllowsEditing:NO];
        [pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
        [self presentViewController:pickerController animated:YES completion:^{}];
    //}
    
}

-(void)takePhoto {
        UIImagePickerController *pickerController = [[UIImagePickerController alloc] init];
        pickerController.delegate = self;
        [pickerController setAllowsEditing:NO];
        [pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
        
        [self presentViewController:pickerController animated:YES completion:NULL];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
        UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
        isPhotoSelected = YES;
    
        UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
        // Crop the image to a square (yikes, fancy!)
        UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
    
        [self.imgPhoto setImage:croppedImage];
        
        NSData* imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
        NSString* imagePath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/saved_example_image.png"];
        [imageData writeToFile:imagePath atomically:YES];
        [picker dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }];
        
    //}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
        [picker dismissViewControllerAnimated:YES completion:^{
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        }];
    //}
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0: {
            [self takePhoto];
            break;
        }
        case 1: {
            [self selectPhoto];
            break;
        }
        default:
            break;
    }
}

#pragma mark NIDropDown

- (void) niDropDownDelegateMethod: (NIDropDown *) sender
{
    [self rel];
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [self.view addGestureRecognizer:tap];
    
    if(sender == _pais_Button){
        [[NSUserDefaults standardUserDefaults] setObject:titulo forKey:@"pais_Button"];
        _pais.placeholder = @"";
        paisSelected = titulo;
        
    }else{
        _tipoddocumento.placeholder = @"";
        tipoSelected = titulo;
    }
    
    //[self rel];
}

-(void)rel{
    //    [dropDown release];
    dropDownPregrado = nil;
}


-(void)dismissKeyboard {
    [mytextField resignFirstResponder];
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 749+153)];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   /* if (textField == _Username_TextField) {
        [_Username_TextField resignFirstResponder];
        [_password_TextField becomeFirstResponder];
    } else if (textField == _password_TextField) {*/
        // here you can define what happens
        // when user presses return on the email field
        [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 749)];
        [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
        
        [textField resignFirstResponder];
   // }
    return YES;
}
- (BOOL)EmailValidationL:(NSString *)email
{
    NSString *emailRegEx =@"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    BOOL a = [regExPredicate evaluateWithObject:email];
    return a;
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
}



@end
