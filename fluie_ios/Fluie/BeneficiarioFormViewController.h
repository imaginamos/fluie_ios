//
//  BeneficiarioFormViewController.h
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NIDropDown.h"

@interface BeneficiarioFormViewController : UIViewController<UITextInputDelegate,NIDropDownDelegate
>
{
    NIDropDown *dropDownPregrado;
}

@property (strong, nonatomic) NSDictionary *beneficiario;

@end
