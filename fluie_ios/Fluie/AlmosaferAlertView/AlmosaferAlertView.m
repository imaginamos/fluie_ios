//
//  AlmosaferAlertView.m
//  Almosafer
//
//  Created by Romanov Yuryevich on 9/5/14.
//  Copyright (c) 2014 Romanov. All rights reserved.
//

#import "AlmosaferAlertView.h"
#import <QuartzCore/QuartzCore.h>

#define kAlertWidth 270.0f
#define kAlertHeight 175.0f

#define kTitleYOffset 20.0f
#define kTitleHeight 25.0f
#define kPaddingX    16.0f
#define kPaddingY    15.0f
#define kContentOffset 30.0f
#define kBetweenLabelOffset 20.0f

@interface AlmosaferAlertView(){
    
    BOOL _leftLeave;
}

@property (nonatomic, strong) UILabel *alertTitleLabel;
@property (nonatomic, strong) UILabel *alertContentLabel;
@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;
@property (nonatomic, strong) UIView *backImageView;

@end

@implementation AlmosaferAlertView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (id)initWithTitle:(NSString *)title
        contentText:(NSString *)content
    leftButtonTitle:(NSString *)leftTitle
   rightButtonTitle:(NSString *)rigthTitle{

    if (self = [super init]) {
        
        self.layer.cornerRadius = 5.0;
        self.layer.masksToBounds = YES;
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = [UIColor colorWithRed:178.0f/255 green:178.0f/255 blue:178.0f/255 alpha:1].CGColor;
        self.layer.borderWidth = 0.5f;
        
        // Creates a title label
        
        self.alertTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, kTitleYOffset, kAlertWidth, kTitleHeight)];
        self.alertTitleLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
        self.alertTitleLabel.textColor = [UIColor blackColor];;
        self.alertTitleLabel.text = title;
        self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.alertTitleLabel];
        
        // Creates a content label
        
        CGFloat contentLabelWidth = kAlertWidth - kPaddingX;
        CGSize maxSize = CGSizeMake(contentLabelWidth, MAXFLOAT);
        CGSize expectedSize = [content boundingRectWithSize:maxSize options:(NSStringDrawingUsesLineFragmentOrigin) attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Helvetica" size:14]} context:nil].size;
        
       
        self.alertContentLabel = [[UILabel alloc] initWithFrame:CGRectMake((kAlertWidth - contentLabelWidth) * 0.5, CGRectGetMaxY(self.alertTitleLabel.frame)+kPaddingY, contentLabelWidth, expectedSize.height)];
        self.alertContentLabel.numberOfLines = expectedSize.height/14;
        self.alertContentLabel.textAlignment = self.alertTitleLabel.textAlignment = NSTextAlignmentCenter;
        self.alertContentLabel.textColor = [UIColor blackColor];
        self.alertContentLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0f];
        self.alertContentLabel.text = content;
        [self addSubview:self.alertContentLabel];
        
        #define kSingleButtonWidth 270.0f
        #define kCoupleButtonWidth 138.0f
        #define kButtonHeight 47.0f
        #define kButtonBottomOffset 0.0f

        CGRect leftBtnFrame;
        CGRect rightBtnFrame;
        
        if (!rigthTitle) {
            
            leftBtnFrame = CGRectMake((kAlertWidth - kSingleButtonWidth) * 0.5, kAlertHeight - kButtonBottomOffset - kButtonHeight, kSingleButtonWidth, kButtonHeight);
            if (self.alertContentLabel.numberOfLines > 4) {
                leftBtnFrame = CGRectMake((kAlertWidth - kSingleButtonWidth) * 0.5, kAlertHeight - kButtonBottomOffset - kButtonHeight+15, kSingleButtonWidth, kButtonHeight-10);
            }
            self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            self.leftBtn.frame = leftBtnFrame;
            self.leftBtn.layer.borderColor = [UIColor colorWithRed:178.0f/255 green:178.0f/255 blue:178.0f/255 alpha:1].CGColor;
            self.leftBtn.layer.borderWidth = 0.5f;
            
        }else {
            
            leftBtnFrame = CGRectMake((kAlertWidth - 2 * kCoupleButtonWidth - kButtonBottomOffset) * 0.5-1, kAlertHeight - kButtonBottomOffset - kButtonHeight+1, kCoupleButtonWidth, kButtonHeight);
            rightBtnFrame = CGRectMake(CGRectGetMaxX(leftBtnFrame) + kButtonBottomOffset-1, kAlertHeight - kButtonBottomOffset - kButtonHeight+1, kCoupleButtonWidth, kButtonHeight);
            self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
            self.leftBtn.frame = leftBtnFrame;
            self.rightBtn.frame = rightBtnFrame;
            self.rightBtn.layer.borderColor = self.leftBtn.layer.borderColor = [UIColor colorWithRed:178.0f/255 green:178.0f/255 blue:178.0f/255 alpha:1].CGColor;
            self.rightBtn.layer.borderWidth = self.leftBtn.layer.borderWidth = 0.5f;
        }
        
        // Resizing frame of buttons per content length
        
        [self.leftBtn addTarget:self action:@selector(leftBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [self.rightBtn addTarget:self action:@selector(rightBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.leftBtn];
        [self addSubview:self.rightBtn];
        
        _leftLeave = NO;
        
        [self.rightBtn setTitle:rigthTitle forState:UIControlStateNormal];
        [self.leftBtn setTitle:leftTitle forState:UIControlStateNormal];
        self.leftBtn.titleLabel.font = [UIFont fontWithName:@"Helveticat" size:17.0f];
        self.rightBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
        
        [self.leftBtn setBackgroundColor:[UIColor whiteColor]];
        [self.rightBtn setBackgroundColor:[UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0f]];
        
        [self.leftBtn setTitleColor:[UIColor colorWithRed:34.0/255 green:17.0/255 blue:101.0/255 alpha:1.0f] forState:UIControlStateNormal];
        [self.rightBtn setTitleColor:[UIColor colorWithRed:34.0/255 green:17.0/255 blue:101.0/255 alpha:1.0f] forState:UIControlStateNormal];
        
        // Background Color

        //self.backgroundColor = [Tools colorFromHexString:@"#02377f" withAlpha:1.0f];
        //self.leftBtn.backgroundColor = [Tools colorFromHexString:@"#497fc6" withAlpha:1.0f];
        self.leftBtn.titleLabel.textColor = [UIColor whiteColor];
        //self.rightBtn.backgroundColor = [Tools colorFromHexString:@"#497fc6" withAlpha:1.0f];
        self.rightBtn.titleLabel.textColor = [UIColor whiteColor];
        self.alertContentLabel.textColor = [UIColor blackColor];
    }
    return self;

}

- (void)leftBtnClicked:(id)sender
{
    self.leftBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    [self.leftBtn setBackgroundColor:[UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0f]];
    [self.leftBtn setTitleColor:[UIColor colorWithRed:34.0/255 green:17.0/255 blue:101.0/255 alpha:1.0f] forState:UIControlStateNormal];
    
    self.rightBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Neue" size:17.0f];
    [self.rightBtn setBackgroundColor:[UIColor whiteColor]];
    [self.rightBtn setTitleColor:[UIColor colorWithRed:34.0/255 green:17.0/255 blue:101.0/255 alpha:1.0f] forState:UIControlStateNormal];
    
    _leftLeave = YES;
    [self dismissAlert];
    if (self.leftBlock) {
        self.leftBlock();
    }
}

- (void)rightBtnClicked:(id)sender
{

    self.leftBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    self.rightBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:17.0f];
    
    [self.leftBtn setBackgroundColor:[UIColor whiteColor]];
    [self.rightBtn setBackgroundColor:[UIColor colorWithRed:229.0/255 green:229.0/255 blue:229.0/255 alpha:1.0f]];
    
    [self.leftBtn setTitleColor:[UIColor colorWithRed:158.0/255 green:158.0/255 blue:158.0/255 alpha:1.0f] forState:UIControlStateNormal];
    [self.rightBtn setTitleColor:[UIColor colorWithRed:247.0/255 green:148.0/255 blue:30.0/255 alpha:1.0f] forState:UIControlStateNormal];
    
    _leftLeave = NO;
    
    [self dismissAlert];
    if (self.rightBlock) {
        self.rightBlock();
    }
}

- (void)dismissAlert
{
    [self removeFromSuperview];
    if (self.dismissBlock) {
        self.dismissBlock();
    }
}

- (void)show{
    
    UIViewController *topVC = [self appRootViewController];
    self.frame = CGRectMake((CGRectGetWidth(topVC.view.bounds) - kAlertWidth) * 0.5, (CGRectGetHeight(topVC.view.bounds)-kAlertHeight)*0.5, kAlertWidth, kAlertHeight);
    NSLog(@"*AlertView Frame******************%@",NSStringFromCGRect(self.frame));
    [topVC.view addSubview:self];
}

- (void)showInView:(UIView *)superView{
    
    self.frame = CGRectMake((CGRectGetWidth(superView.bounds) - kAlertWidth) * 0.5, kAlertHeight, kAlertWidth, kAlertHeight);
    NSLog(@"*AlertView Frame******************%@",NSStringFromCGRect(self.frame));
    [superView addSubview:self];
}

- (UIViewController *)appRootViewController
{
    UIViewController *appRootVC = (UIViewController*)[UIApplication sharedApplication].keyWindow.rootViewController;
    return appRootVC;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview == nil) {
        return;
    }
    UIViewController *topVC = [self appRootViewController];
    
    if (!self.backImageView) {
        self.backImageView = [[UIView alloc] initWithFrame:topVC.view.bounds];
        self.backImageView.backgroundColor = [UIColor blackColor];
        self.backImageView.alpha = 0.4f;
        self.backImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }
    [topVC.view addSubview:self.backImageView];
    
    self.alpha = 0;
    self.transform = CGAffineTransformMakeTranslation(0, 300);
    [UIView animateKeyframesWithDuration:0.35f delay:0.0f options:0 animations:^{
        // End
        self.alpha = 1;
        self.transform = CGAffineTransformMakeTranslation(0, 0);
    } completion:^(BOOL finished) {
      
    }];
    
    [super willMoveToSuperview:newSuperview];
}

- (void)removeFromSuperview
{

    self.alpha = 1;
//    self.transform = CGAffineTransformMakeTranslation(0, 0);
    [UIView animateKeyframesWithDuration:0.35 delay:0.0f options:0 animations:^{
        // End
        self.alpha = 0;
        self.backImageView.alpha = 0;
//        self.transform = CGAffineTransformMakeTranslation(-300, 0);
    } completion:^(BOOL finished) {
        
        [self.backImageView removeFromSuperview];
         self.backImageView = nil;
        [super removeFromSuperview];
    }];
}


- (void)drawRect:(CGRect)rect
{
    // Drawing code
}


@end
