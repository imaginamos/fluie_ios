//
//  AlmosaferAlertView.h
//  Almosafer
//
//  Created by Romanov Yuryevich on 9/5/14.
//  Copyright (c) 2014 Romanov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlmosaferAlertView : UIView

- (id)initWithTitle:(NSString *)title
        contentText:(NSString *)content
    leftButtonTitle:(NSString *)leftTitle
   rightButtonTitle:(NSString *)rigthTitle;

- (void)show;
- (void)showInView:(UIView *)superView;

@property (nonatomic, copy) dispatch_block_t leftBlock;
@property (nonatomic, copy) dispatch_block_t rightBlock;
@property (nonatomic, copy) dispatch_block_t dismissBlock;

@end
