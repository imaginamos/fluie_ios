//
//  BeneficiariosListViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/4/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "BeneficiariosListViewController.h"
#import "BeneficiarioFormViewController.h"
#import "MBProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "AlmosaferAlertView.h"

@interface BeneficiariosListViewController ()
{
    NSArray *dataSource;
    NSArray *parentezcoArray;
    NSArray *userArray;
    __weak IBOutlet UITextField *search;
    NSMutableDictionary *beneficiario;
    IBOutlet UISearchBar *candySearchBar;
    NSMutableArray *filteredCandyArray;
    NSString *language_id;
    
}
@property (weak, nonatomic) IBOutlet UITableView *mytableview;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIButton *BTN1;
@property (weak, nonatomic) IBOutlet UIButton *cancel;

@property BOOL searchControllerWasActive;
@property BOOL searchControllerSearchFieldWasFirstResponder;

@end

@implementation BeneficiariosListViewController

-(void) viewWillAppear:(BOOL)animated {
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        [_BTN1 setTitle:@"ADD BENEFICIARY" forState:UIControlStateNormal];
        [_cancel setTitle:@"Cancel" forState:UIControlStateNormal];
        candySearchBar.placeholder = @"Search";
    }
    [self getRelaciones];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _mytableview.backgroundColor = [UIColor clearColor];
    _mytableview.separatorColor = [UIColor clearColor];
    _mytableview.delegate = self;
    _mytableview.dataSource = self;
    [_mytableview setSeparatorColor:[UIColor grayColor]];
    
    //[self getRelaciones];
    
    // Do any additional setup after loading the view.
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

-(void)getRelaciones {
    
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
       
    }

    NSString *urlString = @"http://fluie.com/fluie/api/relationship";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            
            if(arrayresponse.count)
            {
                parentezcoArray=arrayresponse;
            }
            [self getdataSource];

            //[self getUsuarios];
        }else
        {
            
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                [alert showInView:self.view];
            }
        }
    }];
    [dataTask resume];
    
}

-(void)getdataSource {
    
    NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?with=beneficiary&filter=%@", escBox]; //query para pasarle a la url
    
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"beneficiaryUser"] stringByAppendingString:query];
    
    //NSString *urlString = @"http://fluie.com/fluie/api/beneficiaryUser";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil)
        {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];//Todos los beneficiarios de la Base de Datos
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSLog(@"%@", arrayresponse);
            [self hideProgressHUD];
            NSMutableArray *datasourcetemporal = [NSMutableArray arrayWithObjects:nil];
            
            if(arrayresponse.count)
            {
                for(NSDictionary* dato in arrayresponse) { //Recorro los beneficiarios para ver si son mios
                    
                    NSMutableDictionary *datoTemp = [dato mutableCopy];
                    
                    for(NSDictionary* relationship in parentezcoArray) {
                        if ([relationship[@"idrelationship"] isEqualToString:dato[@"relationship_id"]]) {
                            [datoTemp setObject:relationship[@"relationship"] forKey:@"relationship"]; //Guardo la relacion
                        }
                    }
                    
                    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", datoTemp[@"beneficiary"][@"iduser"]];
                    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                    
                    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
                    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                    [request setHTTPMethod: @"POST"];
                    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                    [request setHTTPBody: myRequestData];
                    
                    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                    NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    datoTemp[@"beneficiary"] = [dato[@"beneficiary"] mutableCopy];
                    NSLog(@"%@", datoTemp[@"beneficiary"]);
                    [datoTemp[@"beneficiary"] setObject:foto[@"image"] forKey:@"image"];
                    
                    [datasourcetemporal addObject:datoTemp]; //Almaceno el beneficiario

                }
                dataSource=datasourcetemporal;
                [_mytableview reloadData];
                
            }
            else
            {
                dataSource =[[NSArray alloc]init];
                 [_mytableview reloadData];
                NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notification"
                                                 contentText:@"You do not have any beneficiaries"
                                                 leftButtonTitle:@"Accept"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
                else
                {
                    AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                                 initWithTitle:@"Notificacion"
                                                 contentText:@"Usted no tiene ningún beneficiarios"
                                                 leftButtonTitle:@"Aceptar"
                                                 rightButtonTitle:nil];
                    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                }
            }
        }
        else
        {
            
            NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        return filteredCandyArray.count;
    }
    else
    {
        return dataSource.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
{
    return 62;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //tableView = _mytableview;
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [_mytableview dequeueReusableCellWithIdentifier:CellIdentifier];
    if (tableView == self.searchDisplayController.searchResultsTableView) {
        cell = [_mytableview dequeueReusableCellWithIdentifier:CellIdentifier];
    }
    
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    
    NSMutableDictionary *diccionario;
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        diccionario = [filteredCandyArray[indexPath.row] mutableCopy];
        
    }
    else
    {
        diccionario = [dataSource[indexPath.row] mutableCopy];
    }
    
    UILabel *nombre = (UILabel *)[cell viewWithTag:101];
    nombre.text = [NSString stringWithFormat:@"%@ %@", diccionario[@"beneficiary"][@"name"], diccionario[@"beneficiary"][@"name_last"]];
    UILabel *relacion = (UILabel *)[cell viewWithTag:102];
    relacion.text = diccionario[@"relationship"];

    
    UIButton *botonAgregar = (UIButton *)[cell viewWithTag:500];
//    [botonAgregar addTarget:self
//                     action:@selector(agregar:)
//           forControlEvents:UIControlEventTouchUpInside];
//    botonAgregar.tag = indexPath.row;
    
    
    UIImageView *_miimageView = (UIImageView *)[cell viewWithTag:103];
    _miimageView.layer.masksToBounds = YES;
    [_miimageView.layer setCornerRadius:23.0f];
    [_miimageView.layer setBorderColor:[UIColor whiteColor].CGColor];
    [_miimageView.layer setBorderWidth:0.5f];
    [_miimageView.layer setShadowColor:[UIColor blackColor].CGColor];
    [_miimageView.layer setShadowOpacity:0.8];
    [_miimageView.layer setShadowRadius:3.0];
    [_miimageView.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    if([diccionario[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
        [_miimageView sd_setImageWithURL:[NSURL URLWithString:diccionario[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
    }else{
        [_miimageView sd_setImageWithURL:[NSURL URLWithString:diccionario[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
        }
    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        [botonAgregar setTitle:@"Add" forState:UIControlStateNormal];
    }
    else
    {
        [botonAgregar setTitle:@"Añadir" forState:UIControlStateNormal];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (tableView == self.searchDisplayController.searchResultsTableView)
    {
        beneficiario = [filteredCandyArray[indexPath.row] mutableCopy];
    }
    else
    {
        beneficiario = [[dataSource objectAtIndex:indexPath.row] mutableCopy];
    }
    
    [self.delegado PlayerAdded:self andPlayer:beneficiario];
   // [self performSegueWithIdentifier:@"editarbeneficiario" sender:nil];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

//-(IBAction)agregar:(id)sender
//{
//    UIButton *tempotal = (UIButton *) sender;
//    NSLog(@"%ld", (long)tempotal.tag);
//    
//    beneficiario = [[dataSource objectAtIndex:tempotal.tag] mutableCopy];
//    
//    
//}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"editarbeneficiario"]) {
        BeneficiarioFormViewController *BeneficiarioDetailsViewController = segue.destinationViewController;
        BeneficiarioDetailsViewController.beneficiario = beneficiario;
    }
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    //NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[c] %@", searchText];
    //searchResults = [recipes filteredArrayUsingPredicate:resultPredicate];
    
    [filteredCandyArray removeAllObjects];
    // Filter the array using NSPredicate
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"beneficiary.name CONTAINS[c] %@",searchText];
    NSArray *myarray = dataSource;
    
    filteredCandyArray = [NSMutableArray arrayWithArray:[dataSource filteredArrayUsingPredicate:predicate]];
    NSLog(@"filteredCandyArray %@", filteredCandyArray);
    //dataSource = [NSMutableArray arrayWithArray:[myarray filteredArrayUsingPredicate:predicate]];
    [_mytableview reloadData];
}


/*-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    // Return YES to cause the search result table view to be reloaded.
    return YES;
}*/


@end
