//
//  MsnVozViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/12/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "MsnVozViewController.h"
#import "AlmosaferAlertView.h"
#import "UIImage+Resize.h"
#import "MBProgressHUD.h"
#import "HistorialViewController.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"


@interface MsnVozViewController () {
    UIDatePicker *datePicker;
    NSDate *date;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    __weak IBOutlet UISlider *slider;
    NSString *receptoresString;
    NSMutableArray * receptores;
    UIToolbar *keyboardToolbar;
    __weak IBOutlet UIButton *addButon;
    NSMutableArray *borrador;
    UITextField *mytextField;
    NSTimer *timer1;
    NSArray *beneficiarios;
    NIDropDown *dropDown;
    UITapGestureRecognizer *tap;
    NSTimer* animationTimer;
    BOOL big;
    BOOL borradorBool;
    NSString * language;
}
@property (weak, nonatomic) IBOutlet UILabel *agregar;
@property (weak, nonatomic) IBOutlet UILabel *timelabel;
@property (weak, nonatomic) IBOutlet UILabel *selectPlaceholder;
@property (weak, nonatomic) IBOutlet UIButton *btnSelect;
@property (weak, nonatomic) IBOutlet UIButton *aButton;
@property (weak, nonatomic) IBOutlet UITextField *fecha;
@property (weak, nonatomic) IBOutlet UILabel *lbl2;
@property (weak, nonatomic) IBOutlet UIView *playerview;
@property (weak, nonatomic) IBOutlet UILabel *lbl1;
@property (weak, nonatomic) IBOutlet UIButton *btn1;
@property (weak, nonatomic) IBOutlet UITextField *titulo;
@property (strong, nonatomic) MBProgressHUD *progressHUD;
@property (weak, nonatomic) IBOutlet UIScrollView *receptoresScroll;


@end


@implementation MsnVozViewController
@synthesize stopButton, playButton, recordPauseButton;

-(void) viewWillAppear:(BOOL)animated {
    
    
    [_progressview setProgress:1 animated:NO];
}

-(void)dismissKeyboard {
    if(mytextField!=nil)
        [mytextField resignFirstResponder];
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound) {
        _lbl1.text = @"Used space";
        _titulo.placeholder = @"Message title";
        [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
        _selectPlaceholder.text = @"Program to";
        _agregar.text = @"Add beneficiary";
        
    }
    
    // Disable Stop/Play button when application launches
    [stopButton setEnabled:NO];
    [playButton setEnabled:NO];
    receptores = [[NSMutableArray alloc] init];
    
    tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    // Set the audio file
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    // Setup audio session
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    // Define the recorder setting
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    recorder.delegate = self;
    recorder.meteringEnabled = YES;
    [recorder prepareToRecord];
    [_aButton addTarget:self action:@selector(recordPauseTappedvoid) forControlEvents:UIControlEventTouchDown];
    [_aButton addTarget:self action:@selector(stopTappedVoid) forControlEvents:UIControlEventTouchUpInside];
    
    if (keyboardToolbar == nil) {
        keyboardToolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 44)];
        [keyboardToolbar setBarTintColor:[UIColor whiteColor]];
        
        UIBarButtonItem *extraSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        
        UIBarButtonItem *accept = [[UIBarButtonItem alloc] initWithTitle:@"Cerrar" style:UIBarButtonItemStyleDone target:self action:@selector(closeKeyboard:)];
        
        [keyboardToolbar setItems:[[NSArray alloc] initWithObjects: extraSpace, accept, nil]];
    }
    
    _fecha.inputAccessoryView = keyboardToolbar;
    
    
    datePicker = [[UIDatePicker alloc] init];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    _fecha.inputView = datePicker;
    
    
    if(_esBorrador){
        _selectPlaceholder.text = @"";
        [_playerview setHidden:NO];
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:nil];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:@"MyAudioMemo.m4a"];
        
        [_borradorDiccionario[@"data"] writeToFile:path atomically:YES];
        NSURL *moveUrl = [NSURL fileURLWithPath:path];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:moveUrl error:nil];
        [player setDelegate:self];
        [_playerview setHidden:NO];
        NSString *dur = [NSString stringWithFormat:@"%02d:%02d", (int)((int)(player.duration)) / 60, (int)((int)(player.duration)) % 60];
        _timelabel.text = dur;

        
        _titulo.text = _borradorDiccionario[@"title"];
        _fecha.text = _borradorDiccionario[@"fecha"];
        receptores = [_borradorDiccionario[@"receptores"] mutableCopy];
        
        if (_borradorDiccionario[@"fecha"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"fecha"];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }

        
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";
            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
            if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                [aView removeFromSuperview];
            }
        }
        
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text=@"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
    }else if(_borradorDiccionario!=nil){
        _selectPlaceholder.text = @"";
        [_playerview setHidden:NO];
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        [audioSession setActive:NO error:nil];
        
        NSURL *imageURL = [NSURL URLWithString:_borradorDiccionario[@"messageFiles"][0][@"url"]];
        NSLog(@"%@", _borradorDiccionario);
        NSLog(@"imagen: %@", _borradorDiccionario[@"messageFiles"]);
        NSData *dataFile = [NSData dataWithContentsOfURL:imageURL];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *documentsDirectory = [paths objectAtIndex:0];
        NSString *path = [documentsDirectory stringByAppendingPathComponent:_borradorDiccionario[@"messageFiles"][0][@"file"]];
        
        [dataFile writeToFile:path atomically:YES];
        NSURL *moveUrl = [NSURL fileURLWithPath:path];
        
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:moveUrl error:nil];
        [player setDelegate:self];
        [_playerview setHidden:NO];
        NSString *dur = [NSString stringWithFormat:@"%02d:%02d", (int)((int)(player.duration)) / 60, (int)((int)(player.duration)) % 60];
        _timelabel.text = dur;
        
        _titulo.text = _borradorDiccionario[@"title"];
        NSLog(@"%@", _borradorDiccionario[@"date_view"]);
        if (_borradorDiccionario[@"date_view"] != (id)[NSNull null]){
            _fecha.hidden = NO;
            _fecha.text = _borradorDiccionario[@"date_view"];
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Date" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Fecha" forState:UIControlStateNormal];
            }
        }else{
            //
            _fecha.hidden = YES;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                [_btnSelect setTitle:@"Absence" forState:UIControlStateNormal];
            }
            else
            {
                [_btnSelect setTitle:@"Ausencia" forState:UIControlStateNormal];
            }
        }
        [self getBeneficiarios];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    if(![[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]){
        [self performSegueWithIdentifier:@"goLogin" sender:nil];
    }else{
        [self loadFreeSpace];
    }
}

-(void)loadFreeSpace {
    NSString *urlFreeSpaceString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/freeSpace/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *requestFS = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlFreeSpaceString]];
    [requestFS setHTTPMethod: @"GET"];
    [requestFS addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [requestFS setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    //NSData *dataFS = [NSURLConnection sendSynchronousRequest: requestFS returningResponse: nil error: nil];
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:requestFS completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil) {
            if(returnData){
                NSArray *freeSpace = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                for(id space in freeSpace) {
                    _lbl2.text = [NSString stringWithFormat:@"%@ Mb", space[@"size"]];
                    float progressVal = 0;
                    
                    NSLog(@"%@",space);
                    
                    if([space[@"space"] intValue]!=0){
                        progressVal = ([space[@"space"] floatValue] - [space[@"free"] floatValue])/[space[@"space"] floatValue];
                        
                    }
                    
                    [_progressview setProgress:progressVal animated:YES];
                    
                    NSLog(@"%f",progressVal);
                    [_progressview setProgress:progressVal animated:YES];
                    
                }
            }
        }else{
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
           
        }
    }];
    [dataTask resume];
}

- (void)recordPauseTappedvoid {
    // Stop the audio player before recording
    animationTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(animation:) userInfo:nil repeats:YES];
    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [recordPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        
    } else {
        
        // Pause recording
        [recorder pause];
        [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    }
    
    [stopButton setEnabled:YES];
    [playButton setEnabled:NO];
    
}

- (IBAction)recordPauseTapped:(id)sender {
    // Stop the audio player before recording

    if (player.playing) {
        [player stop];
    }
    
    if (!recorder.recording) {
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        [session setActive:YES error:nil];
        
        // Start recording
        [recorder record];
        [recordPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        
    } else {
        
        // Pause recording
        [recorder pause];
        [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    }
    
    [stopButton setEnabled:YES];
    [playButton setEnabled:NO];
}

- (IBAction)stopTapped:(id)sender {
    [recorder stop];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
}

- (IBAction)stopTappedVoid {
    [recorder stop];
    
    if(animationTimer)
    {
        [animationTimer invalidate];
        animationTimer = nil;
    }
    
    if(big){
        _aButton.frame = CGRectMake(_aButton.frame.origin.x+20, _aButton.frame.origin.y+20, _aButton.frame.size.width-40,  _aButton.frame.size.height-40);
    }
    big=false;
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setActive:NO error:nil];
    
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
    [player setDelegate:self];
    
    if(player.duration<1)
    {
        if([language rangeOfString:@"es"].location == NSNotFound)
        {
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Audio Message"
                                         contentText:@"Press and hold the button to record and release to end"
                                         leftButtonTitle:@"Accept"
                                         rightButtonTitle:nil];
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
        }
        else
        {
            AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                         initWithTitle:@"Mensaje de Audio"
                                         contentText:@"Deja pulsado el botón para grabar y suelta para terminar"
                                         leftButtonTitle:@"Aceptar"
                                         rightButtonTitle:nil];
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
        }
        
    }else{
        [_playerview setHidden:NO];
        NSString *dur = [NSString stringWithFormat:@"%02d:%02d", (int)((int)(player.duration)) / 60, (int)((int)(player.duration)) % 60];
        NSLog(@"%@", dur);
        _timelabel.text = dur;
        NSData *videoData = [NSData dataWithContentsOfFile:[recorder.url path]];
        _lbl2.text = [NSString stringWithFormat:@"%.2f MB", (float)videoData.length/1024.0f/1024.0f];
    }
    
    
}

- (IBAction)playTapped:(id)sender {
     AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    if (!recorder.recording){
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:recorder.url error:nil];
        [player setDelegate:self];
        [player play];
        slider.maximumValue = [player duration];
        slider.value = 0.0;
        timer1 = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    }
}

#pragma mark - AVAudioRecorderDelegate

- (void) audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
    [recordPauseButton setTitle:@"Record" forState:UIControlStateNormal];
    [stopButton setEnabled:NO];
    [playButton setEnabled:YES];
    
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)back:(id)sender {
    if(_esBorrador || _borradorDiccionario!=nil)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self.navigationController popToRootViewControllerAnimated:YES];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"gotoHistorial"]) {
        HistorialViewController *HistorialViewController = segue.destinationViewController;
        HistorialViewController.type_file_id = @"2";
        HistorialViewController.borrador = borradorBool;
    }
    
    if([segue.identifier isEqualToString:@"gotoAdd"]){
        BeneficiariosListViewController *controller = [segue destinationViewController];
        controller.delegado = self;
    }
}

-(IBAction)onAction:(id)sender {
    if ([[Reachability reachabilityForInternetConnection]currentReachabilityStatus]==NotReachable)
    {
        //connection unavailable
        NSLog(@"connection unavailable");
        [self guardarvideo];
    }
    else
    {
        //connection available
        NSLog(@"connection available");
        if (_progressHUD == nil)
        {
            _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
        
        if (receptores.count>0)
        {
            [self guardarvideo];
        }
        else
        {
            [self hideProgressHUD];
            
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Audio Message"
                                             contentText:@"You must select who to send the message"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Mensaje de Audio"
                                             contentText:@"Debe seleccionar a quién enviarle el mensaje"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
        }
    }
}

- (void)guardarvideo {
    borradorBool = NO;
    if(_esBorrador){
        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
        
        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
        if(jsondata!=nil){
            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
            for (int i = 0; i<json.count; i++) {
                NSMutableDictionary *diccionario = [json objectAtIndex:i];
                if(diccionario == _borradorDiccionario){
                    [json removeObjectAtIndex:i];
                }
            }
        }
    }
    
    receptoresString = @"";
    for (int i = 0; i<receptores.count; i++) {
        if([receptoresString isEqualToString:@""])
            receptoresString = [receptores objectAtIndex:i][@"beneficiary"][@"iduser"];
        else
            receptoresString = [NSString stringWithFormat:@"%@,%@", receptoresString, [receptores objectAtIndex:i][@"beneficiary"][@"iduser"]];
        
    }
    
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"] forKey:@"user_id"];
    [_params setObject:[NSString stringWithFormat:@"%@", _titulo.text] forKey:@"m[title]"];
    [_params setObject:@"2" forKey:@"m[type_file_id]"];
    [_params setObject:@"1" forKey:@"m[type_message_id]"];
    [_params setObject:[NSString stringWithFormat:@"%@", _fecha.text] forKey:@"m[date_view]"];
    [_params setObject:@"" forKey:@"m[description]"];
    [_params setObject:receptoresString forKey:@"m[beneficiaries]"];
    
    NSString *urlString = @"http://fluie.com/fluie/messageRestFull/createMessage";
    //http://fluie.com/fluie/messageRestFull/createMessage
    if(_borradorDiccionario!=nil && !_esBorrador){
        urlString = @"http://fluie.com/fluie/messageRestFull/updateMessage";
        [_params setObject:_borradorDiccionario[@"idmessage"] forKey:@"m[idmessage]"];
    }
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"m[file]"; //MODIFICAR
    NSURL* requestURL = [NSURL URLWithString:urlString];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setHTTPMethod:@"POST"];
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in _params) {
        NSLog(@"%@", param);
        NSLog(@"%@", [_params objectForKey:param]);
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // agregar el video, tomar primero su data
    NSData *videoData = [NSData dataWithContentsOfFile:[recorder.url path]];
    
    if (videoData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"MyAudioMemo.m4a\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: audio/m4a\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:videoData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setURL:requestURL]; // ENVIO EL REQUEST
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        [self hideProgressHUD];
        if (error == nil)
        {
            if(returnData)
            {
                NSString *returnString = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
                NSLog(@"response %@",returnString);
                NSDictionary *responseDic = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                AlmosaferAlertView *alert;
                
                if([language rangeOfString:@"es"].location == NSNotFound)
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Accept"
                             rightButtonTitle:nil];
                }
                else
                {
                    alert = [[AlmosaferAlertView alloc]
                             initWithTitle:nil
                             contentText:responseDic[@"msg"]
                             leftButtonTitle:@"Aceptar"
                             rightButtonTitle:nil];
                    
                }

                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                alert.leftBlock = ^()
                {
                    for (UIView *aView in [_receptoresScroll subviews]){
                        _agregar.text = @"Agregar beneficiario";
                        if([language rangeOfString:@"es"].location == NSNotFound)
                            _agregar.text = @"Add beneficiary";
                        if ([aView isKindOfClass:[UIButton class]]){
                            [aView removeFromSuperview];
                        }
                        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                            [aView removeFromSuperview];
                        }
                    }
                    
                    if(![responseDic[@"result"] isEqualToString:@"error"]){
                        _titulo.text = @"";
                        _fecha.text = @"";
                        _fecha.hidden = YES;
                        [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                        _selectPlaceholder.text = @"Programar para";
                        if([language rangeOfString:@"es"].location == NSNotFound) {
                            _titulo.placeholder = @"Message title";
                            _selectPlaceholder.text = @"Program to";
                            [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                            
                            
                        }
                        receptores = nil;
                        //[self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                        if(_esBorrador || _borradorDiccionario!=nil)
                            [self.navigationController popViewControllerAnimated:YES];
                        else
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                    //[self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                    }else{
                        NSError* error;
                        
                        NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                        NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
                        
                        NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
                        if(jsondata!=nil){
                            
                            
                            NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                            if(_esBorrador){
                                int index = (int)[json indexOfObject:_borradorDiccionario];
                                [json removeObjectAtIndex:index];
                                
                            }
                            [json addObject:@{
                                              @"idtypefile" : @"2",
                                              @"title" : _titulo.text,
                                              @"fecha" : _fecha.text,
                                              @"url" : urlString,
                                              @"params" : _params,
                                              @"receptores" : receptores,
                                              @"data" : [NSData dataWithContentsOfFile:[recorder.url path]],
                                              }];
                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                            [data writeToFile:zipFilePath atomically:YES];
                            
                        }else{
                            NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                            [arrayfavoritos addObject:@{
                                                        @"idtypefile" : @"2",
                                                        @"title" : _titulo.text,
                                                        @"fecha" : _fecha.text,
                                                        @"url" : urlString,
                                                        @"params" : _params,
                                                        @"receptores" : receptores,
                                                        @"data" : [NSData dataWithContentsOfFile:[recorder.url path]],
                                                        }];
                            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                            [data writeToFile:zipFilePath atomically:YES];
                            
                            
                        }
                        AlmosaferAlertView *alert;
                        if([language rangeOfString:@"es"].location == NSNotFound)
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Connection error"
                                     contentText:@"It has no internet at the time , the message has been saved in drafts , you will be automatically sent once you have connection"
                                     leftButtonTitle:@"Accept"
                                     rightButtonTitle:nil];
                            
                        }
                        else
                        {
                            alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Error de conexión"
                                     contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
                        }
                        NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
                        alert.leftBlock = ^(){
                            borradorBool = YES;
                            for (UIView *aView in [_receptoresScroll subviews]){
                                _agregar.text = @"Agregar beneficiario";
                                if([language rangeOfString:@"es"].location == NSNotFound)
                                    _agregar.text = @"Add beneficiary";
                                if ([aView isKindOfClass:[UIButton class]]){
                                    [aView removeFromSuperview];
                                }
                                if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                                    [aView removeFromSuperview];
                                }
                            }
                            _titulo.text = @"";
                            _fecha.text = @"";
                            _selectPlaceholder.text = @"Programar para";
                            _fecha.hidden = YES;
                            [_btnSelect setTitle:@"" forState:UIControlStateNormal];
                            if([language rangeOfString:@"es"].location == NSNotFound) {
                                _titulo.placeholder = @"Message title";
                                _selectPlaceholder.text = @"Program to";
                                [_btn1 setTitle:@"SAVE" forState:UIControlStateNormal];
                                
                                
                            }
                            receptores = nil;
                            [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                        };
                        
                    }
                };
            }
        }else{
            NSError* error;
            
            NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSURL *fileURL = [NSURL fileURLWithPath:[documentsDirectory stringByAppendingPathComponent:@"jsoninfo2.json"]];
            
            NSData *jsondata = [NSData dataWithContentsOfURL:fileURL];
            if(jsondata!=nil){
                
                
                NSMutableArray* json = [NSKeyedUnarchiver unarchiveObjectWithData:jsondata];
                if(_esBorrador){
                    int index = (int)[json indexOfObject:_borradorDiccionario];
                    [json removeObjectAtIndex:index];
                    
                }
                [json addObject:@{
                                  @"idtypefile" : @"2",
                                  @"title" : _titulo.text,
                                  @"fecha" : _fecha.text,
                                  @"url" : urlString,
                                  @"params" : _params,
                                  @"receptores" : receptores,
                                  @"data" : [NSData dataWithContentsOfFile:[recorder.url path]],
                                  }];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:json];
                [data writeToFile:zipFilePath atomically:YES];
                
            }else{
                NSMutableArray *arrayfavoritos = [[NSMutableArray alloc] init];
                [arrayfavoritos addObject:@{
                                            @"idtypefile" : @"2",
                                            @"title" : _titulo.text,
                                            @"fecha" : _fecha.text,
                                            @"url" : urlString,
                                            @"params" : _params,
                                  @"receptores" : receptores,
                                            @"data" : [NSData dataWithContentsOfFile:[recorder.url path]],
                                            }];
                NSString *documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                NSString *zipFilePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"jsoninfo2.json"]];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:arrayfavoritos];
                [data writeToFile:zipFilePath atomically:YES];
                
                
            }
            AlmosaferAlertView *alert;
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Connection error"
                         contentText:@"It has no internet at the time , the message has been saved in drafts , you will be automatically sent once you have connection"
                         leftButtonTitle:@"Accept"
                         rightButtonTitle:nil];
            }
            else
            {
                alert = [[AlmosaferAlertView alloc]
                         initWithTitle:@"Error de conexión"
                         contentText:@"No posee conexión a internet en el momento, el mensaje ha sido guardado en borradores, será enviado automáticamente una vez tenga conexión"
                         leftButtonTitle:@"Aceptar"
                         rightButtonTitle:nil];
                
            }
            NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            alert.leftBlock = ^() {
                borradorBool = YES;
                [self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
                //[self performSegueWithIdentifier:@"gotoHistorial" sender:nil];
            };
        }
    }];
    [dataTask resume];
    
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

- (void)datePickerValueChanged:(id)sender{
    
    date = datePicker.date;
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"YYYY-MM-dd"];
    [_fecha setText:[df stringFromDate:date]];
}

- (void)closeKeyboard:(id)sender{ [_fecha resignFirstResponder]; }



- (IBAction)slide {
    player.currentTime = slider.value;
}

- (IBAction)borrar:(id)sender {
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    [_playerview setHidden:YES];
}

- (void)updateTime:(NSTimer *)timer {
    slider.value = player.currentTime;
    NSString *dur = [NSString stringWithFormat:@"%02d:%02d", (int)((int)(player.currentTime)) / 60, (int)((int)(player.duration)) % 60 - (int)((int)(player.currentTime)) % 60];
    NSLog(@"%f %f", player.currentTime, player.duration);
    NSLog(@"%f %f", slider.value, slider.maximumValue);
    _timelabel.text = dur;
    
    if(slider.value==0.0){
        if(timer1)
        {
            [timer1 invalidate];
            timer1 = nil;
        }
    }
}
-(void)animation:(NSTimer *)timer {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1.00];
    [UIView setAnimationDelegate:self];
    
    if(!big)
        _aButton.frame = CGRectMake(_aButton.frame.origin.x-20, _aButton.frame.origin.y-20, _aButton.frame.size.width+40,  _aButton.frame.size.height+40);
    else
        _aButton.frame = CGRectMake(_aButton.frame.origin.x+20, _aButton.frame.origin.y+20, _aButton.frame.size.width-40,  _aButton.frame.size.height-40);
    big=!big;
    [UIView commitAnimations];
}

#pragma mark - ------------ text fields ------------

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.view setFrame:CGRectMake(0,190 - textField.frame.origin.y,self.view.frame.size.width,self.view.frame.size.height)];
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
    [textField resignFirstResponder];
    mytextField = nil;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    mytextField = nil;
    [self.view setFrame:CGRectMake(0,0,self.view.frame.size.width,self.view.frame.size.height)];
}

-(void) PlayerAdded:(BeneficiariosListViewController *)controller andPlayer:(NSDictionary *)beneficiario
{
    if(receptores==nil)
        receptores = [[NSMutableArray alloc] init];
    
    if(![receptores containsObject:beneficiario])
        [receptores addObject:beneficiario];

    
    for (UIView *aView in [_receptoresScroll subviews]){
        _agregar.text = @"Agregar beneficiario";
        if([language rangeOfString:@"es"].location == NSNotFound)
            _agregar.text = @"Add beneficiary";
        if ([aView isKindOfClass:[UIButton class]]){
            [aView removeFromSuperview];
        }
        if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
            [aView removeFromSuperview];
        }
    }
    
    UIImageView *_miimageView = [[UIImageView alloc] init];
    
    for(int i = 0; i<[receptores count]; i++){
        _agregar.text=@"";
        NSDictionary *receptDictionary = [receptores objectAtIndex:i];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTag:i];
        [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
        button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
        button.clipsToBounds = YES;
        
        button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
        button.layer.borderColor=[UIColor grayColor].CGColor;
        button.layer.borderWidth=0.5f;
        
        if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
        }else{
            [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
        }
        
        [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
        
        UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
        [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
        [_receptoresScroll addSubview:button];
        [_receptoresScroll addSubview:nombreReceptor];
        [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
    }
    
    [controller dismissViewControllerAnimated:YES completion:nil];
    
}

-(IBAction)deleteUser:(id)sender {
    UIButton * btn = (UIButton *)sender;
    
    AlmosaferAlertView *alert;
    if([language rangeOfString:@"es"].location == NSNotFound)
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirm"
                 contentText:@"You want to delete this message Beneficiary?"
                 leftButtonTitle:@"Yes"
                 rightButtonTitle:@"No"];
    }
    else
    {
        alert = [[AlmosaferAlertView alloc]
                 initWithTitle:@"Confirmar"
                 contentText:@"Desea borrar de este mensaje al Beneficiario?"
                 leftButtonTitle:@"Si"
                 rightButtonTitle:@"No"];
        
    }
    NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
    alert.leftBlock = ^() {
        [receptores removeObjectAtIndex:(btn.tag-1)];
        
        for (UIView *aView in [_receptoresScroll subviews]){
            _agregar.text = @"Agregar beneficiario";
            if([language rangeOfString:@"es"].location == NSNotFound)
                _agregar.text = @"Add beneficiary";
            if ([aView isKindOfClass:[UIButton class]]){
                [aView removeFromSuperview];
            }
            if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                [aView removeFromSuperview];
            }
        }
        UIImageView *_miimageView = [[UIImageView alloc] init];
        
        for(int i = 0; i<[receptores count]; i++){
            _agregar.text = @"";
            NSDictionary *receptDictionary = [receptores objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTag:i];
            [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
            button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
            button.clipsToBounds = YES;
            
            button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
            button.layer.borderColor=[UIColor grayColor].CGColor;
            button.layer.borderWidth=0.5f;
            
            if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
            }else{
                [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
            }
            
            [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
            
            UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
            nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
            [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
            [_receptoresScroll addSubview:button];
            [_receptoresScroll addSubview:nombreReceptor];
            [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
        }
    };
    
}

-(void)viewWillDisappear:(BOOL)animated {
    if(timer1)
    {
        [timer1 invalidate];
        timer1 = nil;
    }
    if(animationTimer)
    {
        [animationTimer invalidate];
        animationTimer = nil;
    }
}

-(void)getBeneficiarios {
    //Muestro la vista de cargando
    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
    NSString* boxQuery = [NSString stringWithFormat:@"{\"user_id\":\"%@\"}",[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSString* escBox = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,
                                                                                             (CFStringRef) boxQuery,
                                                                                             NULL,
                                                                                             (CFStringRef) @"!*();':@&=+$,/?%#[]{}",
                                                                                             kCFStringEncodingUTF8));
    NSString* query = [NSString stringWithFormat:@"?with=beneficiary&filter=%@", escBox]; //query para pasarle a la url
    
    
    NSString* urlString = [[@"http://fluie.com/fluie/api/" stringByAppendingPathComponent:@"beneficiaryUser"] stringByAppendingString:query];
    
    //NSString *urlString = @"http://fluie.com/fluie/api/beneficiaryUser";
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    
    //NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    
    
    NSURLSessionConfiguration* config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config delegate: nil delegateQueue: [NSOperationQueue mainQueue]];
    
    NSURLSessionDataTask* dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *returnData, NSURLResponse *response, NSError *error) {
        if (error == nil) {
            NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];//Todos los beneficiarios de la Base de Datos
            
            NSArray *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
            NSMutableArray *datasourcetemporal = [NSMutableArray arrayWithObjects:nil];
            if(arrayresponse.count){
                beneficiarios = arrayresponse;
                NSMutableArray *receptoresTemporal = [_borradorDiccionario[@"messageBeneficiaries"] mutableCopy];
                for(int i = 0; i<[receptoresTemporal count]; i++){
                    NSDictionary *receptDictionaryTemporal = [receptoresTemporal objectAtIndex:i];
                    
                    for (int j=0; j<[beneficiarios count]; j++) {
                        NSDictionary *beneficiario = [beneficiarios objectAtIndex:j];
                        NSLog(@"%@ %@", beneficiario[@"beneficiary_id"], receptDictionaryTemporal[@"beneficiary_id"]);
                        if([beneficiario[@"beneficiary_id"] isEqualToString:receptDictionaryTemporal[@"beneficiary_id"]]){
                            [receptores addObject:beneficiario];
                            break;
                        }
                    }
                }
                //receptores
                NSLog(@"receptores %@", receptores);
                for (UIView *aView in [_receptoresScroll subviews]){
                    _agregar.text = @"Agregar beneficiario";
                    if([language rangeOfString:@"es"].location == NSNotFound)
                        _agregar.text = @"Add beneficiary";
                    if ([aView isKindOfClass:[UIButton class]]){
                        [aView removeFromSuperview];
                    }
                    if ([aView isKindOfClass:[UILabel class]] && aView!=_agregar){
                        [aView removeFromSuperview];
                    }
                }
                
                UIImageView *_miimageView = [[UIImageView alloc] init];
                
                for(int i = 0; i<[receptores count]; i++){
                    _agregar.text =@"";
                    NSMutableDictionary *receptDictionary = [[receptores objectAtIndex:i] mutableCopy];
                    NSLog(@"%@", receptDictionary);
                    
                    /*
                     * Armo la foto del beneficiario
                     */
                    NSString *myRequestString = [NSString stringWithFormat:@"iduser=%@", receptDictionary[@"beneficiary_id"]];
                    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
                    
                    NSString *urlString = [NSString stringWithFormat:@"http://fluie.com/fluie/api/user/getImage"];
                    //[[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]
                    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
                    [request setHTTPMethod: @"POST"];
                    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
                    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
                    [request setHTTPBody: myRequestData];
                    
                    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
                    NSDictionary *foto = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
                    NSLog(@"%@", foto[@"image"]);
                    receptDictionary[@"beneficiary"] = [receptDictionary[@"beneficiary"] mutableCopy];
                    [receptDictionary[@"beneficiary"] setObject:foto[@"image"] forKey:@"image"];
                    /*
                     * Fin foto del beneficiario
                     */
                    
                    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
                    [button setTag:i];
                    [button addTarget:self action:@selector(deleteUser:) forControlEvents:UIControlEventTouchUpInside];
                    button.frame = CGRectMake(i*(addButon.frame.size.width+5), 0, addButon.frame.size.width, addButon.frame.size.width);//width and height
                    button.clipsToBounds = YES;
                    
                    button.layer.cornerRadius = addButon.frame.size.width/2;//half of the width
                    button.layer.borderColor=[UIColor grayColor].CGColor;
                    button.layer.borderWidth=0.5f;
                    
                    if([receptDictionary[@"beneficiary"][@"gender_id"] isEqualToString:@"1"]){
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarm.PNG"]];
                    }else{
                        [_miimageView sd_setImageWithURL:[NSURL URLWithString:receptDictionary[@"beneficiary"][@"image"]] placeholderImage:[UIImage imageNamed:@"avatarf.PNG"]];
                    }
                    
                    [button setBackgroundImage:_miimageView.image forState:UIControlStateNormal];
                    
                    UILabel *nombreReceptor = [[UILabel alloc] initWithFrame:CGRectMake(button.frame.origin.x, button.frame.origin.y + button.frame.size.height, button.frame.size.width, 10)];
                    if(receptDictionary[@"beneficiary"][@"name"])
                        nombreReceptor.text = [NSString stringWithFormat:@"%@ %@", receptDictionary[@"beneficiary"][@"name"], receptDictionary[@"beneficiary"][@"name_last"]];
                    [nombreReceptor setFont:[UIFont fontWithName:@"Helvetica Neue" size:9]];
                    [_receptoresScroll addSubview:button];
                    [_receptoresScroll addSubview:nombreReceptor];
                    [_receptoresScroll setContentSize:CGSizeMake(((i*(addButon.frame.size.width+5)) + (addButon.frame.size.width+5)), _receptoresScroll.frame.size.height)];
                }
            }
        }else
        {
            if([language rangeOfString:@"es"].location == NSNotFound)
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notification"
                                             contentText:@"Could not establish communication. Check your connection"
                                             leftButtonTitle:@"Accept"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            else
            {
                AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                             initWithTitle:@"Notificacion"
                                             contentText:@"No se pudo establecer la comunicación. Verifique su conexión"
                                             leftButtonTitle:@"Aceptar"
                                             rightButtonTitle:nil];
                NSString *savedValue = [[NSUserDefaults standardUserDefaults] stringForKey:@"checkvalue"];
                if ([savedValue isEqualToString:@"alertyes"])
                {
                    [alert showInView:self.view];
                }
                else
                {
                    [alert show];
                };
            }
            
            [self hideProgressHUD];
        }
    }];
    [dataTask resume];
    
}

- (void) whoissender: (UIButton *) sender andtext:(NSString *)titulo
{
    [self.view addGestureRecognizer:tap];
    _selectPlaceholder.text = @"";
    if(([titulo isEqualToString:@"Fecha"])||([titulo isEqualToString:@"Date"]))
    {
        _fecha.hidden = NO;
    }else{
        _fecha.text = @"";
        _fecha.hidden = YES;
        
    }
}


- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
    NSLog(@"%@", _btnSelect.titleLabel.text);
}

-(void)rel{
    //    [dropDown release];
    dropDown = nil;
}

- (IBAction)selectClicked:(id)sender {
    [self.view removeGestureRecognizer:tap];
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Ausencia", @"Fecha",nil];
    language = [[NSLocale preferredLanguages] objectAtIndex:0];
    if([language rangeOfString:@"es"].location == NSNotFound)
        arr = [NSArray arrayWithObjects:@"Absence", @"Date",nil];
    if(dropDown == nil) {
        CGFloat f = 80;
        dropDown = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown.delegate = self;
    }
    else {
        [dropDown hideDropDown:sender];
        [self rel];
    }
}

//                if([language rangeOfString:@"es"].location == NSNotFound)
//                {
//                     alert = [[AlmosaferAlertView alloc]
//                                                 initWithTitle:@"Audio Message"
//                                                 contentText:responseDic[@"msg"]
//                                                 leftButtonTitle:@"Accept"
//                                                 rightButtonTitle:nil];
//                }
//                else
//                {
//                     alert = [[AlmosaferAlertView alloc]
//                                                 initWithTitle:@"Mensaje de Audio"
//                                                 contentText:responseDic[@"msg"]
//                                                 leftButtonTitle:@"Aceptar"
//                                                 rightButtonTitle:nil];
//                }

@end
