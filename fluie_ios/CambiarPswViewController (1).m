//
//  CambiarPswViewController.m
//  Fluie
//
//  Created by Carlos Robinson on 12/19/15.
//  Copyright © 2015 Carlos Robinson. All rights reserved.
//

#import "CambiarPswViewController.h"
#import "AlmosaferAlertView.h"
#import "MBProgressHUD.h"
#import "UIImage+Resize.h"

@interface CambiarPswViewController (){
    NSDictionary *usuario;
    UITextField *mytextField;
}

@property (weak, nonatomic) IBOutlet UIImageView *imgPhoto;
@property (weak, nonatomic) IBOutlet UITextField *antigua;
@property (weak, nonatomic) IBOutlet UITextField *nueva;
@property (weak, nonatomic) IBOutlet UITextField *nueva2;
@property (weak, nonatomic) IBOutlet UIScrollView *myscroll;
@property (strong, nonatomic) MBProgressHUD *progressHUD;


@end



@implementation CambiarPswViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //_miimageView.layer.cornerRadius = 100;
    self.imgPhoto.layer.masksToBounds = YES;
    // border radius
    [self.imgPhoto.layer setCornerRadius:45.0f];
    // border
    [self.imgPhoto.layer setBorderColor:[UIColor lightGrayColor].CGColor];
    [self.imgPhoto.layer setBorderWidth:2.5f];
    // drop shadow
    [self.imgPhoto.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.imgPhoto.layer setShadowOpacity:0.8];
    [self.imgPhoto.layer setShadowRadius:3.0];
    [self.imgPhoto.layer setShadowOffset:CGSizeMake(2.0, 2.0)];
    
    [self.myscroll setScrollEnabled:YES];
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 363)];
    
    NSString *urlString = [NSString stringWithFormat:@"http://mentesweb.com/fluie/programacion/api/user/%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"idUser"]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"GET"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    if(returnData){
        NSDictionary *response = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
        
        usuario = response;
        if(![usuario[@"image"] isKindOfClass:[NSNull class]]){
            NSData *data = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:usuario[@"image"]]];
            UIImage *image = [[UIImage alloc] initWithData:data];
            UIImage *selectedImage = image;
            UIImage *scaledImage = [selectedImage resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(90, 90) interpolationQuality:kCGInterpolationHigh];
            UIImage *croppedImage = [scaledImage croppedImage:CGRectMake((scaledImage.size.width -90)/2, (scaledImage.size.height -90)/2, 90, 90)];
            [_imgPhoto setImage:image];
        }
        
        
    }
}

- (void) hideProgressHUD
{
    if (_progressHUD)
    {
        [_progressHUD hide:YES];
        [_progressHUD removeFromSuperview];
        _progressHUD = nil;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    mytextField = textField;
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609+253)];

    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.myscroll setContentSize:CGSizeMake(self.view.bounds.size.width, 609)];
    [self.myscroll setContentOffset:CGPointMake(0, 0) animated:YES];
    
    [textField resignFirstResponder];
    // }
    return YES;
}


- (IBAction)onGuardar:(id)sender {

    if (_progressHUD == nil)
    {
        _progressHUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        _progressHUD.labelText = @"Loading...";
        _progressHUD.detailsLabelText = @"Iniciando Sesión";
        _progressHUD.dimBackground = YES;
    }
    
    NSString *urlString = @"http://mentesweb.com/fluie/programacion/api/user/changePassword";
    NSString *myRequestString = [NSString stringWithFormat:@"email=%@&password=%@",usuario[@"email"], _nueva.text];
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: urlString]];
    [request setHTTPMethod: @"POST"];
    [request addValue:@"Basic YWRtaW46YWRtaW4=" forHTTPHeaderField:@"Authorization"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    NSString *responsestring = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"response %@",responsestring);
    
    NSDictionary *arrayresponse = [[NSJSONSerialization JSONObjectWithData:returnData options:0 error: NULL] mutableCopy];
    
    if([arrayresponse[@"status"] isEqualToString:@"ok"]){
        
        [self hideProgressHUD];
        
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Ingreso"
                                     contentText:[NSString stringWithFormat:@"%@", arrayresponse[@"message"]]
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            [self dismissViewControllerAnimated:YES completion:nil];
        };
        
    
    }else{
        [self hideProgressHUD];
        id val = nil;
        NSArray *values = [arrayresponse[@"message"] allValues];
        if ([values count] != 0)
            val = [values objectAtIndex:0];
        
        NSString *respuesta=@"";
        if([val isKindOfClass:[NSArray class]]){
            respuesta = [val objectAtIndex:0];
        }else if([val isKindOfClass:[NSString class]]){
            respuesta = val;
        }else{
            respuesta = val;
        }
        
        
        AlmosaferAlertView *alert = [[AlmosaferAlertView alloc]
                                     initWithTitle:@"Ingreso"
                                     contentText:[NSString stringWithFormat:@"%@", respuesta]
                                     leftButtonTitle:@"Aceptar"
                                     rightButtonTitle:nil];
        [alert showInView:self.view];
        
        alert.leftBlock = ^() {
            NSLog(@"left button clicked");
        };
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)cancelar:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
